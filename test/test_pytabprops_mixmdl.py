import unittest
import numpy as np
import pytabprops as pytab


class BetaMixMdlTest(unittest.TestCase):
    def test_pdf_scalar_eval(self):
        mean = 0.3
        svar = 0.1
        beta = pytab.BetaMixMdl()
        beta.set_mean(mean)
        beta.set_scaled_variance(svar)
        beta_value = beta.get_pdf(0.4)
        self.assertGreater(beta_value, 0.0)

    def test_pdf_array_eval(self):
        mean = 0.3
        svar = 0.1
        beta = pytab.BetaMixMdl()
        beta.set_mean(mean)
        beta.set_scaled_variance(svar)
        xc = np.array([np.linspace(0, 1, 20),
                       np.linspace(0.1, 0.4, 20)])
        beta_values = beta.get_pdf(xc)
        self.assertTrue(xc.shape == beta_values.shape)

    def test_integrate(self):
        mean = 0.3
        svar = 0.1
        beta = pytab.BetaMixMdl()
        beta.set_mean(mean)
        beta.set_scaled_variance(svar)
        xt = np.linspace(0, 1, 20)
        yt = np.sin(2. * np.pi * xt) + xt
        linear = pytab.LagrangeInterpolant1D(1, xt, yt, True)
        self.assertGreater(beta.integrate(linear, 1), 0.0)


class ClipGaussMixMdlTest(unittest.TestCase):
    def test_pdf_scalar_eval(self):
        mean = 0.3
        svar = 0.1
        beta = pytab.ClippedGaussMixMdl(200, 200, False)
        beta.set_mean(mean)
        beta.set_scaled_variance(svar)
        beta_value = beta.get_pdf(0.4)
        self.assertGreater(beta_value, 0.0)

    def test_pdf_array_eval(self):
        mean = 0.3
        svar = 0.1
        beta = pytab.ClippedGaussMixMdl(200, 200, False)
        beta.set_mean(mean)
        beta.set_scaled_variance(svar)
        xc = np.array([np.linspace(0, 1, 20),
                       np.linspace(0.1, 0.4, 20)])
        beta_values = beta.get_pdf(xc)
        self.assertTrue(xc.shape == beta_values.shape)

    def test_integrate(self):
        mean = 0.3
        svar = 0.1
        beta = pytab.ClippedGaussMixMdl(200, 200, False)
        beta.set_mean(mean)
        beta.set_scaled_variance(svar)
        xt = np.linspace(0, 1, 20)
        yt = np.sin(2. * np.pi * xt) + xt
        linear = pytab.LagrangeInterpolant1D(1, xt, yt, True)
        self.assertGreater(beta.integrate(linear, 1), 0.0)
