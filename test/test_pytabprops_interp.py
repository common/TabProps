import unittest
import numpy as np
import pytabprops as pytab


class Lagrange1DTest(unittest.TestCase):
    def _build_table(self):
        self._xname = list(['x'])
        self._xt = np.linspace(0., 1., 16)
        self._yt = np.cos(2. * np.pi * self._xt)

        self._table = pytab.StateTable()
        self._table.add_entry('linear_clip', pytab.LagrangeInterpolant1D(1, self._xt, self._yt, True), self._xname)
        self._table.add_entry('linear_noclip', pytab.LagrangeInterpolant1D(1, self._xt, self._yt, False), self._xname)
        self._table.add_entry('cubic_clip', pytab.LagrangeInterpolant1D(3, self._xt, self._yt, True), self._xname)
        self._table.add_entry('cubic_noclip', pytab.LagrangeInterpolant1D(3, self._xt, self._yt, False), self._xname)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._build_table()

    def test_scalar_eval(self):
        xt3 = float(self._xt[3])
        yt3 = float(self._yt[3])
        for dv in self._table.get_depvar_names():
            self.assertIsNone(np.testing.assert_allclose(yt3, self._table.query(dv, xt3), atol=1e-12))

    def test_array_eval(self):
        xt_expanded = np.array([self._xt]).T
        for dv in self._table.get_depvar_names():
            self.assertIsNone(np.testing.assert_allclose(self._yt, self._table.query(dv, self._xt), atol=1e-12))
            self.assertTrue(self._table.query(dv, xt_expanded).shape == xt_expanded.shape)
            self.assertIsNone(
                np.testing.assert_allclose(self._yt, self._table.query(dv, xt_expanded)[:, 0], atol=1e-12))

            self.assertIsNone(
                np.testing.assert_allclose(self._yt[::2], self._table.query(dv, self._xt[::2]), atol=1e-12))

    def test_invalid_eval_inputs(self):
        try:
            self._table.query('linear_clip', self._xt, self._xt)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query('blah', self._xt)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

    def test_derivatives(self):
        # specific for 1D only
        scalar_der = np.zeros_like(self._yt)
        for i in range(scalar_der.size):
            scalar_der[i] = self._table.derivative(self._table.get_depvar_names()[0], self._xt[i])
        array_der = self._table.derivative(self._table.get_depvar_names()[0], self._xt)
        array_der_slice = self._table.derivative(self._table.get_depvar_names()[0], self._xt[::2])
        self.assertIsNone(np.testing.assert_allclose(array_der[::2], array_der_slice, atol=1e-12))
        self.assertIsNone(np.testing.assert_allclose(scalar_der[::2], array_der_slice, atol=1e-12))

        # general derivative function
        scalar_gen_der = np.zeros_like(self._yt)
        for i in range(scalar_gen_der.size):
            scalar_gen_der[i] = self._table.derivative(self._table.get_depvar_names()[0], self._xname[0], self._xt[i])
        array_gen_der = self._table.derivative(self._table.get_depvar_names()[0], self._xname[0], self._xt)
        array_gen_der_slice = self._table.derivative(self._table.get_depvar_names()[0], self._xname[0], self._xt[::2])
        self.assertIsNone(np.testing.assert_allclose(array_gen_der[::2], array_gen_der_slice, atol=1e-12))

        self.assertIsNone(np.testing.assert_allclose(scalar_gen_der, array_gen_der, atol=1e-12))
        self.assertIsNone(np.testing.assert_allclose(scalar_gen_der, scalar_der, atol=1e-12))
        self.assertIsNone(np.testing.assert_allclose(array_gen_der, array_der, atol=1e-12))

        # testing shapes
        xt_expanded = np.array([self._xt]).T
        self.assertTrue(self._table.derivative(self._table.get_depvar_names()[0], self._xt).shape == self._xt.shape)
        self.assertTrue(
            self._table.derivative(self._table.get_depvar_names()[0], xt_expanded).shape == xt_expanded.shape)
        self.assertTrue(
            self._table.derivative(self._table.get_depvar_names()[0], self._xname[0], self._xt).shape == self._xt.shape)
        self.assertTrue(self._table.derivative(self._table.get_depvar_names()[0], self._xname[0],
                                               xt_expanded).shape == xt_expanded.shape)

        # general derivatives function
        all_ders = self._table.derivatives(self._table.get_depvar_names()[0], self._xt)
        all_ders_slice = self._table.derivatives(self._table.get_depvar_names()[0], self._xt[::2])
        self.assertIsNone(np.testing.assert_allclose(array_gen_der, all_ders[0], atol=1e-12))
        self.assertIsNone(np.testing.assert_allclose(all_ders[0][::2], all_ders_slice[0], atol=1e-12))

        # testing 1D derivative inputs
        try:
            self._table.derivative('blah', self._xt)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        # testing general derivative inputs
        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._xname[0], self._xt, self._xt)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], 'blah', self._xt)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative('blah', self._xname[0], self._xt)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        # testing derivatives inputs
        try:
            self._table.derivatives('blah', self._xt)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._xt, self._xt)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

    def test_second_derivatives(self):
        # specific for 1D only
        scalar_der = np.zeros_like(self._yt)
        for i in range(scalar_der.size):
            scalar_der[i] = self._table.second_derivative(self._table.get_depvar_names()[0], self._xt[i])
        array_der = self._table.second_derivative(self._table.get_depvar_names()[0], self._xt)
        array_der_slice = self._table.second_derivative(self._table.get_depvar_names()[0], self._xt[::2])
        self.assertIsNone(np.testing.assert_allclose(array_der[::2], array_der_slice, atol=1e-12))
        self.assertIsNone(np.testing.assert_allclose(scalar_der[::2], array_der_slice, atol=1e-12))

        # general second_derivative function
        scalar_gen_der = np.zeros_like(self._yt)
        for i in range(scalar_gen_der.size):
            scalar_gen_der[i] = self._table.second_derivative(self._table.get_depvar_names()[0], self._xname[0],
                                                              self._xname[0], self._xt[i])
        array_gen_der = self._table.second_derivative(self._table.get_depvar_names()[0], self._xname[0], self._xname[0],
                                                      self._xt)
        array_gen_der_slice = self._table.second_derivative(self._table.get_depvar_names()[0], self._xname[0],
                                                            self._xname[0], self._xt[::2])
        self.assertIsNone(np.testing.assert_allclose(array_gen_der[::2], array_gen_der_slice, atol=1e-12))

        self.assertIsNone(np.testing.assert_allclose(scalar_gen_der, array_gen_der, atol=1e-12))
        self.assertIsNone(np.testing.assert_allclose(scalar_gen_der, scalar_der, atol=1e-12))
        self.assertIsNone(np.testing.assert_allclose(array_gen_der, array_der, atol=1e-12))

        # testing shapes
        xt_expanded = np.array([self._xt]).T
        self.assertTrue(
            self._table.second_derivative(self._table.get_depvar_names()[0], self._xt).shape == self._xt.shape)
        self.assertTrue(
            self._table.second_derivative(self._table.get_depvar_names()[0], xt_expanded).shape == xt_expanded.shape)
        self.assertTrue(
            self._table.second_derivative(self._table.get_depvar_names()[0], self._xname[0], self._xname[0],
                                          self._xt).shape == self._xt.shape)
        self.assertTrue(self._table.second_derivative(self._table.get_depvar_names()[0], self._xname[0], self._xname[0],
                                                      xt_expanded).shape == xt_expanded.shape)

        # testing 1D second_derivative inputs
        try:
            self._table.second_derivative('blah', self._xt)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        # testing general second_derivative inputs
        try:
            self._table.second_derivative(self._table.get_depvar_names()[0], self._xname[0], self._xname[0], self._xt,
                                          self._xt)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.second_derivative(self._table.get_depvar_names()[0], self._xname[0], 'blah', self._xt)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.second_derivative(self._table.get_depvar_names()[0], 'blah', self._xname[0], self._xt)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.second_derivative(self._table.get_depvar_names()[0], 'blah', 'blah', self._xt)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.second_derivative('blah', self._xname[0], self._xname[0], self._xt)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)


class Lagrange2DTest(unittest.TestCase):
    def _build_table(self):
        self._xname = list(['x1', 'x2'])
        x1 = np.linspace(0., 1., 16)
        x2 = x1.copy()
        self._X1t, self._X2t = np.meshgrid(x1, x2, indexing='ij')

        self._yt = np.cos(2. * np.pi * self._X1t) + np.sin(2. * np.pi * self._X2t)

        self._table = pytab.StateTable()
        self._table.add_entry('yt', pytab.LagrangeInterpolant2D(1, x1, x2, self._yt.ravel(order='F'), True),
                              self._xname)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._build_table()

    def test_scalar_eval(self):
        x1t3 = float(self._X1t.ravel()[3])
        x2t3 = float(self._X2t.ravel()[3])
        yt3 = float(self._yt.ravel()[3])
        self.assertIsNone(
            np.testing.assert_allclose(yt3, self._table.query(self._table.get_depvar_names()[0], x1t3, x2t3),
                                       atol=1e-12))

    def test_array_eval(self):
        self.assertIsNone(np.testing.assert_allclose(self._yt,
                                                     self._table.query(self._table.get_depvar_names()[0], self._X1t,
                                                                       self._X2t), atol=1e-12))
        self.assertTrue(
            self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t).shape == self._X1t.shape)
        self.assertIsNone(np.testing.assert_allclose(self._yt.ravel(),
                                                     self._table.query(self._table.get_depvar_names()[0],
                                                                       self._X1t.ravel(), self._X2t.ravel()),
                                                     atol=1e-12))
        self.assertIsNone(np.testing.assert_allclose(self._yt[::2, ::2],
                                                     self._table.query(self._table.get_depvar_names()[0],
                                                                       self._X1t[::2, ::2], self._X2t[::2, ::2]),
                                                     atol=1e-12))

    def test_invalid_eval_inputs(self):
        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X2t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, 1.)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

    def test_derivatives(self):
        all_der = self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t)
        all_der_slice = self._table.derivatives(self._table.get_depvar_names()[0], self._X1t[::2, ::2],
                                                self._X2t[::2, ::2])
        scalar_dx1 = np.zeros_like(self._yt.ravel())
        for i in range(scalar_dx1.size):
            scalar_dx1[i] = self._table.derivative(self._table.get_depvar_names()[0],
                                                   self._table.get_indepvar_names()[0], self._X1t.ravel()[i],
                                                   self._X2t.ravel()[i])
        array_dx1 = self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0],
                                           self._X1t, self._X2t)
        self.assertIsNone(np.testing.assert_allclose(scalar_dx1, array_dx1.ravel(), atol=1e-12))
        array_dx1_slice = self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0],
                                                 self._X1t[::2, ::2], self._X2t[::2, ::2])
        self.assertIsNone(np.testing.assert_allclose(array_dx1[::2, ::2], array_dx1_slice, atol=1e-12))
        self.assertTrue(array_dx1.shape == self._X1t.shape)
        for i in range(self._table.get_ndim()):
            self.assertIsNone(np.testing.assert_allclose(all_der[i][::2, ::2], all_der_slice[i], atol=1e-12))
            self.assertIsNone(np.testing.assert_allclose(all_der[i],
                                                         self._table.derivative(self._table.get_depvar_names()[0],
                                                                                self._table.get_indepvar_names()[i],
                                                                                self._X1t, self._X2t), atol=1e-12))

        # testing derivative inputs
        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   self._X2t, self._X2t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   1.)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        # testing derivatives inputs
        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X2t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, 1.)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

    def test_second_derivatives(self):
        ndim = len(self._xname)
        x1 = self._X1t.ravel()
        x2 = self._X2t.ravel()
        dv = self._table.get_depvar_names()[0]
        for i in range(ndim):
            iv1 = self._table.get_indepvar_names()[i]
            for j in range(ndim):
                iv2 = self._table.get_indepvar_names()[j]
                scalar_value = self._table.second_derivative(dv, iv1, iv2, x1[0], x2[0])
                scalar_value_t = self._table.second_derivative(dv, iv2, iv1, x1[0], x2[0])
                array_values = self._table.second_derivative(dv, iv1, iv2, x1, x2)
                slice_values = self._table.second_derivative(dv, iv1, iv2, x1[::2], x2[::2])

                self.assertEqual(scalar_value, scalar_value_t)  # symmetry
                self.assertEqual(scalar_value, array_values[0])
                self.assertEqual(scalar_value, slice_values[0])
                self.assertIsNone(np.testing.assert_allclose(array_values[::2], slice_values, atol=1e-12))

        try:
            self._table.second_derivative(dv, iv1, iv2, x1, x2, x2)  # too many args
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.second_derivative('bleh', iv1, iv2, x1, x2)  # invalid property
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.second_derivative(dv, 'bleh', iv2, x1, x2)  # invalid first arg
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.second_derivative(dv, iv1, 'bleh', x1, x2)  # invalid second arg
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)


class Lagrange3DTest(unittest.TestCase):
    def _build_table(self):
        self._xname = list(['x1', 'x2', 'x3'])
        x1 = np.linspace(0., 1., 16)
        x2 = x1.copy()
        x3 = x1.copy()
        self._X1t, self._X2t, self._X3t = np.meshgrid(x1, x2, x3, indexing='ij')

        self._yt = np.cos(2. * np.pi * self._X1t) + np.sin(2. * np.pi * self._X2t) + np.cos(2. * np.pi * self._X3t) ** 2

        self._table = pytab.StateTable()
        self._table.add_entry('yt', pytab.LagrangeInterpolant3D(1, x1, x2, x3, self._yt.ravel(order='F'), True),
                              self._xname)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._build_table()

    def test_scalar_eval(self):
        x1t3 = float(self._X1t.ravel()[3])
        x2t3 = float(self._X2t.ravel()[3])
        x3t3 = float(self._X3t.ravel()[3])
        yt3 = float(self._yt.ravel()[3])
        self.assertIsNone(
            np.testing.assert_allclose(yt3, self._table.query(self._table.get_depvar_names()[0], x1t3, x2t3, x3t3),
                                       atol=1e-12))

    def test_array_eval(self):
        self.assertIsNone(np.testing.assert_allclose(self._yt,
                                                     self._table.query(self._table.get_depvar_names()[0], self._X1t,
                                                                       self._X2t, self._X3t), atol=1e-12))
        self.assertTrue(self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t,
                                          self._X3t).shape == self._X1t.shape)
        self.assertIsNone(np.testing.assert_allclose(self._yt.ravel(),
                                                     self._table.query(self._table.get_depvar_names()[0],
                                                                       self._X1t.ravel(), self._X2t.ravel(),
                                                                       self._X3t.ravel()), atol=1e-12))
        self.assertIsNone(np.testing.assert_allclose(self._yt[::2, ::2, ::2],
                                                     self._table.query(self._table.get_depvar_names()[0],
                                                                       self._X1t[::2, ::2, ::2],
                                                                       self._X2t[::2, ::2, ::2],
                                                                       self._X3t[::2, ::2, ::2]), atol=1e-12))

    def test_invalid_eval_inputs(self):
        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t, self._X3t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, 1., self._X3t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t, 1.)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

    def test_derivatives(self):
        all_der = self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t)
        all_der_slice = self._table.derivatives(self._table.get_depvar_names()[0], self._X1t[::2, ::2, ::2],
                                                self._X2t[::2, ::2, ::2], self._X3t[::2, ::2, ::2])
        scalar_dx1 = np.zeros_like(self._yt.ravel())
        for i in range(scalar_dx1.size):
            scalar_dx1[i] = self._table.derivative(self._table.get_depvar_names()[0],
                                                   self._table.get_indepvar_names()[0], self._X1t.ravel()[i],
                                                   self._X2t.ravel()[i], self._X3t.ravel()[i])
        array_dx1 = self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0],
                                           self._X1t, self._X2t, self._X3t)
        self.assertIsNone(np.testing.assert_allclose(scalar_dx1, array_dx1.ravel(), atol=1e-12))
        array_dx1_slice = self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0],
                                                 self._X1t[::2, ::2, ::2], self._X2t[::2, ::2, ::2],
                                                 self._X3t[::2, ::2, ::2])
        self.assertIsNone(np.testing.assert_allclose(array_dx1[::2, ::2, ::2], array_dx1_slice, atol=1e-12))
        self.assertTrue(array_dx1.shape == self._X1t.shape)
        for i in range(self._table.get_ndim()):
            self.assertIsNone(np.testing.assert_allclose(all_der[i][::2, ::2, ::2], all_der_slice[i], atol=1e-12))
            self.assertIsNone(np.testing.assert_allclose(all_der[i],
                                                         self._table.derivative(self._table.get_depvar_names()[0],
                                                                                self._table.get_indepvar_names()[i],
                                                                                self._X1t, self._X2t, self._X3t),
                                                         atol=1e-12))

        # testing derivative inputs
        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   self._X2t, self._X3t, self._X3t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   self._X2t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   1., self._X3t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   self._X2t, 1.)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        # testing derivatives inputs
        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t, self._X3t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, 1., self._X3t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t, 1.)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

    def test_second_derivatives(self):
        ndim = len(self._xname)
        x1 = self._X1t.ravel()
        x2 = self._X2t.ravel()
        x3 = self._X3t.ravel()
        dv = self._table.get_depvar_names()[0]
        for i in range(ndim):
            iv1 = self._table.get_indepvar_names()[i]
            for j in range(ndim):
                iv2 = self._table.get_indepvar_names()[j]
                scalar_value = self._table.second_derivative(dv, iv1, iv2, x1[0], x2[0], x3[0])
                scalar_value_t = self._table.second_derivative(dv, iv2, iv1, x1[0], x2[0], x3[0])
                array_values = self._table.second_derivative(dv, iv1, iv2, x1, x2, x3)
                slice_values = self._table.second_derivative(dv, iv1, iv2, x1[::2], x2[::2], x3[::2])

                self.assertEqual(scalar_value, scalar_value_t)  # symmetry
                self.assertEqual(scalar_value, array_values[0])
                self.assertEqual(scalar_value, slice_values[0])
                self.assertIsNone(np.testing.assert_allclose(array_values[::2], slice_values, atol=1e-12))

        try:
            self._table.second_derivative(dv, iv1, iv2, x1, x2, x3, x3)  # too many args
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.second_derivative('bleh', iv1, iv2, x1, x2, x3)  # invalid property
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.second_derivative(dv, 'bleh', iv2, x1, x2, x3)  # invalid first arg
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.second_derivative(dv, iv1, 'bleh', x1, x2, x3)  # invalid second arg
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)


class Lagrange4DTest(unittest.TestCase):
    def _build_table(self):
        self._xname = list(['x1', 'x2', 'x3', 'x4'])
        x1 = np.linspace(0., 1., 8)
        x2 = x1.copy()
        x3 = x1.copy()
        x4 = x1.copy()
        self._X1t, self._X2t, self._X3t, self._X4t = np.meshgrid(x1, x2, x3, x4, indexing='ij')

        self._yt = np.cos(2. * np.pi * self._X1t) + np.sin(2. * np.pi * self._X2t) + np.cos(
            2. * np.pi * self._X3t) ** 2 + np.sin(2. * np.pi * self._X4t) ** 2

        self._table = pytab.StateTable()
        self._table.add_entry('yt', pytab.LagrangeInterpolant4D(1, x1, x2, x3, x4, self._yt.ravel(order='F'), True),
                              self._xname)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._build_table()

    def test_scalar_eval(self):
        x1t3 = float(self._X1t.ravel()[3])
        x2t3 = float(self._X2t.ravel()[3])
        x3t3 = float(self._X3t.ravel()[3])
        x4t3 = float(self._X4t.ravel()[3])
        yt3 = float(self._yt.ravel()[3])
        self.assertIsNone(np.testing.assert_allclose(yt3,
                                                     self._table.query(self._table.get_depvar_names()[0], x1t3, x2t3,
                                                                       x3t3, x4t3), atol=1e-12))

    def test_array_eval(self):
        self.assertIsNone(np.testing.assert_allclose(self._yt,
                                                     self._table.query(self._table.get_depvar_names()[0], self._X1t,
                                                                       self._X2t, self._X3t, self._X4t), atol=1e-12))
        self.assertTrue(self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t,
                                          self._X4t).shape == self._X1t.shape)
        self.assertIsNone(np.testing.assert_allclose(self._yt.ravel(),
                                                     self._table.query(self._table.get_depvar_names()[0],
                                                                       self._X1t.ravel(), self._X2t.ravel(),
                                                                       self._X3t.ravel(), self._X4t.ravel()),
                                                     atol=1e-12))
        self.assertIsNone(np.testing.assert_allclose(self._yt[::2, ::2, ::2, ::2],
                                                     self._table.query(self._table.get_depvar_names()[0],
                                                                       self._X1t[::2, ::2, ::2, ::2],
                                                                       self._X2t[::2, ::2, ::2, ::2],
                                                                       self._X3t[::2, ::2, ::2, ::2],
                                                                       self._X4t[::2, ::2, ::2, ::2]), atol=1e-12))

    def test_invalid_eval_inputs(self):
        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t, self._X4t, self._X4t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, 1., self._X3t, self._X4t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t, 1., self._X4t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t, 1.)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

    def test_derivatives(self):
        all_der = self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t, self._X4t)
        all_der_slice = self._table.derivatives(self._table.get_depvar_names()[0], self._X1t[::2, ::2, ::2, ::2],
                                                self._X2t[::2, ::2, ::2, ::2], self._X3t[::2, ::2, ::2, ::2],
                                                self._X4t[::2, ::2, ::2, ::2])
        scalar_dx1 = np.zeros_like(self._yt.ravel())
        for i in range(scalar_dx1.size):
            scalar_dx1[i] = self._table.derivative(self._table.get_depvar_names()[0],
                                                   self._table.get_indepvar_names()[0], self._X1t.ravel()[i],
                                                   self._X2t.ravel()[i], self._X3t.ravel()[i], self._X4t.ravel()[i])
        array_dx1 = self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0],
                                           self._X1t, self._X2t, self._X3t, self._X4t)
        self.assertIsNone(np.testing.assert_allclose(scalar_dx1, array_dx1.ravel(), atol=1e-12))
        array_dx1_slice = self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0],
                                                 self._X1t[::2, ::2, ::2, ::2], self._X2t[::2, ::2, ::2, ::2],
                                                 self._X3t[::2, ::2, ::2, ::2], self._X4t[::2, ::2, ::2, ::2])
        self.assertIsNone(np.testing.assert_allclose(array_dx1[::2, ::2, ::2, ::2], array_dx1_slice, atol=1e-12))
        self.assertTrue(array_dx1.shape == self._X1t.shape)
        for i in range(self._table.get_ndim()):
            self.assertIsNone(np.testing.assert_allclose(all_der[i][::2, ::2, ::2, ::2], all_der_slice[i], atol=1e-12))
            self.assertIsNone(np.testing.assert_allclose(all_der[i],
                                                         self._table.derivative(self._table.get_depvar_names()[0],
                                                                                self._table.get_indepvar_names()[i],
                                                                                self._X1t, self._X2t, self._X3t,
                                                                                self._X4t), atol=1e-12))

        # testing derivative inputs
        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   self._X2t, self._X3t, self._X4t, self._X4t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   self._X2t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   self._X2t, self._X3t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   1., self._X3t, self._X4t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   self._X2t, 1., self._X4t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   self._X2t, self._X3t, 1.)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        # testing derivatives inputs
        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t, self._X4t,
                                    self._X4t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, 1., self._X3t, self._X4t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t, 1., self._X4t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t, 1.)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

    def test_second_derivatives(self):
        ndim = len(self._xname)
        x1 = self._X1t.ravel()
        x2 = self._X2t.ravel()
        x3 = self._X3t.ravel()
        x4 = self._X4t.ravel()
        dv = self._table.get_depvar_names()[0]
        for i in range(ndim):
            iv1 = self._table.get_indepvar_names()[i]
            for j in range(ndim):
                iv2 = self._table.get_indepvar_names()[j]
                scalar_value = self._table.second_derivative(dv, iv1, iv2, x1[0], x2[0], x3[0], x4[0])
                scalar_value_t = self._table.second_derivative(dv, iv2, iv1, x1[0], x2[0], x3[0], x4[0])
                array_values = self._table.second_derivative(dv, iv1, iv2, x1, x2, x3, x4)
                slice_values = self._table.second_derivative(dv, iv1, iv2, x1[::2], x2[::2], x3[::2], x4[::2])

                self.assertEqual(scalar_value, scalar_value_t)  # symmetry
                self.assertEqual(scalar_value, array_values[0])
                self.assertEqual(scalar_value, slice_values[0])
                self.assertIsNone(np.testing.assert_allclose(array_values[::2], slice_values, atol=1e-12))

        try:
            self._table.second_derivative(dv, iv1, iv2, x1, x2, x3, x4, x4)  # too many args
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.second_derivative('bleh', iv1, iv2, x1, x2, x3, x4)  # invalid property
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.second_derivative(dv, 'bleh', iv2, x1, x2, x3, x4)  # invalid first arg
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.second_derivative(dv, iv1, 'bleh', x1, x2, x3, x4)  # invalid second arg
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)


class Lagrange5DTest(unittest.TestCase):
    def _build_table(self):
        self._xname = list(['x1', 'x2', 'x3', 'x4', 'x5'])
        x1 = np.linspace(0., 1., 5)
        x2 = x1.copy()
        x3 = x1.copy()
        x4 = x1.copy()
        x5 = x1.copy()
        self._X1t, self._X2t, self._X3t, self._X4t, self._X5t = np.meshgrid(x1, x2, x3, x4, x5, indexing='ij')

        self._yt = np.cos(2. * np.pi * self._X1t) + np.sin(2. * np.pi * self._X2t) + np.cos(
            2. * np.pi * self._X3t) ** 2 + np.sin(2. * np.pi * self._X4t) ** 2 + np.cos(
            2. * np.pi * self._X5t) * np.sin(2. * np.pi * self._X5t)

        self._table = pytab.StateTable()
        self._table.add_entry('yt', pytab.LagrangeInterpolant5D(1, x1, x2, x3, x4, x5, self._yt.ravel(order='F'), True),
                              self._xname)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._build_table()

    def test_scalar_eval(self):
        x1t3 = float(self._X1t.ravel()[3])
        x2t3 = float(self._X2t.ravel()[3])
        x3t3 = float(self._X3t.ravel()[3])
        x4t3 = float(self._X4t.ravel()[3])
        x5t3 = float(self._X5t.ravel()[3])
        yt3 = float(self._yt.ravel()[3])
        self.assertIsNone(np.testing.assert_allclose(yt3,
                                                     self._table.query(self._table.get_depvar_names()[0], x1t3, x2t3,
                                                                       x3t3, x4t3, x5t3), atol=1e-12))

    def test_array_eval(self):
        self.assertIsNone(np.testing.assert_allclose(self._yt,
                                                     self._table.query(self._table.get_depvar_names()[0], self._X1t,
                                                                       self._X2t, self._X3t, self._X4t, self._X5t),
                                                     atol=1e-12))
        self.assertTrue(self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t, self._X4t,
                                          self._X5t).shape == self._X1t.shape)
        self.assertIsNone(np.testing.assert_allclose(self._yt.ravel(),
                                                     self._table.query(self._table.get_depvar_names()[0],
                                                                       self._X1t.ravel(), self._X2t.ravel(),
                                                                       self._X3t.ravel(), self._X4t.ravel(),
                                                                       self._X5t.ravel()), atol=1e-12))
        self.assertIsNone(np.testing.assert_allclose(self._yt[::2, ::2, ::2, ::2, ::2],
                                                     self._table.query(self._table.get_depvar_names()[0],
                                                                       self._X1t[::2, ::2, ::2, ::2, ::2],
                                                                       self._X2t[::2, ::2, ::2, ::2, ::2],
                                                                       self._X3t[::2, ::2, ::2, ::2, ::2],
                                                                       self._X4t[::2, ::2, ::2, ::2, ::2],
                                                                       self._X5t[::2, ::2, ::2, ::2, ::2]), atol=1e-12))

    def test_invalid_eval_inputs(self):
        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t, self._X4t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, 1., self._X3t, self._X4t, self._X5t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t, 1., self._X4t, self._X5t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t, 1., self._X5t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.query(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t, self._X4t, 1.)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

    def test_derivatives(self):
        all_der = self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t, self._X4t,
                                          self._X5t)
        all_der_slice = self._table.derivatives(self._table.get_depvar_names()[0], self._X1t[::2, ::2, ::2, ::2, ::2],
                                                self._X2t[::2, ::2, ::2, ::2, ::2], self._X3t[::2, ::2, ::2, ::2, ::2],
                                                self._X4t[::2, ::2, ::2, ::2, ::2], self._X5t[::2, ::2, ::2, ::2, ::2])
        scalar_dx1 = np.zeros_like(self._yt.ravel())
        for i in range(scalar_dx1.size):
            scalar_dx1[i] = self._table.derivative(self._table.get_depvar_names()[0],
                                                   self._table.get_indepvar_names()[0], self._X1t.ravel()[i],
                                                   self._X2t.ravel()[i], self._X3t.ravel()[i], self._X4t.ravel()[i],
                                                   self._X5t.ravel()[i])
        array_dx1 = self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0],
                                           self._X1t, self._X2t, self._X3t, self._X4t, self._X5t)
        self.assertIsNone(np.testing.assert_allclose(scalar_dx1, array_dx1.ravel(), atol=1e-12))
        array_dx1_slice = self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0],
                                                 self._X1t[::2, ::2, ::2, ::2, ::2], self._X2t[::2, ::2, ::2, ::2, ::2],
                                                 self._X3t[::2, ::2, ::2, ::2, ::2], self._X4t[::2, ::2, ::2, ::2, ::2],
                                                 self._X5t[::2, ::2, ::2, ::2, ::2])
        self.assertIsNone(np.testing.assert_allclose(array_dx1[::2, ::2, ::2, ::2, ::2], array_dx1_slice, atol=1e-12))
        self.assertTrue(array_dx1.shape == self._X1t.shape)
        for i in range(self._table.get_ndim()):
            self.assertIsNone(
                np.testing.assert_allclose(all_der[i][::2, ::2, ::2, ::2, ::2], all_der_slice[i], atol=1e-12))
            self.assertIsNone(np.testing.assert_allclose(all_der[i],
                                                         self._table.derivative(self._table.get_depvar_names()[0],
                                                                                self._table.get_indepvar_names()[i],
                                                                                self._X1t, self._X2t, self._X3t,
                                                                                self._X4t, self._X5t), atol=1e-12))

        # testing derivative inputs
        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   self._X2t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   self._X2t, self._X3t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   self._X2t, self._X3t, self._X4t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   1., self._X3t, self._X4t, self._X5t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   self._X2t, 1., self._X4t, self._X5t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   self._X2t, self._X3t, 1., self._X5t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivative(self._table.get_depvar_names()[0], self._table.get_indepvar_names()[0], self._X1t,
                                   self._X2t, self._X3t, self._X4t, 1.)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        # testing derivatives inputs
        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t, self._X4t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, 1., self._X3t, self._X4t, self._X5t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t, 1., self._X4t, self._X5t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t, 1., self._X5t)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.derivatives(self._table.get_depvar_names()[0], self._X1t, self._X2t, self._X3t, self._X4t, 1.)
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

    def test_second_derivatives(self):
        ndim = len(self._xname)
        x1 = self._X1t.ravel()
        x2 = self._X2t.ravel()
        x3 = self._X3t.ravel()
        x4 = self._X4t.ravel()
        x5 = self._X5t.ravel()
        dv = self._table.get_depvar_names()[0]
        for i in range(ndim):
            iv1 = self._table.get_indepvar_names()[i]
            for j in range(ndim):
                iv2 = self._table.get_indepvar_names()[j]
                scalar_value = self._table.second_derivative(dv, iv1, iv2, x1[0], x2[0], x3[0], x4[0], x5[0])
                scalar_value_t = self._table.second_derivative(dv, iv2, iv1, x1[0], x2[0], x3[0], x4[0], x5[0])
                array_values = self._table.second_derivative(dv, iv1, iv2, x1, x2, x3, x4, x5)
                slice_values = self._table.second_derivative(dv, iv1, iv2, x1[::2], x2[::2], x3[::2], x4[::2], x5[::2])

                self.assertEqual(scalar_value, scalar_value_t)  # symmetry
                self.assertEqual(scalar_value, array_values[0])
                self.assertEqual(scalar_value, slice_values[0])
                self.assertIsNone(np.testing.assert_allclose(array_values[::2], slice_values, atol=1e-12))

        try:
            self._table.second_derivative(dv, iv1, iv2, x1, x2, x3, x4, x5, x5)  # too many args
            self.assertTrue(False)
        except TypeError:
            self.assertTrue(True)

        try:
            self._table.second_derivative('bleh', iv1, iv2, x1, x2, x3, x4, x5)  # invalid property
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.second_derivative(dv, 'bleh', iv2, x1, x2, x3, x4, x5)  # invalid first arg
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)

        try:
            self._table.second_derivative(dv, iv1, 'bleh', x1, x2, x3, x4, x5)  # invalid second arg
            self.assertTrue(False)
        except RuntimeError:
            self.assertTrue(True)
