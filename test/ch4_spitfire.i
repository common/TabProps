<ReactionModel type="Spitfire">
  <CanteraInputFile>gri30.cti</CanteraInputFile>
  <textFileDirectory>spitfire_txt_files</textFileDirectory>
  <order>3</order>
  <tableName>SpitfireTable</tableName>
  <SelectForOutput>temperature enthalpy</SelectForOutput>
  <SelectSpeciesForOutput>CH4 O2 H2O H</SelectSpeciesForOutput>
</ReactionModel>
