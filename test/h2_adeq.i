<ReactionModel type="AdiabaticEquilibrium">

  <nfpts>51</nfpts>

  <CanteraInputFile>h2o2.cti</CanteraInputFile>

  <FuelComposition type="MoleFraction">
    <Species name="H2" >0.8</Species>
    <Species name="H2O">0.8</Species>
  </FuelComposition>

  <FuelTemperature>300</FuelTemperature>

  <OxidizerComposition type="MoleFraction">
    <Species name="O2">0.21</Species>
    <Species name="AR">0.79</Species>
  </OxidizerComposition>

  <OxidizerTemperature>400</OxidizerTemperature>

  <SelectForOutput>
    temperature
    density
    MolecularWeight
    enthalpy
    species
    ReactionRate
    SpeciesEnthalpy
  </SelectForOutput>
  
</ReactionModel>