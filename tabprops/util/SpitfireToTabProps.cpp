#include <fstream>
#include <iostream>
#include <tabprops/StateTable.h>
#include <tabprops/TabProps.h>
#include <boost/program_options.hpp>

namespace po = boost::program_options;

template<typename T>
std::vector<T> read_file(const std::string &file_name) {
    std::vector<T> result;
    {
        std::ifstream infile(file_name);
        std::string line;
        if (infile.is_open()) {
            while (std::getline(infile, line)) {
                std::istringstream iss(line);
                T this_value;
                iss >> this_value;
                result.push_back(this_value);
            }
        } else throw std::runtime_error("Unable to open file " + file_name);
    }
    return result;
}

std::map<std::string, std::vector<double> >
read_bulkdata(const std::vector<std::string> &var_names, const bool isIndepVar, const std::string &outdir) {
    std::map<std::string, std::vector<double> > value_map;
    std::string file_prefix;
    isIndepVar ? file_prefix = "ivar_" : file_prefix = "dvar_";
    for (const auto &var : var_names) {
        value_map.insert(std::make_pair(var, read_file<double>(outdir + "/bulkdata_" + file_prefix + var + ".txt")));
    }
    return value_map;
}


int main(int iarg, char *carg[]) {
    std::string tableName, path, verify_variable;
    int order;
    bool allowClipping = true;
    bool tableVerify = true;

    po::options_description desc("Supported Options");
    desc.add_options()
            ("help", "Print help message")
            ("table-name", po::value<std::string>(&tableName)->default_value("SpitfireTable"),
             "The name for the table that is to be generated NOT including the .tbl extension")
            ("path", po::value<std::string>(&path)->default_value("."),
             "Full path to location of Spitfire txt files (defaults to working directory)")
            ("order", po::value<int>(&order)->default_value(3), "Order for interpolation.")
            ("disable-clipping",
             "Don't clip values on independent variables prior to interpolation. The default behavior is to clip independent variables to avoid extrapolation")
            ("disable-verification",
             "Don't loop through the dependent table variables to verify they were interpolated correctly.");


    // parse the command line options
    try {
        po::variables_map args;
        po::store(po::parse_command_line(iarg, carg, desc), args);
        po::notify(args);

        if (args.count("help")) {
            std::cout << std::endl
                      << "--------------------------------------------------------------------" << std::endl
                      << "Spitfire -> StateTable conversion utility." << std::endl
                      << std::endl
                      << "TabProps Version (date): " << TabPropsVersionDate << std::endl
                      << "TabProps Version (hash): " << TabPropsVersionHash << std::endl
                      << "--------------------------------------------------------------------" << std::endl
                      << std::endl
                      << "This imports flamelet files from Spitfire and" << std::endl
                      << "outputs a StateTable suitable for loading into a CFD solver." << std::endl
                      << std::endl
                      << desc << std::endl << std::endl
                      << "Example: Generate a table from files in 'tmp' folder \n"
                      << "         and using third order interpolants:" << std::endl
                      << "   ./sp_loader --path=tmp --order=3" << std::endl
                      << std::endl;
            return 1;
        }
        allowClipping = (args.count("disable-clipping") == 0);
        tableVerify = (args.count("disable-verification") == 0);
    }
    catch (std::exception &err) {
        std::cout << err.what() << std::endl << desc << std::endl;
        return -1;
    }

    //-- Load the library from disk, generate the table, and write it out to disk
    try {
        const auto ivar_names = read_file<std::string>(path + "/metadata_independent_variables.txt");
        if (ivar_names.size() > 5) {
            throw std::runtime_error("Cannot create a table with more than 5 independent variables.");
        }
        const auto dvar_names = read_file<std::string>(path + "/metadata_dependent_variables.txt");

        const auto ivar_map = read_bulkdata(ivar_names, true, path);

        for (const auto &ivar : ivar_names) {
            if (ivar_map.at(ivar).size() <= order) {
                std::ostringstream errmsg;
                errmsg << "ERROR: the mesh must have a minimum of " << order + 1 << " entries in each dimension. \n"
                       << "       Found " << ivar_map.at(ivar).size() << " entries in " << ivar << " dimension.\n";
                throw std::runtime_error(errmsg.str());
            }
        }

        const auto dvar_map = read_bulkdata(dvar_names, false, path);

        StateTable table;
        InterpT *interp;

        for (const auto &dvar : dvar_names) {
            switch (ivar_names.size()) {
                case 1:
                    if (ivar_map.at(ivar_names[0]).size() != dvar_map.at(dvar).size()) {
                        throw std::runtime_error("Dimension mismatch between independent and dependent variables.");
                    }
                    interp = new Interp1D(order,
                                          ivar_map.at(ivar_names[0]),
                                          dvar_map.at(dvar),
                                          allowClipping);
                    break;
                case 2:
                    if (ivar_map.at(ivar_names[0]).size() * ivar_map.at(ivar_names[1]).size() !=
                        dvar_map.at(dvar).size()) {
                        throw std::runtime_error("Dimension mismatch between independent and dependent variables.");
                    }
                    interp = new Interp2D(order,
                                          ivar_map.at(ivar_names[0]),
                                          ivar_map.at(ivar_names[1]),
                                          dvar_map.at(dvar),
                                          allowClipping);
                    break;
                case 3:
                    if (ivar_map.at(ivar_names[0]).size() * ivar_map.at(ivar_names[1]).size() *
                        ivar_map.at(ivar_names[2]).size() != dvar_map.at(dvar).size()) {
                        throw std::runtime_error("Dimension mismatch between independent and dependent variables.");
                    }
                    interp = new Interp3D(order,
                                          ivar_map.at(ivar_names[0]),
                                          ivar_map.at(ivar_names[1]),
                                          ivar_map.at(ivar_names[2]),
                                          dvar_map.at(dvar),
                                          allowClipping);
                    break;
                case 4:
                    if (ivar_map.at(ivar_names[0]).size() * ivar_map.at(ivar_names[1]).size() *
                        ivar_map.at(ivar_names[2]).size() * ivar_map.at(ivar_names[3]).size() !=
                        dvar_map.at(dvar).size()) {
                        throw std::runtime_error("Dimension mismatch between independent and dependent variables.");
                    }
                    interp = new Interp4D(order,
                                          ivar_map.at(ivar_names[0]),
                                          ivar_map.at(ivar_names[1]),
                                          ivar_map.at(ivar_names[2]),
                                          ivar_map.at(ivar_names[3]),
                                          dvar_map.at(dvar),
                                          allowClipping);
                    break;
                case 5:
                    if (ivar_map.at(ivar_names[0]).size() * ivar_map.at(ivar_names[1]).size() *
                        ivar_map.at(ivar_names[2]).size() * ivar_map.at(ivar_names[3]).size() *
                        ivar_map.at(ivar_names[4]).size() != dvar_map.at(dvar).size()) {
                        throw std::runtime_error("Dimension mismatch between independent and dependent variables.");
                    }
                    interp = new Interp5D(order,
                                          ivar_map.at(ivar_names[0]),
                                          ivar_map.at(ivar_names[1]),
                                          ivar_map.at(ivar_names[2]),
                                          ivar_map.at(ivar_names[3]),
                                          ivar_map.at(ivar_names[4]),
                                          dvar_map.at(dvar),
                                          allowClipping);
                    break;
            }
            table.add_entry(dvar, interp, ivar_names, false);
        }

        table.output_table_info(std::cout);
        table.write_table(tableName + ".tbl");


        // verifying the table...
        if (tableVerify) {
            StateTable table_test;
            table_test.read_table(tableName + ".tbl");
            bool pointsInterpolatedCorrectly = true;

            for (const auto &verify_variable : dvar_names) {
                const InterpT *const tblTsp = table_test.find_entry(verify_variable);

                if (tblTsp == NULL) {
                    std::cout << "Could not verify table since " << verify_variable << " was not found in the table."
                              << std::endl;
                    pointsInterpolatedCorrectly = false;
                } else {
                    std::vector<double> pt(ivar_names.size(), 0.0);
                    // get percent difference between the original and interpolated values
                    switch (ivar_names.size()) {
                        case 1:
                            for (int i = 0; i < ivar_map.at(ivar_names[0]).size(); ++i) {
                                pt[0] = ivar_map.at(ivar_names[0])[i];
                                try {
                                    const double dval = dvar_map.at(verify_variable)[i];
                                    double Tperr =
                                            100.0 * (dval - tblTsp->value(pt)) / (dval+1.e-16);
                                    if (std::abs(Tperr) > 1.0e-10) {
                                        std::cout << "PROBLEMS at ivar = [" << pt[0] << "]!  depvar: " << dval
                                                  << ", " << tblTsp->value(pt) << ", %error=" << Tperr << std::endl;
                                        pointsInterpolatedCorrectly = false;
                                    }
                                }
                                catch (std::runtime_error &err) {
                                    std::cout << err.what() << "ERROR interpolating table for ivar1=" << pt[0]
                                              << std::endl;
                                }
                            }
                            break;
                        case 2:
                            for (int j = 0; j < ivar_map.at(ivar_names[1]).size(); ++j) {
                                pt[1] = ivar_map.at(ivar_names[1])[j];
                                for (int i = 0; i < ivar_map.at(ivar_names[0]).size(); ++i) {
                                    pt[0] = ivar_map.at(ivar_names[0])[i];
                                    const size_t idx = i + j * ivar_map.at(ivar_names[0]).size();
                                    try {
                                        const double dval = dvar_map.at(verify_variable)[idx];
                                        double Tperr =
                                                100.0 * (dval - tblTsp->value(pt)) / (dval+1.e-16);
                                        if (std::abs(Tperr) > 1.0e-10) {
                                            std::cout << "PROBLEMS at ivar = [" << pt[0] << ", " << pt[1]
                                                      << "]!  depvar: " << dval << ", " << tblTsp->value(pt)
                                                      << ", %error=" << Tperr << std::endl;
                                            pointsInterpolatedCorrectly = false;
                                        }
                                    }
                                    catch (std::runtime_error &err) {
                                        std::cout << err.what() << "ERROR interpolating table for ivar = [" << pt[0]
                                                  << ", " << pt[1] << "]" << std::endl;
                                    }
                                }
                            }
                            break;
                        case 3:
                            for (int k = 0; k < ivar_map.at(ivar_names[2]).size(); ++k) {
                                pt[2] = ivar_map.at(ivar_names[2])[k];
                                for (int j = 0; j < ivar_map.at(ivar_names[1]).size(); ++j) {
                                    pt[1] = ivar_map.at(ivar_names[1])[j];
                                    for (int i = 0; i < ivar_map.at(ivar_names[0]).size(); ++i) {
                                        pt[0] = ivar_map.at(ivar_names[0])[i];
                                        const size_t idx = i + j * ivar_map.at(ivar_names[0]).size() +
                                                           k * ivar_map.at(ivar_names[0]).size() *
                                                           ivar_map.at(ivar_names[1]).size();
                                        try {
                                            const double dval = dvar_map.at(verify_variable)[idx];
                                            double Tperr =
                                                    100.0 * (dval - tblTsp->value(pt)) / (dval+1.e-16);
                                            if (std::abs(Tperr) > 1.0e-10) {
                                                std::cout << "PROBLEMS at ivar = [" << pt[0] << ", " << pt[1] << ", "
                                                          << pt[2] << "]!  depvar: " << dval << ", "
                                                          << tblTsp->value(pt) << ", %error=" << Tperr << std::endl;
                                                pointsInterpolatedCorrectly = false;
                                            }
                                        }
                                        catch (std::runtime_error &err) {
                                            std::cout << err.what() << "ERROR interpolating table for ivar = [" << pt[0]
                                                      << ", " << pt[1] << ", " << pt[2] << "]" << std::endl;
                                        }
                                    }
                                }
                            }
                            break;
                        case 4:
                            for (int l = 0; l < ivar_map.at(ivar_names[3]).size(); ++l) {
                                pt[3] = ivar_map.at(ivar_names[3])[l];
                                for (int k = 0; k < ivar_map.at(ivar_names[2]).size(); ++k) {
                                    pt[2] = ivar_map.at(ivar_names[2])[k];
                                    for (int j = 0; j < ivar_map.at(ivar_names[1]).size(); ++j) {
                                        pt[1] = ivar_map.at(ivar_names[1])[j];
                                        for (int i = 0; i < ivar_map.at(ivar_names[0]).size(); ++i) {
                                            pt[0] = ivar_map.at(ivar_names[0])[i];
                                            const size_t idx = i + j * ivar_map.at(ivar_names[0]).size() +
                                                               k * ivar_map.at(ivar_names[0]).size() *
                                                               ivar_map.at(ivar_names[1]).size() +
                                                               l * ivar_map.at(ivar_names[0]).size() *
                                                               ivar_map.at(ivar_names[1]).size() *
                                                               ivar_map.at(ivar_names[2]).size();
                                            try {
                                                const double dval = dvar_map.at(verify_variable)[idx];
                                                double Tperr =
                                                        100.0 * (dval - tblTsp->value(pt)) / (dval+1.e-16);
                                                if (std::abs(Tperr) > 1.0e-10) {
                                                    std::cout << "PROBLEMS at ivar = [" << pt[0] << ", " << pt[1]
                                                              << ", " << pt[2] << ", " << pt[3] << "]!  depvar: "
                                                              << dval << ", " << tblTsp->value(pt) << ", %error="
                                                              << Tperr << std::endl;
                                                    pointsInterpolatedCorrectly = false;
                                                }
                                            }
                                            catch (std::runtime_error &err) {
                                                std::cout << err.what() << "ERROR interpolating table for ivar = ["
                                                          << pt[0] << ", " << pt[1] << ", " << pt[2] << ", " << pt[3]
                                                          << "]" << std::endl;
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        case 5:
                            for (int m = 0; m < ivar_map.at(ivar_names[4]).size(); ++m) {
                                pt[4] = ivar_map.at(ivar_names[4])[m];
                                for (int l = 0; l < ivar_map.at(ivar_names[3]).size(); ++l) {
                                    pt[3] = ivar_map.at(ivar_names[3])[l];
                                    for (int k = 0; k < ivar_map.at(ivar_names[2]).size(); ++k) {
                                        pt[2] = ivar_map.at(ivar_names[2])[k];
                                        for (int j = 0; j < ivar_map.at(ivar_names[1]).size(); ++j) {
                                            pt[1] = ivar_map.at(ivar_names[1])[j];
                                            for (int i = 0; i < ivar_map.at(ivar_names[0]).size(); ++i) {
                                                pt[0] = ivar_map.at(ivar_names[0])[i];
                                                const size_t idx = i + j * ivar_map.at(ivar_names[0]).size() +
                                                                   k * ivar_map.at(ivar_names[0]).size() *
                                                                   ivar_map.at(ivar_names[1]).size() +
                                                                   l * ivar_map.at(ivar_names[0]).size() *
                                                                   ivar_map.at(ivar_names[1]).size() *
                                                                   ivar_map.at(ivar_names[2]).size() +
                                                                   m * ivar_map.at(ivar_names[0]).size() *
                                                                   ivar_map.at(ivar_names[1]).size() *
                                                                   ivar_map.at(ivar_names[2]).size() *
                                                                   ivar_map.at(ivar_names[3]).size();
                                                try {
                                                    const double dval = dvar_map.at(verify_variable)[idx];
                                                    double Tperr =
                                                            100.0 * (dval - tblTsp->value(pt)) / (dval+1.e-16);
                                                    if (std::abs(Tperr) > 1.0e-10) {
                                                        std::cout << "PROBLEMS at ivar = [" << pt[0] << ", " << pt[1]
                                                                  << ", " << pt[2] << ", " << pt[3] << ", " << pt[4]
                                                                  << "]!  depvar: " << dval << ", " << tblTsp->value(pt)
                                                                  << ", %error=" << Tperr << std::endl;
                                                        pointsInterpolatedCorrectly = false;
                                                    }
                                                }
                                                catch (std::runtime_error &err) {
                                                    std::cout << err.what() << "ERROR interpolating table for ivar = ["
                                                              << pt[0] << ", " << pt[1] << ", " << pt[2] << ", "
                                                              << pt[3] << ", " << pt[4] << "]" << std::endl;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        default:
                            std::ostringstream errmsg;
                            errmsg << "ERROR: unsupported table dimension!"
                                   << __FILE__ << " : " << __LINE__ << std::endl;
                            throw std::runtime_error(errmsg.str());
                    }

                    if (!pointsInterpolatedCorrectly) {
                        std::cout << "********" << std::endl
                                  << "WARNING: the table does not interpolate the spitfire " << verify_variable
                                  << "data!  This likely indicates a problem with the table!"
                                  << "********" << std::endl;
                    }
                }
            }
            if (pointsInterpolatedCorrectly) {
                std::cout << "The table correctly interpolates the spitfire data." << std::endl;
            }
        }
    }
    catch (std::exception &err) {
        std::cout << std::endl << "ERROR trapped.  Details follow... " << std::endl
                  << err.what() << std::endl;
        return -1;
    }

    return 0;
}
