/**
 *  \file   LagrangeInterpolant.cpp
 *  \date   Jun 24, 2013
 *  \author "James C. Sutherland"
 *
 *
 * The MIT License
 *
 * Copyright (c) 2013-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

#include <tabprops/LagrangeInterpolant.h>
#include <tabprops/LagrangeInterpolant_Poly.h>

#ifdef ENABLE_CUDA

#define LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
#include <tabprops/LagrangeInterpolant_Poly.h>
#undef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA

//#define INNER_BLOCK_ONE_DIM  1024
#define INNER_BLOCK_ONE_DIM  512
#define NBLOCK(n) (n / INNER_BLOCK_ONE_DIM + 1)
#define NTHREAD(n) ((n > INNER_BLOCK_ONE_DIM) ? INNER_BLOCK_ONE_DIM : n)

#endif 
#define TMPVAL( i,j,xvals )( (xvals[i]-xvals[j]) )
// Maximum number of block in one dimension is 65535.
#include <tabprops/Archive.h>

#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <cassert>
#include <limits>

using std::cout;
using std::endl;
using std::vector;
using std::pair; using std::make_pair;

//===================================================================

#ifdef ENABLE_CUDA
#define GPU_ERROR_CHECK(err) { gpu_error( err, __FILE__, __LINE__); }
inline void gpu_error(cudaError err, const char *file, int line)
{
  if (cudaSuccess != err){
    std::ostringstream msg;
    msg << "GPU failed!\n"
        << "Cuda message : " << cudaGetErrorString(err) << "\n"
        << "\t - " << file << " : " << line << std::endl;
    throw(std::runtime_error(msg.str()));
  }
}
#endif
//===================================================================

//#define VERBOSE_COMPARE

bool vec_compare( const std::vector<double>& v1, const std::vector<double>& v2 ){
  if( v1.size() != v2.size() ) return false;
  const size_t nval = v1.size();

  double maxVal = 0.0;
  for( size_t i=0; i<nval; ++i )  maxVal += std::max( maxVal, std::abs(v1[i]) );

  const double RTOL = 5e-10;

# ifdef VERBOSE_COMPARE
  size_t ndiff=0;
# endif
  for( size_t i=0; i<nval; ++i ){
    const double abserr = std::abs( v1[i] - v2[i] );
    const double relerr = abserr / maxVal;
#   ifdef VERBOSE_COMPARE
    if( relerr > RTOL ){
      ++ndiff;
      cout << " differs at location " << i << " : " << v1[i] << " : " << v2[i] << " (" << abserr << ", " << relerr << ")" << std::endl;
    }
#   else
    if( relerr > RTOL ) return false;
#   endif
  }
# ifdef VERBOSE_COMPARE
  if( ndif>0 ) cout << ndiff << " / " << v2.size() << " locations differ\n";
  return ndiff==0;
# else
  return true;
# endif
}

//===================================================================

bool
is_monotonic( const vector<double>& x )
{
  assert( x.size() > 1 );
  if( x[1]>x[0] ){ // presume ascending order
    double lo = x[0];
    for( size_t i=1; i<x.size(); ++i ){
      if( x[i] < lo ) return false;
      lo = x[i];
    }
  }
  else{ // presume descending order
    double hi = x[0];
    for( size_t i=1; i<x.size(); ++i ){
      if( x[i] > hi ) return false;
      hi = x[i];
    }
  }
  return true;
}

//===================================================================

bool
is_increasing( const vector<double>& x )
{
  assert( x.size() > 1 );
  double lo = x[0];
  for( size_t i=1; i<x.size(); ++i ){
    if( x[i] < lo ) return false;
    lo = x[i];
  }
  return true;
}

//===================================================================

void enforce_incresing( const vector<double>& x, const std::string& str )
{
  if( !is_increasing(x) ){
    std::ostringstream msg;
    msg << endl << endl << "ERROR from " << __FILE__ << " : " << __LINE__
        << endl << endl << str << endl
        << "Variable must be monontonically increasing." << endl;
    throw std::invalid_argument( msg.str() );
  }
}

//===================================================================

pair<double,double>
bounds( const vector<double>& x )
{
  assert( is_monotonic(x) );
  const double x1 = x.front();
  const double x2 = x.back();
  return ( x2 > x1 )      ?
      make_pair( x1, x2 ) :  // ascending
      make_pair( x2, x1 ) ;  // descending;
}

//===================================================================

bool is_uniform( const std::vector<double>& xvals )
{
  assert( xvals.size() > 1 );
  double dx = xvals[1]-xvals[0];
  for( size_t i=1; i<xvals.size(); ++i ){
    if( std::abs(dx - (xvals[i]-xvals[i-1])) / dx > 1e-14 ) return false;
  }
  return true;
}


//===================================================================

template<typename Archive> void
LagrangeInterpolant::serialize( Archive& ar, const unsigned version )
{
  ar & order_ & allowClipping_;
}

//===================================================================

LagrangeInterpolant1D::LagrangeInterpolant1D( const unsigned order,
                                              const vector<double>& xvals,
                                              const vector<double>& fvals,
                                              const bool clipValues )
: LagrangeInterpolant( order, clipValues ),
  bounds_( bounds(xvals) ),
  xvals_( xvals ),
  fvals_( fvals ),
  isUniform_( is_uniform(xvals) )
{
  assert( fvals_.size() == xvals_.size() );
  enforce_incresing( xvals_, "1D interpolant independent variable" );
  
# ifdef ENABLE_CUDA
  gpu_malloc();
  cpu2gpu_memcpy();
# endif
}

//-------------------------------------------------------------------

LagrangeInterpolant1D::LagrangeInterpolant1D( const LagrangeInterpolant1D& other )
: LagrangeInterpolant( other.order_, other.allowClipping_ ),
  bounds_( other.bounds_ ),
  xvals_( other.xvals_ ),
  fvals_( other.fvals_ ),
  isUniform_( other.isUniform_ )
{
  assert( fvals_.size() == xvals_.size() );
  
# ifdef ENABLE_CUDA
  gpu_malloc();
  cudaMemcpy(gpu_xvals_, other.gpu_xvals_, sizeof(double) * xvals_.size(), cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_fvals_, other.gpu_fvals_, sizeof(double) * fvals_.size(), cudaMemcpyDeviceToDevice);
  
  cudaMemcpy(gpu_isUniform_,      other.gpu_isUniform_,     sizeof(bool) * 1,   cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_xvalsize_,       other.gpu_xvalsize_,      sizeof(size_t) * 1, cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_bounds_,         other.gpu_bounds_,        sizeof(double) * 2, cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_allowClipping_,  other.gpu_allowClipping_, sizeof(bool),       cudaMemcpyDeviceToDevice);
  
  GPU_ERROR_CHECK(cudaGetLastError());
# endif
}

//-------------------------------------------------------------------

LagrangeInterpolant1D::LagrangeInterpolant1D()
: LagrangeInterpolant(1,true)
{}

//-------------------------------------------------------------------
#ifdef ENABLE_CUDA
bool
LagrangeInterpolant1D::gpu_malloc()
{
  cudaMalloc((void**) &gpu_xvals_, sizeof(double) * xvals_.size());
  cudaMalloc((void**) &gpu_fvals_, sizeof(double) * fvals_.size());
  cudaMalloc((void**) &gpu_isUniform_, sizeof(bool));
  cudaMalloc((void**) &gpu_xvalsize_, sizeof(size_t));
  cudaMalloc((void**) &gpu_bounds_, sizeof(double) * 2);
  cudaMalloc((void**) &gpu_allowClipping_, sizeof(bool));
  
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant1D::cpu2gpu_memcpy()
{
  // Copying from CPU to GPU
  cudaMemcpy(gpu_xvals_, &xvals_[0], sizeof(double) * xvals_.size(), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_fvals_, &fvals_[0], sizeof(double) * fvals_.size(), cudaMemcpyHostToDevice);
  
  cudaMemcpy(gpu_isUniform_, &isUniform_, sizeof(bool), cudaMemcpyHostToDevice);
  
  size_t xvalsize = xvals_.size();
  cudaMemcpy(gpu_xvalsize_, &xvalsize, sizeof(size_t), cudaMemcpyHostToDevice);
  
  const double bounds[2] = {bounds_.first, bounds_.second};
  cudaMemcpy(gpu_bounds_, &bounds[0], sizeof(double) * 2, cudaMemcpyHostToDevice);
  
  cudaMemcpy(gpu_allowClipping_, &allowClipping_, sizeof(bool), cudaMemcpyHostToDevice);
  
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant1D::gpu_value( const std::vector<const double*>& indep, double* values, const size_t indepsize) const
{
  value1D<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(values, indep[0], gpu_xvals_, gpu_fvals_,
                                                     gpu_xvalsize_,
                                                     order_,
                                                     gpu_isUniform_,
                                                     gpu_allowClipping_,
                                                     gpu_bounds_,
                                                     indepsize);
  
  cudaThreadSynchronize();
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant1D::gpu_derivative( const std::vector<const double*>& indep, const int dim, double* values, const size_t indepsize ) const
{
  assert( xvals_.size() > order_ );
  derivative1D<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(values, indep[0], gpu_xvals_, gpu_fvals_,
                                                          gpu_xvalsize_,
                                                          order_,
                                                          gpu_isUniform_,
                                                          gpu_allowClipping_,
                                                          gpu_bounds_,
                                                          indepsize,
                                                          dim);
  
  cudaThreadSynchronize();
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant1D::gpu_second_derivative( const std::vector<const double*>& indep, const int dim1, const int dim2, double* values, const size_t indepsize ) const
{
  assert( xvals_.size() > order_+1 );
  second_derivative1D<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(values, indep[0],gpu_xvals_, gpu_fvals_,
                                                                 gpu_xvalsize_,
                                                                 order_,
                                                                 gpu_isUniform_,
                                                                 gpu_allowClipping_,
                                                                 gpu_bounds_,
                                                                 indepsize,
                                                                 dim1, dim2);
  
  cudaThreadSynchronize();
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}

#endif /* ENABLE_CUDA */

//-------------------------------------------------------------------

LagrangeInterpolant1D::~LagrangeInterpolant1D()
{}

//-------------------------------------------------------------------

vector<pair<double,double> >
LagrangeInterpolant1D::get_bounds() const
{
  vector<pair<double,double> > b;
  b.push_back(bounds_);
  return b;
}

//-------------------------------------------------------------------

bool
LagrangeInterpolant1D::operator==( const LagrangeInterpolant& other ) const
{
  const LagrangeInterpolant1D& a = dynamic_cast<const LagrangeInterpolant1D&>(other);
  return order_ == a.get_order()
      && allowClipping_ == a.clipping()
      && vec_compare( xvals_, a.xvals_ )
      && vec_compare( fvals_, a.fvals_ )
      && bounds_ == a.bounds_;
}

//-------------------------------------------------------------------

template<typename Archive> void
LagrangeInterpolant1D::serialize( Archive& ar, const unsigned version )
{
  ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP( LagrangeInterpolant )
     & BOOST_SERIALIZATION_NVP( xvals_ )
     & BOOST_SERIALIZATION_NVP( fvals_ )
     & BOOST_SERIALIZATION_NVP( isUniform_ );
  bounds_ = bounds(xvals_);
# ifdef ENABLE_CUDA
  gpu_malloc();
  cpu2gpu_memcpy();
# endif
  
}

//===================================================================

LagrangeInterpolant2D::LagrangeInterpolant2D( const unsigned order,
                                              const vector<double>& xvals,
                                              const vector<double>& yvals,
                                              const vector<double>& fvals,
                                              const bool clipValues )
: LagrangeInterpolant(order,clipValues),
  xbounds_( bounds(xvals) ),
  ybounds_( bounds(yvals) ),
  xvals_( xvals ),
  yvals_( yvals ),
  fvals_( fvals ),
  isUniformX_( is_uniform(xvals) ),
  isUniformY_( is_uniform(yvals) )
{
  assert( fvals_.size() == xvals_.size() * yvals_.size() );

  assert( xvals_.size() > order_+1 );
  assert( yvals_.size() > order_+1 );

  enforce_incresing( xvals_, "2D interpolant independent variable 1" );
  enforce_incresing( yvals_, "2D interpolant independent variable 2" );

  xcoef1d_.resize( order_+1 );
  ycoef1d_.resize( order_+1 );
# ifdef ENABLE_CUDA
  gpu_malloc();
  cpu2gpu_memcpy();
# endif

}

//-------------------------------------------------------------------

LagrangeInterpolant2D::LagrangeInterpolant2D( const LagrangeInterpolant2D& other )
: LagrangeInterpolant(other.order_,other.allowClipping_),
  xbounds_( other.xbounds_ ),
  ybounds_( other.ybounds_ ),
  xvals_( other.xvals_ ),
  yvals_( other.yvals_ ),
  fvals_( other.fvals_ ),
  isUniformX_( other.isUniformX_ ),
  isUniformY_( other.isUniformY_ )
{
  assert( fvals_.size() == xvals_.size() * yvals_.size() );

  xcoef1d_.resize( order_+1 );
  ycoef1d_.resize( order_+1 );
  
# ifdef ENABLE_CUDA
  gpu_malloc();
  cudaMemcpy(gpu_xvals_, other.gpu_xvals_, sizeof(double) * xvals_.size(), cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_yvals_, other.gpu_yvals_, sizeof(double) * yvals_.size(), cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_fvals_, other.gpu_fvals_, sizeof(double) * fvals_.size(), cudaMemcpyDeviceToDevice);
  
  cudaMemcpy(gpu_isUniform_,      other.gpu_isUniform_,     sizeof(bool) * 2,   cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_xvalsize_,       other.gpu_xvalsize_,      sizeof(size_t) * 2, cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_bounds_,         other.gpu_bounds_,        sizeof(double) * 4, cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_allowClipping_,  other.gpu_allowClipping_, sizeof(bool),       cudaMemcpyDeviceToDevice);
  
  GPU_ERROR_CHECK(cudaGetLastError());
# endif
}

//-------------------------------------------------------------------

LagrangeInterpolant2D::LagrangeInterpolant2D()
: LagrangeInterpolant(1,true)
{}

//-------------------------------------------------------------------
#ifdef ENABLE_CUDA
bool
LagrangeInterpolant2D::gpu_malloc()
{
  cudaMalloc((void**) &gpu_xvals_, sizeof(double) * xvals_.size());
  cudaMalloc((void**) &gpu_yvals_, sizeof(double) * yvals_.size());
  cudaMalloc((void**) &gpu_fvals_, sizeof(double) * fvals_.size());
  
  cudaMalloc((void**) &gpu_isUniform_, sizeof(bool) * 2);
  cudaMalloc((void**) &gpu_xvalsize_, sizeof(size_t) * 2);
  cudaMalloc((void**) &gpu_bounds_, sizeof(double) * 4);
  cudaMalloc((void**) &gpu_allowClipping_, sizeof(bool));
  
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant2D::cpu2gpu_memcpy()
{
  // Copying from CPU to GPU
  cudaMemcpy(gpu_xvals_, &xvals_[0], sizeof(double) * xvals_.size(), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_yvals_, &yvals_[0], sizeof(double) * yvals_.size(), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_fvals_, &fvals_[0], sizeof(double) * fvals_.size(), cudaMemcpyHostToDevice);
  
  bool isUniform[2] = {isUniformX_, isUniformY_};
  cudaMemcpy(gpu_isUniform_, &isUniform[0], sizeof(bool) * 2, cudaMemcpyHostToDevice);
  
  const size_t xvalsize[2] = {xvals_.size(), yvals_.size()};
  cudaMemcpy(gpu_xvalsize_, &xvalsize[0], sizeof(size_t) * 2, cudaMemcpyHostToDevice);
  
  const double bounds[4] = {xbounds_.first, xbounds_.second, ybounds_.first, ybounds_.second};
  cudaMemcpy(gpu_bounds_, &bounds[0], sizeof(double) * 4, cudaMemcpyHostToDevice);
  
  cudaMemcpy(gpu_allowClipping_, &allowClipping_, sizeof(bool), cudaMemcpyHostToDevice);
  
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant2D::gpu_value( const std::vector<const double *>& indep, double* values, const size_t indepsize) const
{
  value2D<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(values, indep[0], indep[1],
                                                     gpu_xvals_, gpu_yvals_, gpu_fvals_,
                                                     gpu_xvalsize_,
                                                     order_,
                                                     gpu_isUniform_,
                                                     gpu_allowClipping_,
                                                     gpu_bounds_,
                                                     indepsize);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant2D::gpu_derivative( const std::vector<const double*>& indep, const int dim, double* values, const size_t indepsize ) const
{
  derivative2D<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(values, indep[0], indep[1],
                                                          gpu_xvals_, gpu_yvals_, gpu_fvals_,
                                                          gpu_xvalsize_,
                                                          order_,
                                                          gpu_isUniform_,
                                                          gpu_allowClipping_,
                                                          gpu_bounds_,
                                                          indepsize,
                                                          dim);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant2D::gpu_second_derivative( const std::vector<const double*>& indep, const int dim1, const int dim2, double* values, const size_t indepsize ) const
{
  second_derivative2D<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(values, indep[0], indep[1],
                                                                 gpu_xvals_, gpu_yvals_, gpu_fvals_,
                                                                 gpu_xvalsize_,
                                                                 order_,
                                                                 gpu_isUniform_,
                                                                 gpu_allowClipping_,
                                                                 gpu_bounds_,
                                                                 indepsize,
                                                                 dim1, dim2);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}

#endif

//-------------------------------------------------------------------

LagrangeInterpolant2D::~LagrangeInterpolant2D()
{}

//-------------------------------------------------------------------

vector<pair<double,double> >
LagrangeInterpolant2D::get_bounds() const
{
  vector<pair<double,double> > b;
  b.push_back( bounds(xvals_) );
  b.push_back( bounds(yvals_) );
  return b;
}

//-------------------------------------------------------------------

bool
LagrangeInterpolant2D::operator==( const LagrangeInterpolant& other ) const
{
  const LagrangeInterpolant2D& a = dynamic_cast<const LagrangeInterpolant2D&>(other);
  return order_ == a.get_order()
      && allowClipping_ == a.clipping()
      && vec_compare( xvals_, a.xvals_ )
      && vec_compare( yvals_, a.yvals_ )
      && vec_compare( fvals_, a.fvals_ )
      && xbounds_ == a.xbounds_
      && ybounds_ == a.ybounds_;
}

//-------------------------------------------------------------------

template<typename Archive> void
LagrangeInterpolant2D::serialize( Archive& ar, const unsigned version )
{
  ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP( LagrangeInterpolant )
     & BOOST_SERIALIZATION_NVP( xvals_ )
     & BOOST_SERIALIZATION_NVP( yvals_ )
     & BOOST_SERIALIZATION_NVP( fvals_ )
     & BOOST_SERIALIZATION_NVP( isUniformX_ )
     & BOOST_SERIALIZATION_NVP( isUniformY_ );
  xbounds_ = bounds(xvals_);
  ybounds_ = bounds(yvals_);
  xcoef1d_.resize( order_+1 );
  ycoef1d_.resize( order_+1 );
# ifdef ENABLE_CUDA
  gpu_malloc();
  cpu2gpu_memcpy();
# endif
}

//===================================================================

LagrangeInterpolant3D::LagrangeInterpolant3D( const unsigned order,
                                              const vector<double>& xvals,
                                              const vector<double>& yvals,
                                              const vector<double>& zvals,
                                              const vector<double>& fvals,
                                              const bool clipValues )
: LagrangeInterpolant(order,clipValues),
  xbounds_( bounds(xvals) ),
  ybounds_( bounds(yvals) ),
  zbounds_( bounds(zvals) ),
  xvals_( xvals ),
  yvals_( yvals ),
  zvals_( zvals ),
  fvals_( fvals ),
  isUniformX_( is_uniform(xvals) ),
  isUniformY_( is_uniform(yvals) ),
  isUniformZ_( is_uniform(zvals) )
{
  assert( fvals_.size() == xvals_.size() * yvals_.size() * zvals_.size() );

  enforce_incresing( xvals_, "3D interpolant independent variable 1" );
  enforce_incresing( yvals_, "3D interpolant independent variable 2" );
  enforce_incresing( zvals_, "3D interpolant independent variable 3" );

  xcoef1d_.resize( order_+1 );
  ycoef1d_.resize( order_+1 );
  zcoef1d_.resize( order_+1 );
# ifdef ENABLE_CUDA
  gpu_malloc();
  cpu2gpu_memcpy();
# endif
}

//-------------------------------------------------------------------

LagrangeInterpolant3D::LagrangeInterpolant3D( const LagrangeInterpolant3D& other )
: LagrangeInterpolant(other.order_,other.allowClipping_),
  xbounds_( other.xbounds_ ),
  ybounds_( other.ybounds_ ),
  zbounds_( other.zbounds_ ),
  xvals_( other.xvals_ ),
  yvals_( other.yvals_ ),
  zvals_( other.zvals_ ),
  fvals_( other.fvals_ ),
  isUniformX_( other.isUniformX_ ),
  isUniformY_( other.isUniformY_ ),
  isUniformZ_( other.isUniformZ_ )
{
  assert( fvals_.size() == xvals_.size() * yvals_.size() * zvals_.size() );

  xcoef1d_.resize( order_+1 );
  ycoef1d_.resize( order_+1 );
  zcoef1d_.resize( order_+1 );
  
# ifdef ENABLE_CUDA
  gpu_malloc();
  cudaMemcpy(gpu_xvals_, other.gpu_xvals_, sizeof(double) * xvals_.size(), cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_yvals_, other.gpu_yvals_, sizeof(double) * yvals_.size(), cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_zvals_, other.gpu_zvals_, sizeof(double) * zvals_.size(), cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_fvals_, other.gpu_fvals_, sizeof(double) * fvals_.size(), cudaMemcpyDeviceToDevice);
  
  cudaMemcpy(gpu_isUniform_,      other.gpu_isUniform_,     sizeof(bool) * 3,   cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_xvalsize_,       other.gpu_xvalsize_,      sizeof(size_t) * 3, cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_bounds_,         other.gpu_bounds_,        sizeof(double) * 6, cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_allowClipping_,  other.gpu_allowClipping_, sizeof(bool),       cudaMemcpyDeviceToDevice);
  
  GPU_ERROR_CHECK(cudaGetLastError());
# endif
}

//-------------------------------------------------------------------

LagrangeInterpolant3D::LagrangeInterpolant3D()
: LagrangeInterpolant(1,true)
{}

//-------------------------------------------------------------------
#ifdef ENABLE_CUDA
bool
LagrangeInterpolant3D::gpu_malloc()
{
  cudaMalloc((void**) &gpu_xvals_, sizeof(double) * xvals_.size());
  cudaMalloc((void**) &gpu_yvals_, sizeof(double) * yvals_.size());
  cudaMalloc((void**) &gpu_zvals_, sizeof(double) * zvals_.size());
  cudaMalloc((void**) &gpu_fvals_, sizeof(double) * fvals_.size());
  
  cudaMalloc((void**) &gpu_isUniform_, sizeof(double) * 3);
  cudaMalloc((void**) &gpu_xvalsize_, sizeof(size_t) * 3);
  cudaMalloc((void**) &gpu_bounds_, sizeof(double) * 6);
  cudaMalloc((void**) &gpu_allowClipping_, sizeof(bool));
  
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant3D::cpu2gpu_memcpy()
{
  // Copying from CPU to GPU memoris
  cudaMemcpy(gpu_xvals_, &xvals_[0], sizeof(double) * xvals_.size(), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_yvals_, &yvals_[0], sizeof(double) * yvals_.size(), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_zvals_, &zvals_[0], sizeof(double) * zvals_.size(), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_fvals_, &fvals_[0], sizeof(double) * fvals_.size(), cudaMemcpyHostToDevice);
  
  bool isUniform[3] = {isUniformX_, isUniformY_, isUniformZ_};
  cudaMemcpy(gpu_isUniform_, &isUniform[0], sizeof(bool) * 3, cudaMemcpyHostToDevice);
  
  const size_t xvalsize[3] = {xvals_.size(), yvals_.size(), zvals_.size()};
  cudaMemcpy(gpu_xvalsize_, &xvalsize[0], sizeof(size_t) * 3, cudaMemcpyHostToDevice);
  
  const double bounds[6] = {xbounds_.first, xbounds_.second, ybounds_.first, ybounds_.second, zbounds_.first, zbounds_.second};
  cudaMemcpy(gpu_bounds_, &bounds[0], sizeof(double) * 6, cudaMemcpyHostToDevice);
  
  cudaMemcpy(gpu_allowClipping_, &allowClipping_, sizeof(bool), cudaMemcpyHostToDevice);
  
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant3D::gpu_value( const std::vector<const double *>& indep, double* values, const size_t indepsize) const
{
  value3D<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(values, indep[0], indep[1], indep[2],
                                                     gpu_xvals_, gpu_yvals_, gpu_zvals_, gpu_fvals_,
                                                     gpu_xvalsize_,
                                                     order_,
                                                     gpu_isUniform_,
                                                     gpu_allowClipping_,
                                                     gpu_bounds_,
                                                     indepsize);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant3D::gpu_derivative( const std::vector<const double*>& indep, const int dim, double* values, const size_t indepsize ) const
{
  derivative3D<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(values, indep[0], indep[1], indep[2],
                                                          gpu_xvals_, gpu_yvals_, gpu_zvals_, gpu_fvals_,
                                                          gpu_xvalsize_,
                                                          order_,
                                                          gpu_isUniform_,
                                                          gpu_allowClipping_,
                                                          gpu_bounds_,
                                                          indepsize,
                                                          dim);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}

//-------------------------------------------------------------------
bool
LagrangeInterpolant3D::gpu_second_derivative( const std::vector<const double*>& indep, const int dim1, const int dim2, double* values, const size_t indepsize ) const
{
  second_derivative3D<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(values, indep[0], indep[1], indep[2],
                                                                 gpu_xvals_, gpu_yvals_, gpu_zvals_, gpu_fvals_,
                                                                 gpu_xvalsize_,
                                                                 order_,
                                                                 gpu_isUniform_,
                                                                 gpu_allowClipping_,
                                                                 gpu_bounds_,
                                                                 indepsize,
                                                                 dim1,
                                                                 dim2);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
#endif
//-------------------------------------------------------------------

LagrangeInterpolant3D::~LagrangeInterpolant3D()
{}

//-------------------------------------------------------------------

vector<pair<double,double> >
LagrangeInterpolant3D::get_bounds() const
{
  vector<pair<double,double> > bounds;
  bounds.push_back( xbounds_ );
  bounds.push_back( ybounds_ );
  bounds.push_back( zbounds_ );
  return bounds;
}

//-------------------------------------------------------------------

bool
LagrangeInterpolant3D::operator==( const LagrangeInterpolant& other ) const
{
  const LagrangeInterpolant3D& a = dynamic_cast<const LagrangeInterpolant3D&>(other);

  return order_ == a.get_order()
      && allowClipping_ == a.clipping()
      && vec_compare(xvals_, a.xvals_)
      && vec_compare(yvals_, a.yvals_)
      && vec_compare(zvals_, a.zvals_)
      && vec_compare(fvals_, a.fvals_)
      && xbounds_ == a.xbounds_
      && ybounds_ == a.ybounds_
      && zbounds_ == a.zbounds_;
}

//-------------------------------------------------------------------

template<typename Archive> void
LagrangeInterpolant3D::serialize( Archive& ar, const unsigned version )
{
  ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP( LagrangeInterpolant )
     & BOOST_SERIALIZATION_NVP( xvals_ )
     & BOOST_SERIALIZATION_NVP( yvals_ )
     & BOOST_SERIALIZATION_NVP( zvals_ )
     & BOOST_SERIALIZATION_NVP( fvals_ )
     & BOOST_SERIALIZATION_NVP( isUniformX_)
     & BOOST_SERIALIZATION_NVP( isUniformY_)
     & BOOST_SERIALIZATION_NVP( isUniformZ_);

  xbounds_ = bounds(xvals_);
  ybounds_ = bounds(yvals_);
  zbounds_ = bounds(zvals_);

  xcoef1d_.resize( order_+1 );
  ycoef1d_.resize( order_+1 );
  zcoef1d_.resize( order_+1 );
  
# ifdef ENABLE_CUDA
  gpu_malloc();
  cpu2gpu_memcpy();
# endif

}

//===================================================================

LagrangeInterpolant4D::LagrangeInterpolant4D( const unsigned order,
                                              const vector<double>& x1vals,
                                              const vector<double>& x2vals,
                                              const vector<double>& x3vals,
                                              const vector<double>& x4vals,
                                              const vector<double>& fvals,
                                              const bool clipValues )
: LagrangeInterpolant(order,clipValues),
  x1vals_( x1vals ),
  x2vals_( x2vals ),
  x3vals_( x3vals ),
  x4vals_( x4vals ),
  fvals_ ( fvals  )
{
  assert( fvals_.size() == x1vals_.size() * x2vals_.size() * x3vals_.size() * x4vals_.size() );

  enforce_incresing( x1vals_, "4D interpolant independent variable 1" );
  enforce_incresing( x2vals_, "4D interpolant independent variable 2" );
  enforce_incresing( x3vals_, "4D interpolant independent variable 3" );
  enforce_incresing( x4vals_, "4D interpolant independent variable 4" );

  x1coef1d_.resize( order_+1 );
  x2coef1d_.resize( order_+1 );
  x3coef1d_.resize( order_+1 );
  x4coef1d_.resize( order_+1 );

  isUniform_[0] = is_uniform(x1vals);
  isUniform_[1] = is_uniform(x2vals);
  isUniform_[2] = is_uniform(x3vals);
  isUniform_[3] = is_uniform(x4vals);

  bounds_.push_back( bounds(x1vals) );
  bounds_.push_back( bounds(x2vals) );
  bounds_.push_back( bounds(x3vals) );
  bounds_.push_back( bounds(x4vals) );
  
# ifdef ENABLE_CUDA
  gpu_malloc();
  cpu2gpu_memcpy();
# endif
}

//-------------------------------------------------------------------

LagrangeInterpolant4D::LagrangeInterpolant4D( const LagrangeInterpolant4D& other )
: LagrangeInterpolant(other.order_,other.allowClipping_),
  bounds_( other.bounds_ ),
  x1vals_( other.x1vals_ ),
  x2vals_( other.x2vals_ ),
  x3vals_( other.x3vals_ ),
  x4vals_( other.x4vals_ ),
  fvals_ ( other.fvals_  )
{
  assert( fvals_.size() == x1vals_.size() * x2vals_.size() * x3vals_.size() * x4vals_.size() );

  x1coef1d_.resize( order_+1 );
  x2coef1d_.resize( order_+1 );
  x3coef1d_.resize( order_+1 );
  x4coef1d_.resize( order_+1 );

  for( short i=0; i<4; ++i ) isUniform_[i] = other.isUniform_[i];
  
# ifdef ENABLE_CUDA
  gpu_malloc();
  cudaMemcpy(gpu_x1vals_, other.gpu_x1vals_, sizeof(double) * x1vals_.size(), cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_x2vals_, other.gpu_x2vals_, sizeof(double) * x2vals_.size(), cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_x3vals_, other.gpu_x3vals_, sizeof(double) * x3vals_.size(), cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_x4vals_, other.gpu_x4vals_, sizeof(double) * x4vals_.size(), cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_fvals_,  other.gpu_fvals_,  sizeof(double) * fvals_.size(),  cudaMemcpyDeviceToDevice);
  
  cudaMemcpy(gpu_isUniform_,      other.gpu_isUniform_,     sizeof(bool) * 4,   cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_xvalsize_,       other.gpu_xvalsize_,      sizeof(size_t) * 4, cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_bounds_,         other.gpu_bounds_,        sizeof(double) * 8, cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_allowClipping_,  other.gpu_allowClipping_, sizeof(bool),       cudaMemcpyDeviceToDevice);
  
  GPU_ERROR_CHECK(cudaGetLastError());
# endif
}

//-------------------------------------------------------------------

LagrangeInterpolant4D::LagrangeInterpolant4D()
: LagrangeInterpolant(1,true)
{}

//-------------------------------------------------------------------
#ifdef ENABLE_CUDA
bool
LagrangeInterpolant4D::gpu_malloc()
{
  cudaMalloc((void**) &gpu_x1vals_, sizeof(double) * x1vals_.size());
  cudaMalloc((void**) &gpu_x2vals_, sizeof(double) * x2vals_.size());
  cudaMalloc((void**) &gpu_x3vals_, sizeof(double) * x3vals_.size());
  cudaMalloc((void**) &gpu_x4vals_, sizeof(double) * x4vals_.size());
  cudaMalloc((void**) &gpu_fvals_,  sizeof(double) * fvals_.size());
  
  cudaMalloc((void**) &gpu_isUniform_, sizeof(double) * 4);
  cudaMalloc((void**) &gpu_xvalsize_, sizeof(size_t) * 4);
  cudaMalloc((void**) &gpu_bounds_, sizeof(double) * 8);
  cudaMalloc((void**) &gpu_allowClipping_, sizeof(bool));
  
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant4D::cpu2gpu_memcpy()
{
  cudaMemcpy(gpu_x1vals_, &x1vals_[0], sizeof(double) * x1vals_.size(), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_x2vals_, &x2vals_[0], sizeof(double) * x2vals_.size(), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_x3vals_, &x3vals_[0], sizeof(double) * x3vals_.size(), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_x4vals_, &x4vals_[0], sizeof(double) * x4vals_.size(), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_fvals_,  &fvals_[0],  sizeof(double) * fvals_.size(),  cudaMemcpyHostToDevice);
  
  cudaMemcpy(gpu_isUniform_, &isUniform_[0], sizeof(bool) * 4, cudaMemcpyHostToDevice);
  
  size_t xvalsize[4] = {x1vals_.size(), x2vals_.size(), x3vals_.size(), x4vals_.size()};
  cudaMemcpy(gpu_xvalsize_, &xvalsize[0], sizeof(size_t) * 4, cudaMemcpyHostToDevice);
  
  const double bounds[8] = {bounds_[0].first, bounds_[0].second, bounds_[1].first, bounds_[1].second,
    bounds_[2].first, bounds_[2].second, bounds_[3].first, bounds_[3].second};
  cudaMemcpy(gpu_bounds_, &bounds[0], sizeof(double) * 8, cudaMemcpyHostToDevice);
  
  cudaMemcpy(gpu_allowClipping_, &allowClipping_, sizeof(bool), cudaMemcpyHostToDevice);
  
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant4D::gpu_value( const std::vector<const double *>& indep, double* values, const size_t indepsize) const
{
  
  value4D<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(values, indep[0], indep[1], indep[2], indep[3],
                                                     gpu_x1vals_, gpu_x2vals_, gpu_x3vals_, gpu_x4vals_, gpu_fvals_,
                                                     gpu_xvalsize_,
                                                     order_,
                                                     gpu_isUniform_,
                                                     gpu_allowClipping_,
                                                     gpu_bounds_,
                                                     indepsize);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant4D::gpu_derivative( const std::vector<const double*>& indep, const int dim, double* values, const size_t indepsize ) const
{
  derivative4D<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(values, indep[0], indep[1], indep[2], indep[3],
                                                          gpu_x1vals_, gpu_x2vals_, gpu_x3vals_, gpu_x4vals_, gpu_fvals_,
                                                          gpu_xvalsize_,
                                                          order_,
                                                          gpu_isUniform_,
                                                          gpu_allowClipping_,
                                                          gpu_bounds_,
                                                          indepsize,
                                                          dim);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant4D::gpu_second_derivative( const std::vector<const double*>& indep, const int dim1, const int dim2, double* values, const size_t indepsize ) const
{
  second_derivative4D<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(values, indep[0], indep[1], indep[2], indep[3],
                                                                 gpu_x1vals_, gpu_x2vals_, gpu_x3vals_, gpu_x4vals_, gpu_fvals_,
                                                                 gpu_xvalsize_,
                                                                 order_,
                                                                 gpu_isUniform_,
                                                                 gpu_allowClipping_,
                                                                 gpu_bounds_,
                                                                 indepsize,
                                                                 dim1, dim2);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
#endif
//-------------------------------------------------------------------

double
LagrangeInterpolant4D::second_derivative( const double* const indep, const int dim1, const int dim2 ) const
{
  if( order_ == 1 ) return 0;

  assert( dim1 < 4 );
  assert( dim2 < 4 );

  const double x1 = allowClipping_ ? std::min( std::max( indep[0], bounds_[0].first ), bounds_[0].second ) : indep[0];
  const double x2 = allowClipping_ ? std::min( std::max( indep[1], bounds_[1].first ), bounds_[1].second ) : indep[1];
  const double x3 = allowClipping_ ? std::min( std::max( indep[2], bounds_[2].first ), bounds_[2].second ) : indep[2];
  const double x4 = allowClipping_ ? std::min( std::max( indep[3], bounds_[3].first ), bounds_[3].second ) : indep[3];

  size_t i1lo, i2lo, i3lo, i4lo;

  switch( dim1 ){
    case 0:{
      switch( dim2 ){
        case 0:
          i1lo = second_der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =            basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =            basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =            basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          break;
        case 1:
          i1lo = der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo = der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          break;
        case 2:
          i1lo = der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo = der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          break;
        case 3:
          i1lo = der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo = der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          break;
      }
      break;
    } // case 0 on switch(dim1)
    case 1:{
      switch( dim2 ){
        case 0:
          i1lo = der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo = der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          break;
        case 1:
          i1lo =            basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo = second_der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =            basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =            basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          break;
        case 2:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo = der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo = der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          break;
        case 3:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo = der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo = der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          break;
      }
      break;
    } // case 1 on switch(dim1)
    case 2:{
      switch( dim2 ){
        case 0:
          i1lo = der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo = der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          break;
        case 1:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo = der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo = der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          break;
        case 2:
          i1lo =            basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =            basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo = second_der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =            basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          break;
        case 3:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo = der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo = der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          break;
      }
      break;
    } // case 2 on switch(dim1)
    case 3:{
      switch( dim2 ){
        case 0:
          i1lo = der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo = der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          break;
        case 1:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo = der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo = der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          break;
        case 2:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo = der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo = der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          break;
        case 3:
          i1lo =            basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =            basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =            basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo = second_der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          break;
      }
      break;
    } // case 3 on switch(dim1)
    default:
      throw std::invalid_argument("invalid dimension specification for second derivative");
  }

  const size_t nx1 = x1vals_.size();
  const size_t nx2 = x2vals_.size();
  const size_t nx3 = x3vals_.size();

  double val = 0.0;

  for( size_t k4=0; k4<=order_; ++k4 ){
    const size_t i4 = (i4lo+k4)*nx1*nx2*nx3;
    for( size_t k3=0; k3<=order_; ++k3 ){
      const double tmp = x3coef1d_[k3] * x4coef1d_[k4];
      const size_t i3 = (i3lo+k3)*nx1*nx2;
      for( size_t k2=0; k2<=order_; ++k2 ){
        const double tmp2 = x2coef1d_[k2] * tmp;
        const size_t i2 = (i2lo+k2)*nx1;
        for( size_t k1=0; k1<=order_; ++k1 ){
          const size_t i1 = i1lo+k1 + i2 + i3 + i4;
#         ifndef NDEBUG
          assert( i1 < fvals_.size() );
#         endif
          val += fvals_[i1] * x1coef1d_[k1] * tmp2;
        }
      }
    }
  }
  return val;
}

//-------------------------------------------------------------------

LagrangeInterpolant4D::~LagrangeInterpolant4D()
{}

//-------------------------------------------------------------------

vector<pair<double,double> >
LagrangeInterpolant4D::get_bounds() const
{
  return bounds_;
}

//-------------------------------------------------------------------

bool
LagrangeInterpolant4D::operator==( const LagrangeInterpolant& other ) const
{
  const LagrangeInterpolant4D& a = dynamic_cast<const LagrangeInterpolant4D&>(other);
;
# ifdef ENABLE_CUDA
//  match = match && gpu_vec_compare(gpu_x1vals_,  a.gpu_x1vals_, x1vals_.size());
//  match = match && gpu_vec_compare(gpu_x2vals_,  a.gpu_x2vals_, x2vals_.size());
//  match = match && gpu_vec_compare(gpu_x3vals_,  a.gpu_x3vals_, x3vals_.size());
//  match = match && gpu_vec_compare(gpu_x4vals_,  a.gpu_x4vals_, x4vals_.size());
//  
//  match = match && gpu_vec_compare(gpu_bounds_,  a.gpu_bounds_, 8);
//  match = match && gpu_vec_compare(gpu_fvals_,   a.gpu_fvals_,  fvals_.size());
//  
//   gpu_vec_compare function must get overloaded for size_t
//  match = match && gpu_vec_compare(gpu_xvalsize_,  a.gpu_xvalsize_,  4);
//  
//  match = match && gpu_vec_compare(gpu_isUniform_, a.gpu_isUniform_, 4);

# endif
  return order_ == a.get_order()
      && allowClipping_ == a.clipping()
      && vec_compare( x1vals_, a.x1vals_ )
      && vec_compare( x2vals_, a.x2vals_ )
      && vec_compare( x3vals_, a.x3vals_ )
      && vec_compare( x4vals_, a.x4vals_ )
      && vec_compare( fvals_ , a.fvals_  )
      && bounds_ == a.bounds_;
}

//-------------------------------------------------------------------

template<typename Archive> void
LagrangeInterpolant4D::serialize( Archive& ar, const unsigned version )
{
  ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP( LagrangeInterpolant )
     & BOOST_SERIALIZATION_NVP( x1vals_ )
     & BOOST_SERIALIZATION_NVP( x2vals_ )
     & BOOST_SERIALIZATION_NVP( x3vals_ )
     & BOOST_SERIALIZATION_NVP( x4vals_ )
     & BOOST_SERIALIZATION_NVP( fvals_  )
     & BOOST_SERIALIZATION_NVP( isUniform_ );

  bounds_.clear();
  bounds_.push_back( bounds(x1vals_) );
  bounds_.push_back( bounds(x2vals_) );
  bounds_.push_back( bounds(x3vals_) );
  bounds_.push_back( bounds(x4vals_) );

  x1coef1d_.resize( order_+1 );
  x2coef1d_.resize( order_+1 );
  x3coef1d_.resize( order_+1 );
  x4coef1d_.resize( order_+1 );
  
# ifdef ENABLE_CUDA
  gpu_malloc();
  cpu2gpu_memcpy();
# endif
}

//===================================================================

LagrangeInterpolant5D::LagrangeInterpolant5D( const unsigned order,
                                              const vector<double>& x1vals,
                                              const vector<double>& x2vals,
                                              const vector<double>& x3vals,
                                              const vector<double>& x4vals,
                                              const vector<double>& x5vals,
                                              const vector<double>& fvals,
                                              const bool clipValues )
: LagrangeInterpolant(order,clipValues),
  x1vals_( x1vals ),
  x2vals_( x2vals ),
  x3vals_( x3vals ),
  x4vals_( x4vals ),
  x5vals_( x5vals ),
  fvals_ ( fvals  )
{
  assert( fvals_.size() == x1vals_.size() * x2vals_.size() * x3vals_.size() * x4vals_.size() * x5vals_.size() );

  enforce_incresing( x1vals_, "4D interpolant independent variable 1" );
  enforce_incresing( x2vals_, "4D interpolant independent variable 2" );
  enforce_incresing( x3vals_, "4D interpolant independent variable 3" );
  enforce_incresing( x4vals_, "4D interpolant independent variable 4" );
  enforce_incresing( x5vals_, "4D interpolant independent variable 5" );

  x1coef1d_.resize( order_+1 );
  x2coef1d_.resize( order_+1 );
  x3coef1d_.resize( order_+1 );
  x4coef1d_.resize( order_+1 );
  x5coef1d_.resize( order_+1 );

  bounds_.push_back( bounds(x1vals_) );
  bounds_.push_back( bounds(x2vals_) );
  bounds_.push_back( bounds(x3vals_) );
  bounds_.push_back( bounds(x4vals_) );
  bounds_.push_back( bounds(x5vals_) );

  isUniform_[0] = is_uniform(x1vals);
  isUniform_[1] = is_uniform(x2vals);
  isUniform_[2] = is_uniform(x3vals);
  isUniform_[3] = is_uniform(x4vals);
  isUniform_[4] = is_uniform(x5vals);
  
# ifdef ENABLE_CUDA
  gpu_malloc();
  cpu2gpu_memcpy();
# endif
}

//-------------------------------------------------------------------

LagrangeInterpolant5D::LagrangeInterpolant5D( const LagrangeInterpolant5D& other )
: LagrangeInterpolant(other.order_,other.allowClipping_),
  bounds_( other.bounds_ ),
  x1vals_( other.x1vals_ ),
  x2vals_( other.x2vals_ ),
  x3vals_( other.x3vals_ ),
  x4vals_( other.x4vals_ ),
  x5vals_( other.x5vals_ ),
  fvals_ ( other.fvals_  )
{
  assert( fvals_.size() == x1vals_.size() * x2vals_.size() * x3vals_.size() * x4vals_.size() * x5vals_.size() );

  x1coef1d_.resize( order_+1 );
  x2coef1d_.resize( order_+1 );
  x3coef1d_.resize( order_+1 );
  x4coef1d_.resize( order_+1 );
  x5coef1d_.resize( order_+1 );

  for( short i=0; i<5; ++i ) isUniform_[i] = other.isUniform_[i];
  
# ifdef ENABLE_CUDA
  gpu_malloc();
  cudaMemcpy(gpu_x1vals_, other.gpu_x1vals_, sizeof(double) * x1vals_.size(), cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_x2vals_, other.gpu_x2vals_, sizeof(double) * x2vals_.size(), cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_x3vals_, other.gpu_x3vals_, sizeof(double) * x3vals_.size(), cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_x4vals_, other.gpu_x4vals_, sizeof(double) * x4vals_.size(), cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_x5vals_, other.gpu_x5vals_, sizeof(double) * x5vals_.size(), cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_fvals_,  other.gpu_fvals_,  sizeof(double) * fvals_.size(),  cudaMemcpyDeviceToDevice);
  
  cudaMemcpy(gpu_isUniform_,      other.gpu_isUniform_,     sizeof(bool) * 5,     cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_xvalsize_,       other.gpu_xvalsize_,      sizeof(size_t) * 5,   cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_bounds_,         other.gpu_bounds_,        sizeof(double) * 10,  cudaMemcpyDeviceToDevice);
  cudaMemcpy(gpu_allowClipping_,  other.gpu_allowClipping_, sizeof(bool),         cudaMemcpyDeviceToDevice);
  
  GPU_ERROR_CHECK(cudaGetLastError());
# endif
}

//-------------------------------------------------------------------

LagrangeInterpolant5D::LagrangeInterpolant5D()
: LagrangeInterpolant(1,true)
{}

//-------------------------------------------------------------------
#ifdef ENABLE_CUDA
bool
LagrangeInterpolant5D::gpu_malloc()
{
  cudaMalloc((void**) &gpu_x1vals_, sizeof(double) * x1vals_.size());
  cudaMalloc((void**) &gpu_x2vals_, sizeof(double) * x2vals_.size());
  cudaMalloc((void**) &gpu_x3vals_, sizeof(double) * x3vals_.size());
  cudaMalloc((void**) &gpu_x4vals_, sizeof(double) * x4vals_.size());
  cudaMalloc((void**) &gpu_x5vals_, sizeof(double) * x5vals_.size());
  cudaMalloc((void**) &gpu_fvals_,  sizeof(double) * fvals_.size());
  
  cudaMalloc((void**) &gpu_isUniform_, sizeof(double) * 5);
  cudaMalloc((void**) &gpu_xvalsize_, sizeof(size_t) * 5);
  cudaMalloc((void**) &gpu_bounds_, sizeof(double) * 10);
  cudaMalloc((void**) &gpu_allowClipping_, sizeof(bool));
  
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant5D::cpu2gpu_memcpy()
{
  cudaMemcpy(gpu_x1vals_, &x1vals_[0], sizeof(double) * x1vals_.size(), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_x2vals_, &x2vals_[0], sizeof(double) * x2vals_.size(), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_x3vals_, &x3vals_[0], sizeof(double) * x3vals_.size(), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_x4vals_, &x4vals_[0], sizeof(double) * x4vals_.size(), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_x5vals_, &x5vals_[0], sizeof(double) * x5vals_.size(), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_fvals_,  &fvals_[0],  sizeof(double) * fvals_.size(),  cudaMemcpyHostToDevice);
  
  cudaMemcpy(gpu_isUniform_, &isUniform_[0], sizeof(bool) * 5, cudaMemcpyHostToDevice);
  
  size_t xvalsize[5] = {x1vals_.size(), x2vals_.size(), x3vals_.size(), x4vals_.size(), x5vals_.size()};
  cudaMemcpy(gpu_xvalsize_, &xvalsize[0], sizeof(size_t) * 5, cudaMemcpyHostToDevice);
  
  const double bounds[10] = {bounds_[0].first, bounds_[0].second, bounds_[1].first, bounds_[1].second,
    bounds_[2].first, bounds_[2].second, bounds_[3].first, bounds_[3].second,
    bounds_[4].first, bounds_[4].second};
  cudaMemcpy(gpu_bounds_, &bounds[0], sizeof(double) * 10, cudaMemcpyHostToDevice);
  
  cudaMemcpy(gpu_allowClipping_, &allowClipping_, sizeof(bool), cudaMemcpyHostToDevice);
  
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant5D::gpu_value( const std::vector<const double *>& indep, double* values, const size_t indepsize) const
{
  value5D<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(values, indep[0], indep[1], indep[2], indep[3], indep[4],
                                                     gpu_x1vals_, gpu_x2vals_, gpu_x3vals_, gpu_x4vals_, gpu_x5vals_, gpu_fvals_,
                                                     gpu_xvalsize_,
                                                     order_,
                                                     gpu_isUniform_,
                                                     gpu_allowClipping_,
                                                     gpu_bounds_,
                                                     indepsize);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant5D::gpu_derivative( const std::vector<const double*>& indep, const int dim, double* values, const size_t indepsize ) const
{
  derivative5D<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(values, indep[0], indep[1], indep[2], indep[3], indep[4],
                                                          gpu_x1vals_, gpu_x2vals_, gpu_x3vals_, gpu_x4vals_, gpu_x5vals_, gpu_fvals_,
                                                          gpu_xvalsize_,
                                                          order_,
                                                          gpu_isUniform_,
                                                          gpu_allowClipping_,
                                                          gpu_bounds_,
                                                          indepsize,
                                                          dim);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}
//-------------------------------------------------------------------
bool
LagrangeInterpolant5D::gpu_second_derivative( const std::vector<const double*>& indep, const int dim1, const int dim2, double* values, const size_t indepsize ) const
{
  second_derivative5D<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(values, indep[0], indep[1], indep[2], indep[3], indep[4],
                                                                 gpu_x1vals_, gpu_x2vals_, gpu_x3vals_, gpu_x4vals_, gpu_x5vals_, gpu_fvals_,
                                                                 gpu_xvalsize_,
                                                                 order_,
                                                                 gpu_isUniform_,
                                                                 gpu_allowClipping_,
                                                                 gpu_bounds_,
                                                                 indepsize,
                                                                 dim1, dim2);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK(cudaGetLastError());
  return true;
}

#endif
//-------------------------------------------------------------------

double
LagrangeInterpolant5D::second_derivative( const double* const indep, const int dim1, const int dim2 ) const
{
  if( order_ == 1 ) return 0.0;

  assert( dim1 < 5 );
  assert( dim1 < 5 );

  const double x1 = allowClipping_ ? std::min( std::max( indep[0], bounds_[0].first ), bounds_[0].second ) : indep[0];
  const double x2 = allowClipping_ ? std::min( std::max( indep[1], bounds_[1].first ), bounds_[1].second ) : indep[1];
  const double x3 = allowClipping_ ? std::min( std::max( indep[2], bounds_[2].first ), bounds_[2].second ) : indep[2];
  const double x4 = allowClipping_ ? std::min( std::max( indep[3], bounds_[3].first ), bounds_[3].second ) : indep[3];
  const double x5 = allowClipping_ ? std::min( std::max( indep[4], bounds_[4].first ), bounds_[4].second ) : indep[4];

  size_t i1lo, i2lo, i3lo, i4lo, i5lo;

  switch( dim1 ){
    case 0:{
      switch( dim2 ){
        case 0:
          i1lo = second_der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =            basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =            basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =            basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo =            basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 1:
          i1lo = der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo = der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo =     basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 2:
          i1lo = der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo = der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo =     basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 3:
          i1lo = der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo = der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo =     basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 4:
          i1lo = der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo = der_basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
      }
      break;
    } // case 0 on switch(dim1)
    case 1:{
      switch( dim2 ){
        case 0:
          i1lo = der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo = der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo =     basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 1:
          i1lo =            basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo = second_der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =            basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =            basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo =            basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 2:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo = der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo = der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo =     basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 3:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo = der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo = der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo =     basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 4:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo = der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo = der_basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
      }
      break;
    } // case 1 on switch(dim1)
    case 2:{
      switch( dim2 ){
        case 0:
          i1lo = der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo = der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo =     basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 1:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo = der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo = der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo =     basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 2:
          i1lo =            basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =            basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo = second_der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =            basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo =            basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 3:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo = der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo = der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo =     basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 4:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo = der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo = der_basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
      }
      break;
    } // case 2 on switch(dim1)
    case 3:{
      switch( dim2 ){
        case 0:
          i1lo = der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo = der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo =     basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 1:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo = der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo = der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo =     basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 2:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo = der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo = der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo =     basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 3:
          i1lo =            basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =            basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =            basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo = second_der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo =            basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 4:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo = der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo = der_basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
      }
      break;
    } // case 3 on switch(dim1)
    case 4:{
      switch( dim2 ){
        case 0:
          i1lo = der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo = der_basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 1:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo = der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo = der_basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 2:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo = der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo = der_basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 3:
          i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo = der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo = der_basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
        case 4:
          i1lo =            basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
          i2lo =            basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
          i3lo =            basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
          i4lo =            basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
          i5lo = second_der_basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
          break;
      }
      break;
    } // case 4 on switch(dim1)
    default:
      throw std::invalid_argument("invalid dimension specification for second derivative");
  }

  const size_t nx1 = x1vals_.size();
  const size_t nx2 = x2vals_.size();
  const size_t nx3 = x3vals_.size();
  const size_t nx4 = x4vals_.size();

  double val = 0.0;

  for( size_t k5=0; k5<=order_; ++k5 ){
    const size_t i5 = (i5lo+k5)*nx1*nx2*nx3*nx4;
    for( size_t k4=0; k4<=order_; ++k4 ){
      const double tmp = x4coef1d_[k4] * x5coef1d_[k5];
      const size_t i4 = (i4lo+k4)*nx1*nx2*nx3;
      for( size_t k3=0; k3<=order_; ++k3 ){
        const double tmp2 = x3coef1d_[k3] * tmp;
        const size_t i3 = (i3lo+k3)*nx1*nx2;
        for( size_t k2=0; k2<=order_; ++k2 ){
          const double tmp3 = x2coef1d_[k2] * tmp2;
          const size_t i2 = (i2lo+k2)*nx1;
          for( size_t k1=0; k1<=order_; ++k1 ){
            const size_t i1 = i1lo+k1 + i2 + i3 + i4 + i5;
            assert( i1 < fvals_.size() );
            val += fvals_[i1] * x1coef1d_[k1] * tmp3;
          }
        }
      }
    }
  }
  return val;
}

//-------------------------------------------------------------------

LagrangeInterpolant5D::~LagrangeInterpolant5D()
{}

//-------------------------------------------------------------------

vector<pair<double,double> >
LagrangeInterpolant5D::get_bounds() const
{
  return bounds_;
}

//-------------------------------------------------------------------

bool
LagrangeInterpolant5D::operator==( const LagrangeInterpolant& other ) const
{
  const LagrangeInterpolant5D& a = dynamic_cast<const LagrangeInterpolant5D&>(other);
  return order_ == a.get_order()
      && allowClipping_ == a.clipping()
      && vec_compare( x1vals_, a.x1vals_ )
      && vec_compare( x2vals_, a.x2vals_ )
      && vec_compare( x3vals_, a.x3vals_ )
      && vec_compare( x4vals_, a.x4vals_ )
      && vec_compare( x5vals_, a.x5vals_ )
      && vec_compare( fvals_ , a.fvals_  )
      && bounds_ == a.bounds_;
}

//-------------------------------------------------------------------

template<typename Archive> void
LagrangeInterpolant5D::serialize( Archive& ar, const unsigned version )
{
  ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP( LagrangeInterpolant )
     & BOOST_SERIALIZATION_NVP( x1vals_ )
     & BOOST_SERIALIZATION_NVP( x2vals_ )
     & BOOST_SERIALIZATION_NVP( x3vals_ )
     & BOOST_SERIALIZATION_NVP( x4vals_ )
     & BOOST_SERIALIZATION_NVP( x5vals_ )
     & BOOST_SERIALIZATION_NVP( fvals_  )
     & BOOST_SERIALIZATION_NVP( isUniform_ );

  bounds_.clear();
  bounds_.push_back( bounds(x1vals_) );
  bounds_.push_back( bounds(x2vals_) );
  bounds_.push_back( bounds(x3vals_) );
  bounds_.push_back( bounds(x4vals_) );
  bounds_.push_back( bounds(x5vals_) );

  x1coef1d_.resize( order_+1 );
  x2coef1d_.resize( order_+1 );
  x3coef1d_.resize( order_+1 );
  x4coef1d_.resize( order_+1 );
  x5coef1d_.resize( order_+1 );

# ifdef ENABLE_CUDA
  gpu_malloc();
  cpu2gpu_memcpy();
# endif

}

//===================================================================

// explicit template instantiation

template void LagrangeInterpolant  ::serialize<OutputArchive>( OutputArchive&, const unsigned );
template void LagrangeInterpolant  ::serialize<InputArchive >( InputArchive& , const unsigned );

template void LagrangeInterpolant1D::serialize<OutputArchive>( OutputArchive&, const unsigned );
template void LagrangeInterpolant1D::serialize<InputArchive >( InputArchive& , const unsigned );

template void LagrangeInterpolant2D::serialize<OutputArchive>( OutputArchive&, const unsigned );
template void LagrangeInterpolant2D::serialize<InputArchive >( InputArchive& , const unsigned );

template void LagrangeInterpolant3D::serialize<OutputArchive>( OutputArchive&, const unsigned );
template void LagrangeInterpolant3D::serialize<InputArchive >( InputArchive& , const unsigned );

template void LagrangeInterpolant4D::serialize<OutputArchive>( OutputArchive&, const unsigned );
template void LagrangeInterpolant4D::serialize<InputArchive >( InputArchive& , const unsigned );

template void LagrangeInterpolant5D::serialize<OutputArchive>( OutputArchive&, const unsigned );
template void LagrangeInterpolant5D::serialize<InputArchive >( InputArchive& , const unsigned );

BOOST_CLASS_EXPORT_IMPLEMENT( LagrangeInterpolant   )   BOOST_CLASS_VERSION( LagrangeInterpolant  , 1 )
BOOST_CLASS_EXPORT_IMPLEMENT( LagrangeInterpolant1D )   BOOST_CLASS_VERSION( LagrangeInterpolant1D, 1 )
BOOST_CLASS_EXPORT_IMPLEMENT( LagrangeInterpolant2D )   BOOST_CLASS_VERSION( LagrangeInterpolant2D, 1 )
BOOST_CLASS_EXPORT_IMPLEMENT( LagrangeInterpolant3D )   BOOST_CLASS_VERSION( LagrangeInterpolant3D, 1 )
BOOST_CLASS_EXPORT_IMPLEMENT( LagrangeInterpolant4D )   BOOST_CLASS_VERSION( LagrangeInterpolant4D, 1 )
BOOST_CLASS_EXPORT_IMPLEMENT( LagrangeInterpolant5D )   BOOST_CLASS_VERSION( LagrangeInterpolant5D, 1 )
