include( CMakePackageConfigHelpers )
include( TPBuildTools.cmake )

set( tabprops_SRC
    PropertyStash.cpp
    StateTable.cpp
  )

set( tabprops_H
    Archive.h
    LagrangeInterpolant.h
    PropertyStash.h
    StateTable.h
    TabProps.h
  )


include(GenerateExportHeader)
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
        "${CMAKE_CURRENT_BINARY_DIR}/TabPropsConfigVersion.cmake"
        VERSION "${MAJOR_VERSION}.${MINOR_VERSION}.${PATCH_VERSION}"
        COMPATIBILITY AnyNewerVersion
)

#---------------- Build and install the interp library --------------
tp_add_library( interp LagrangeInterpolant.cpp )
target_link_libraries( interp
      PUBLIC
        Boost::serialization
        ${CUDA_LIBRARIES}
        ${CUDA_CUBLAS_LIBRARIES}
      )
target_include_directories( interp
    PRIVATE
        ${CMAKE_BINARY_DIR}
        ${CMAKE_SOURCE_DIR}
    PUBLIC
        ${Boost_INCLUDE_DIR}
        ${CUDA_INCLUDE_DIRS}
    INTERFACE
        $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}>
        $<INSTALL_INTERFACE:include>
    )
#--------------------------------------------------------------------

#---------------- Build and install the tabprops library --------------
tp_add_library( tabprops  ${tabprops_SRC} )
target_link_libraries( tabprops
      PUBLIC
        interp
        Boost::serialization
      )
target_include_directories( tabprops
      PUBLIC
        ${Boost_INCLUDE_DIR}
      PRIVATE
        ${CMAKE_SOURCE_DIR}
      INTERFACE
        $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}>
        $<INSTALL_INTERFACE:include>
      )

if( TabProps_PREPROCESSOR OR ENABLE_MIXMDL )
  if( TabProps_PREPROCESSOR )
    message( STATUS "Building TabProps preprocessing tools" )
  else()
    message( STATUS "Building TabProps mixing model library" )
  endif( TabProps_PREPROCESSOR )
  add_subdirectory( prepro )
endif( TabProps_PREPROCESSOR OR ENABLE_MIXMDL )

if( TabProps_ENABLE_TESTING )
  add_subdirectory( test )
endif()
add_subdirectory( util )

# Installation
set( INCLUDE_INSTALL_DIR ${CMAKE_INSTALL_PREFIX}/include )
set( LIB_INSTALL_DIR ${CMAKE_INSTALL_PREFIX}/lib/tabprops )
install( TARGETS tabprops interp
        EXPORT TabPropsTargets  # dumps these targets into "TabPropsTargets.cmake"
        LIBRARY DESTINATION ${LIB_INSTALL_DIR}
        ARCHIVE DESTINATION ${LIB_INSTALL_DIR}
        INCLUDES DESTINATION ${INCLUDE_INSTALL_DIR}
      )
configure_file(
        ${CMAKE_SOURCE_DIR}/TabPropsConfig.h.in
        ${CMAKE_BINARY_DIR}/tabprops/TabPropsConfig.h
)
install( FILES
        ${tabprops_H}
        ${CMAKE_BINARY_DIR}/tabprops/TabPropsConfig.h
        DESTINATION include/tabprops
        PERMISSIONS OWNER_READ GROUP_READ WORLD_READ
        )
# downstream CMake usage:
export( EXPORT TabPropsTargets
        FILE
          ${CMAKE_CURRENT_BINARY_DIR}/TabPropsTargets.cmake
        NAMESPACE
          TabProps::
      )
install( EXPORT TabPropsTargets
        FILE
          TabPropsTargets.cmake
        NAMESPACE TabProps::
        DESTINATION
          ${CMAKE_INSTALL_PREFIX}/lib/tabprops
        )
configure_package_config_file( ${PROJECT_SOURCE_DIR}/TabPropsConfig.cmake.in
        ${PROJECT_BINARY_DIR}/config/TabPropsConfig.cmake
        INSTALL_DESTINATION ${TabProps_CONFIG_INSTALL}
        PATH_VARS INCLUDE_INSTALL_DIR LIB_INSTALL_DIR TabProps_CONFIG_INSTALL
      )
install( FILES
          ${PROJECT_BINARY_DIR}/config/TabPropsConfig.cmake
          ${CMAKE_CURRENT_BINARY_DIR}/TabPropsConfigVersion.cmake
        DESTINATION
          ${TabProps_CONFIG_INSTALL}
      )
#--------------------------------------------------------------------


if( ENABLE_PYTHON )
  pybind11_add_module(pytabprops ${CMAKE_SOURCE_DIR}/tabprops/PybindDefinitions.cpp)
  target_link_libraries(pytabprops PRIVATE tabprops)
  if( ENABLE_MIXMDL )
    target_link_libraries(pytabprops PRIVATE tbmixmdl)
  endif( ENABLE_MIXMDL )
  set(SETUP_PY "${CMAKE_SOURCE_DIR}/tabprops/setup.py")
  find_package(PythonInterp)
  if(PYTHONINTERP_FOUND)
    message(STATUS "Will use ${PYTHON_EXECUTABLE} for python installation.")
    install(CODE "execute_process(COMMAND ${PYTHON_EXECUTABLE} ${SETUP_PY} install WORKING_DIRECTORY ${PROJECT_BINARY_DIR}/tabprops)")
  else()
    message( STATUS "Did not find a python executable" )
  endif(PYTHONINTERP_FOUND)
endif( ENABLE_PYTHON )
