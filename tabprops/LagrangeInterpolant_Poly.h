/**
 *  \file   LagrangeInterpolant_Poly.h
 *  \date   May 24, 2016
 *  \author "Babak Goshayeshi"
 *
 *
 * The MIT License
 *
 * Copyright (c) 2013-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

#ifndef LAGRANGEINTERPOLANT_POLY_H_LOCK

#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
# define LAGRANGEINTERPOLANT_POLY_CUDA_H
#else 
# define LAGRANGEINTERPOLANT_POLY_H
#endif

#ifdef LAGRANGEINTERPOLANT_POLY_H
# ifdef ENABLE_CUDA
#   ifdef LAGRANGEINTERPOLANT_POLY_CUDA_H
#     define LAGRANGEINTERPOLANT_POLY_H_LOCK
#   endif
# else
#   define LAGRANGEINTERPOLANT_POLY_H_LOCK
# endif
#endif

#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
#include <cuda_runtime.h>
#include <cuda.h>
#include <assert.h>
#include <cuda_runtime_api.h>
#endif

#include <vector>
#include <stdexcept>
#include <iostream>
#include <sstream>
#include <cmath> 

#include "LagrangeInterpolant.h"

// macro to expand a calculation of a single fraction term in the Lagrange polynomial expansions
#define BASIS_FUN( k, i, xvals, x ) (x-xvals[i])/(xvals[k]-xvals[i])
#define TMPVAL( i,j,xvals )( (xvals[i]-xvals[j]) )
//===================================================================

#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
inline __device__ size_t gpu_index_finder(const double& x,
                                          const double* xgrid,
                                          const size_t& nx,
                                          const bool allowClipping=false)
{
#else
template< typename IndexT, typename ValT >
IndexT
inline index_finder( const ValT& x,
                     const std::vector<ValT>& xgrid,
                     const bool allowClipping=false )
{
  
  assert( !std::isnan(x) );
  const size_t nx = xgrid.size();
# endif
  size_t ilo = 0;
  size_t ihi = nx-1;

  if( allowClipping && x<xgrid[0] ) return ilo;
  if( allowClipping && x>xgrid[ihi]  ) return ihi;

#ifndef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
  // sanity check
  if( x<xgrid.front() || x>xgrid.back() ){
    std::ostringstream msg;
    msg << __FILE__ << " : " << __LINE__ << std::endl
    << "root is not bracketed!" << std::endl;
    throw std::runtime_error(msg.str());
  }
#endif

#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
  while( ihi-ilo > 1 ){
    const double m = ( xgrid[ihi]-xgrid[ilo] ) / ( ihi-ilo );
    const size_t c = max( ilo+1,  size_t(ihi - (xgrid[ihi]-x)/m)  );
    if( x >= xgrid[c] )
      ilo = min(ihi-1,c);
    else
      ihi = max(ilo+1,c);
  }
#else
  ilo = (std::lower_bound(xgrid.begin(), xgrid.end(), x) - xgrid.begin()) - 1;
#endif
  return ilo;
}


//===================================================================
// for uniform mesh
inline size_t
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA 
__device__ gpu_low_index(
#else
low_index(
#endif
              const double& xlo,
              const double& xhi,
              const double& dx,
              const double& x,
              const unsigned& order)
{
  int i = (x-xlo) / dx;
  // adjust for order
  const int half = int(order)/2;
  const int imax = (xhi-xlo)/dx;
  
  // adjust at boundaries
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
  i = max( 0, i-half );
  i = min( imax-order, max( 0, i ) );
#else
  i = std::max( 0, i-half );
  i = std::min( imax-int(order), std::max( 0, i ) );
#endif
  return i;
}

//===================================================================
// for nonuniform mesh
inline size_t
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
 __device__
gpu_low_index(const double* xvals,
              const double& x,
              const unsigned& order,
              const size_t& nx)
{
  int i = gpu_index_finder(x, xvals, nx, true);
  // adjust for order
  const int half = int(order)/2;
  i = max( 0, i-half );
  // adjust at boundaries
  i = min( int(nx - 1 - order), max( 0, i));
  return i;
}
#else 
low_index(const std::vector<double>& xvals,
          const double x,
          const unsigned order )
{
  int i = index_finder<size_t,double>( x, xvals, true );
  const size_t nx = xvals.size();
  // adjust for order
  const int half = int(order)/2;
  i = std::max( 0, i-half );
  // adjust at boundaries
  i = std::min( int(nx - 1 - order), std::max( 0, i));
  return i;
}
#endif


//===================================================================
/**
 * @brief compute the 1D basis functions for Lagrange polynomial interpolants
 *
 * @param xvals the independent variable function values
 * @param x the independent variable value where we want to interpolate
 * @param order the order of interpolant
 * @param coefs the coefficients that we will set here
 * @param isUniform if true then the search for the bracketing interval will use
 * a faster algorithm.  This should only be used for uniform spacing of the
 * independent variable.
 *
 * @return the lo index for accessing the relevant data (can prevent further searches later)
*/
                         
inline  size_t
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__device__
gpu_basis_funs_1d(const double* const xvals,
                  const double& x,
                  const unsigned& order,
                  double*  coefs,
                  const size_t& nx,
                  const bool isUniform = false )
{
  assert( nx >= order );
  size_t ilo = 0;
  if( isUniform ) ilo = gpu_low_index( xvals[0], xvals[nx-1], xvals[1]-xvals[0], x, order);
  else            ilo = gpu_low_index( xvals, x, order, nx);

#else
basis_funs_1d(const std::vector<double>& xvals,
              const double& x,
              const unsigned& order,
              double*  coefs,
              const bool isUniform = false )
{
  assert( xvals.size() >= order );
  size_t ilo = 0;
  if( isUniform ) ilo = low_index( xvals[0], xvals[xvals.size()-1], xvals[1]-xvals[0], x, order);
  else            ilo = low_index( xvals, x, order );
#endif
  switch (order) {
    case 1:
      coefs[0] = BASIS_FUN(ilo+0,ilo+1,xvals,x);
      coefs[1] = BASIS_FUN(ilo+1,ilo  ,xvals,x);
      break;
    case 2:
      coefs[0] = BASIS_FUN(ilo+0,ilo+1,xvals,x) * BASIS_FUN(ilo+0,ilo+2,xvals,x);
      coefs[1] = BASIS_FUN(ilo+1,ilo  ,xvals,x) * BASIS_FUN(ilo+1,ilo+2,xvals,x);
      coefs[2] = BASIS_FUN(ilo+2,ilo  ,xvals,x) * BASIS_FUN(ilo+2,ilo+1,xvals,x);
      break;
    case 3:
      coefs[0] = BASIS_FUN(ilo+0,ilo+1,xvals,x) * BASIS_FUN(ilo+0,ilo+2,xvals,x) * BASIS_FUN(ilo+0,ilo+3,xvals,x);
      coefs[1] = BASIS_FUN(ilo+1,ilo  ,xvals,x) * BASIS_FUN(ilo+1,ilo+2,xvals,x) * BASIS_FUN(ilo+1,ilo+3,xvals,x);
      coefs[2] = BASIS_FUN(ilo+2,ilo  ,xvals,x) * BASIS_FUN(ilo+2,ilo+1,xvals,x) * BASIS_FUN(ilo+2,ilo+3,xvals,x);
      coefs[3] = BASIS_FUN(ilo+3,ilo  ,xvals,x) * BASIS_FUN(ilo+3,ilo+1,xvals,x) * BASIS_FUN(ilo+3,ilo+2,xvals,x);
      break;
    default:
      for( size_t k=0; k<=order; ++k ){
        coefs[k] = 1;
        for( size_t i=0; i<=order; ++i ){
          if( i!=k ) coefs[k] *= BASIS_FUN( ilo+k, ilo+i, xvals, x );
        }
      }
      break;
  }
  return ilo;
}
//===================================================================
inline  size_t
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__device__
gpu_der_basis_funs_1d(const double* const xvals,
                      const double& x,
                      const unsigned& order,
                      double*  coefs,
                      const size_t& nx,
                      const bool isUniform = false )
{
  assert( nx > order);
  size_t ilo = 0;
  if( isUniform ) ilo = gpu_low_index( xvals[0], xvals[nx-1], xvals[1]-xvals[0], x, order);
  else            ilo = gpu_low_index( xvals, x, order, nx);
# else
der_basis_funs_1d(const std::vector<double>& xvals,
                  const double x,
                  const unsigned order,
                  double* const coefs,
                  const bool isUniform = false )
{
# ifndef NDEBUG
    assert( xvals.size() > order);
    assert( order>=1 );
# endif
  size_t ilo = 0;
  if( isUniform ) ilo = low_index( xvals[0], xvals[xvals.size()-1], xvals[1]-xvals[0], x, order );
  else            ilo = low_index( xvals, x, order );
  switch (order) { // unroll the loops for first and second order
    case 1:
      coefs[0] = 1.0 / (xvals[ilo  ]-xvals[ilo+1]);
      coefs[1] = -coefs[0];
      break;
    case 2:{
      const double dtmp01 = TMPVAL( ilo+0, ilo+1, xvals );
      const double dtmp02 = TMPVAL( ilo+0, ilo+2, xvals );
      const double dtmp12 = TMPVAL( ilo+1, ilo+2, xvals );
      const double dtmp10 = -dtmp01;
      const double dtmp20 = -dtmp02;
      const double dtmp21 = -dtmp12;
      
      coefs[0] = BASIS_FUN(ilo+0,ilo+1,xvals,x) / dtmp02 + BASIS_FUN(ilo+0,ilo+2,xvals,x) / dtmp01;
      coefs[1] = BASIS_FUN(ilo+1,ilo+0,xvals,x) / dtmp12 + BASIS_FUN(ilo+1,ilo+2,xvals,x) / dtmp10;
      coefs[2] = BASIS_FUN(ilo+2,ilo+0,xvals,x) / dtmp21 + BASIS_FUN(ilo+2,ilo+1,xvals,x) / dtmp20;
      break;
    }
    case 3:{
      const double bf01 = BASIS_FUN( ilo+0, ilo+1, xvals, x );
      const double bf02 = BASIS_FUN( ilo+0, ilo+2, xvals, x );
      const double bf03 = BASIS_FUN( ilo+0, ilo+3, xvals, x );
      const double bf10 = BASIS_FUN( ilo+1, ilo+0, xvals, x );
      const double bf12 = BASIS_FUN( ilo+1, ilo+2, xvals, x );
      const double bf13 = BASIS_FUN( ilo+1, ilo+3, xvals, x );
      const double bf20 = BASIS_FUN( ilo+2, ilo+0, xvals, x );
      const double bf21 = BASIS_FUN( ilo+2, ilo+1, xvals, x );
      const double bf23 = BASIS_FUN( ilo+2, ilo+3, xvals, x );
      const double bf30 = BASIS_FUN( ilo+3, ilo+0, xvals, x );
      const double bf31 = BASIS_FUN( ilo+3, ilo+1, xvals, x );
      const double bf32 = BASIS_FUN( ilo+3, ilo+2, xvals, x );
      
      const double dtmp01 = TMPVAL( ilo+0, ilo+1, xvals );
      const double dtmp02 = TMPVAL( ilo+0, ilo+2, xvals );
      const double dtmp03 = TMPVAL( ilo+0, ilo+3, xvals );
      const double dtmp12 = TMPVAL( ilo+1, ilo+2, xvals );
      const double dtmp13 = TMPVAL( ilo+1, ilo+3, xvals );
      const double dtmp23 = TMPVAL( ilo+2, ilo+3, xvals );
      
      coefs[0] = bf02 * bf03 /  dtmp01
      + bf01 * bf03 /  dtmp02
      + bf01 * bf02 /  dtmp03;
      coefs[1] = bf12 * bf13 / -dtmp01
      + bf10 * bf13 /  dtmp12
      + bf10 * bf12 /  dtmp13;
      coefs[2] = bf21 * bf23 / -dtmp02
      + bf20 * bf23 / -dtmp12
      + bf20 * bf21 /  dtmp23;
      coefs[3] = bf31 * bf32 / -dtmp03
      + bf30 * bf32 / -dtmp13
      + bf30 * bf31 / -dtmp23;
      break;
    }
    default:
# endif
      // this is fairly slow because of the duplicate calls to BASIS_FUN
      for( size_t k=0; k<=order; ++k ){
        coefs[k] = 0.0;
        for( size_t i=0; i<=order; ++i ){
          if( i==k ) continue;
          double tmpprod = 1.0;
          for( size_t j=0; j<=order; ++j ){
            if( j==i || j==k ) continue;
            tmpprod *= BASIS_FUN(ilo+k,ilo+j,xvals,x);
          }
          coefs[k] += tmpprod / TMPVAL( ilo+k, ilo+i, xvals );
        }
      }
# ifndef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
      break;
  }
# endif
  return ilo;
}
//===================================================================
#ifndef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
inline size_t basis_funs_1d(const std::vector<double>& xvals,
                            const double x,
                            const unsigned order,
                            std::vector<double>& coefs,
                            const bool isUniform = false )
{
# ifndef NDEBUG
  assert( coefs.size() == order+1 );
  assert( order>=1 );
# endif
  return basis_funs_1d( xvals, x, order, &coefs[0], isUniform );
}
//===================================================================
inline size_t der_basis_funs_1d(const std::vector<double>& xvals,
                                const double x,
                                const unsigned order,
                                std::vector<double>& coefs,
                                const bool isUniform = false )
{
# ifndef NDEBUG
  assert( coefs.size() == order+1 );
  assert( order>=1 );
# endif
  return der_basis_funs_1d( xvals, x, order, &coefs[0], isUniform );
}
#endif
  

//===================================================================

inline size_t
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__device__
gpu_second_der_basis_funs_1d(const double* xvals,
                             const double x,
                             const unsigned order,
                             double* coefs,
                             const size_t& nx,
                             const bool isUniform = false )
{
  assert( nx > order+1 );
  size_t ilo = 0;
  if( isUniform ) ilo = gpu_low_index( xvals[0], xvals[nx-1], xvals[1]-xvals[0], x, order);
  else            ilo = gpu_low_index( xvals, x, order, nx);

  
#else
second_der_basis_funs_1d(const std::vector<double>& xvals,
                         const double x,
                         const unsigned order,
                         double* coefs,
                         const bool isUniform = false )
{
  
# ifndef NDEBUG
  assert( xvals.size() > order+1);
  assert( order>=1 );
# endif
  size_t ilo = 0;
  if( isUniform ) ilo = low_index( xvals[0], xvals[xvals.size()-1], xvals[1]-xvals[0], x, order );
  else            ilo = low_index( xvals, x, order );

#endif
  if( order == 1 ){
    coefs[0] = 0; coefs[1] = 0;
    return 0;  // index doesn't matter in this case.
  }
  switch (order) { // unroll the loops for first and second order
    case 2:{
      const double tmp01 = 1.0/TMPVAL( ilo  , ilo+1, xvals );
      const double tmp02 = 1.0/TMPVAL( ilo  , ilo+2, xvals );
      const double tmp10 = 1.0/TMPVAL( ilo+1, ilo  , xvals );
      const double tmp12 = 1.0/TMPVAL( ilo+1, ilo+2, xvals );
      const double tmp20 = -tmp02;
      const double tmp21 = -tmp12;
      coefs[0] = 2 * tmp01 * tmp02;
      coefs[1] = 2 * tmp10 * tmp12;
      coefs[2] = 2 * tmp20 * tmp21;
      break;
    }
    case 3:{
      const double bf01 = BASIS_FUN( ilo  , ilo+1, xvals, x );
      const double bf02 = BASIS_FUN( ilo  , ilo+2, xvals, x );
      const double bf03 = BASIS_FUN( ilo  , ilo+3, xvals, x );
      const double bf10 = BASIS_FUN( ilo+1, ilo+0, xvals, x );
      const double bf12 = BASIS_FUN( ilo+1, ilo+2, xvals, x );
      const double bf13 = BASIS_FUN( ilo+1, ilo+3, xvals, x );
      const double bf20 = BASIS_FUN( ilo+2, ilo+0, xvals, x );
      const double bf21 = BASIS_FUN( ilo+2, ilo+1, xvals, x );
      const double bf23 = BASIS_FUN( ilo+2, ilo+3, xvals, x );
      const double bf30 = BASIS_FUN( ilo+3, ilo+0, xvals, x );
      const double bf31 = BASIS_FUN( ilo+3, ilo+1, xvals, x );
      const double bf32 = BASIS_FUN( ilo+3, ilo+2, xvals, x );
      
      const double tmp01 = 1.0/TMPVAL( ilo  , ilo+1, xvals );
      const double tmp02 = 1.0/TMPVAL( ilo  , ilo+2, xvals );
      const double tmp03 = 1.0/TMPVAL( ilo  , ilo+3, xvals );
      const double tmp10 = 1.0/TMPVAL( ilo+1, ilo  , xvals );
      const double tmp12 = 1.0/TMPVAL( ilo+1, ilo+2, xvals );
      const double tmp13 = 1.0/TMPVAL( ilo+1, ilo+3, xvals );
      const double tmp20 = -tmp02;
      const double tmp21 = -tmp12;
      const double tmp23 = 1.0/TMPVAL( ilo+2, ilo+3, xvals );
      const double tmp30 = -tmp03;
      const double tmp31 = -tmp13;
      const double tmp32 = -tmp23;
      
      coefs[0] = tmp01*( tmp02*bf03 + tmp03*bf02 )
      + tmp02*( tmp01*bf03 + tmp03*bf01 )
      + tmp03*( tmp01*bf02 + tmp02*bf01 );
      coefs[1] = tmp10*( tmp12*bf13 + tmp13*bf12 )
      + tmp12*( tmp10*bf13 + tmp13*bf10 )
      + tmp13*( tmp10*bf12 + tmp12*bf10 );
      coefs[2] = tmp20*( tmp21*bf23 + tmp23*bf21 )
      + tmp21*( tmp20*bf23 + tmp23*bf20 )
      + tmp23*( tmp20*bf21 + tmp21*bf20 );
      coefs[3] = tmp30*( tmp31*bf32 + tmp32*bf31 )
      + tmp31*( tmp30*bf32 + tmp32*bf30 )
      + tmp32*( tmp30*bf31 + tmp31*bf30 );
      break;
    }
    default:{ // not unrolled. Slow.
      for( size_t k=0; k<=order; ++k ){
        coefs[k] = 0.0;
        for( size_t i=0; i<=order; ++i ){
          if( i == k ) continue;
          double tmp = 0.0;
          for( size_t j=0; j<=order; ++j ){
            if( j == k || j == i ) continue;
            double bf = 1.0;
            for( size_t m=0; m<=order; ++m ){
              if( m == k || m == i || m == j ) continue;
              bf *= BASIS_FUN( ilo+k, ilo+m, xvals, x );
            }
            tmp += bf / TMPVAL( ilo+k, ilo+j, xvals );
          }
          coefs[k] +=  tmp / TMPVAL( ilo+k, ilo+i, xvals );
        }
      }
      break;
    } // default
  } // switch( order )
  
  return ilo;
}
//===================================================================
#ifndef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
size_t second_der_basis_funs_1d(const std::vector<double>& xvals,
                                const double x,
                                const unsigned order,
                                std::vector<double>& coefs,
                                const bool isUniform = false )
{
# ifndef NDEBUG
    assert( coefs.size() == order+1 );
    assert( order>=1 );
# endif
    return second_der_basis_funs_1d(xvals,x,order,&coefs[0],isUniform);
}
#endif
//===================================================================
//===================================================================
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__global__ void value1D(double* gpu_val,
                               const double* const x1,
                               const double* const xvals_,
                               const double* const fvals_,
                               const size_t* xvalsize,
                               const unsigned int order_,
                               const bool* isUniform,
                               const bool allowClipping,
                               const double* const bounds,
                               const size_t indepsize)
{
  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) {
    return;
  }
  const double x = allowClipping ? max( min( x1[idx], bounds[1] ), bounds[0] ) : x1[idx];

  size_t ilo = 0;
  if( isUniform[0] )ilo = gpu_low_index( xvals_[0], xvals_[xvalsize[0]-1], xvals_[1]-xvals_[0], x, order_);
  else              ilo = gpu_low_index( xvals_, x, order_, xvalsize[0]);
  
#else
double
LagrangeInterpolant1D::value( const double* const indep ) const

{
  assert( xvals_.size() > order_ );
  size_t ilo = 0;
  const double x = allowClipping_ ? std::max( std::min( *indep, bounds_.second ), bounds_.first ) : *indep;
  if( isUniform_ ) ilo = low_index( xvals_[0], xvals_[xvals_.size()-1], xvals_[1]-xvals_[0], x, order_ );
  else             ilo = low_index( xvals_, x, order_ );
  
#endif
  double val=0;
  
  switch (order_){ // for up to third order, unroll the loops:
    case 1:
      val = fvals_[ilo  ] * BASIS_FUN( ilo+0, ilo+1, xvals_, x )
      + fvals_[ilo+1] * BASIS_FUN( ilo+1, ilo  , xvals_, x );
      break;
    case 2:
      val = fvals_[ilo  ] * BASIS_FUN( ilo+0, ilo+1, xvals_, x ) * BASIS_FUN( ilo+0, ilo+2, xvals_, x )
      + fvals_[ilo+1] * BASIS_FUN( ilo+1, ilo  , xvals_, x ) * BASIS_FUN( ilo+1, ilo+2, xvals_, x )
      + fvals_[ilo+2] * BASIS_FUN( ilo+2, ilo  , xvals_, x ) * BASIS_FUN( ilo+2, ilo+1, xvals_, x );
      break;
    case 3:
      val = fvals_[ilo  ] * BASIS_FUN( ilo+0, ilo+1, xvals_, x ) * BASIS_FUN( ilo+0, ilo+2, xvals_, x ) * BASIS_FUN( ilo+0, ilo+3, xvals_, x )
      + fvals_[ilo+1] * BASIS_FUN( ilo+1, ilo  , xvals_, x ) * BASIS_FUN( ilo+1, ilo+2, xvals_, x ) * BASIS_FUN( ilo+1, ilo+3, xvals_, x )
      + fvals_[ilo+2] * BASIS_FUN( ilo+2, ilo  , xvals_, x ) * BASIS_FUN( ilo+2, ilo+1, xvals_, x ) * BASIS_FUN( ilo+2, ilo+3, xvals_, x )
      + fvals_[ilo+3] * BASIS_FUN( ilo+3, ilo  , xvals_, x ) * BASIS_FUN( ilo+3, ilo+1, xvals_, x ) * BASIS_FUN( ilo+3, ilo+2, xvals_, x );
      break;
    default: // slower - use loops
      for( size_t k=0; k<=order_; ++k ){
        double tmp=1;
        for( size_t i=0; i<=order_; ++i ){
          if( i==k ) continue;
          tmp *= BASIS_FUN( ilo+k, ilo+i, xvals_, x );
        }
        val += fvals_[k+ilo] * tmp;
      }
      break;
  }
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
  gpu_val[idx] = val;
#else 
  return val;
#endif
}
  
//-------------------------------------------------------------------
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__global__ void derivative1D(double* gpu_val,
                             const double* const x1,
                             const double* const xvals_,
                             const double* const fvals_,
                             const size_t* xvalsize,
                             const unsigned int order_,
                             const bool* isUniform,
                             const bool allowClipping,
                             const double* const bounds,
                             const size_t indepsize,
                             const int dim)
{
  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) return;
  
  const double x = allowClipping ? max( min( x1[idx], bounds[1] ), bounds[0] ) : x1[idx];
  
  size_t ilo = 0;
  if( isUniform[0] )ilo = gpu_low_index( xvals_[0], xvals_[xvalsize[0]-1], xvals_[1]-xvals_[0], x, order_);
  else              ilo = gpu_low_index( xvals_, x, order_, xvalsize[0]);
  
#else
double
LagrangeInterpolant1D::derivative( const double* const indep, const int dim ) const
{
  assert( xvals_.size() > order_ );
  const double x = allowClipping_ ? std::max( std::min( *indep, bounds_.second ), bounds_.first ) : *indep;
  
  size_t ilo = 0;
  if( isUniform_ ) ilo = low_index( xvals_[0], xvals_[xvals_.size()-1], xvals_[1]-xvals_[0], x, order_ );
  else             ilo = low_index( xvals_, x, order_ );
#endif
  double val = 0.0;
  
  switch (order_) { // unroll the loops for first and second order
    case 1:
      val = fvals_[ilo  ] / (xvals_[ilo  ]-xvals_[ilo+1])
      + fvals_[ilo+1] / (xvals_[ilo+1]-xvals_[ilo  ]);
      break;
    case 2:
      val = fvals_[ilo  ] * ( BASIS_FUN( ilo+0, ilo+1, xvals_, x ) / ( xvals_[ilo+0] - xvals_[ilo+2] )
                             + BASIS_FUN( ilo+0, ilo+2, xvals_, x ) / ( xvals_[ilo+0] - xvals_[ilo+1] ) )
      + fvals_[ilo+1] * ( BASIS_FUN( ilo+1, ilo+0, xvals_, x ) / ( xvals_[ilo+1] - xvals_[ilo+2] )
                         + BASIS_FUN( ilo+1, ilo+2, xvals_, x ) / ( xvals_[ilo+1] - xvals_[ilo+0] ) )
      + fvals_[ilo+2] * ( BASIS_FUN( ilo+2, ilo+0, xvals_, x ) / ( xvals_[ilo+2] - xvals_[ilo+1] )
                         + BASIS_FUN( ilo+2, ilo+1, xvals_, x ) / ( xvals_[ilo+2] - xvals_[ilo+0] ) );
      break;
#ifndef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
    case 3:{
      // store a few off to avoid repeated calculation since there are duplicate usage
      const double tmp01 = BASIS_FUN( ilo+0, ilo+1, xvals_, x );
      const double tmp02 = BASIS_FUN( ilo+0, ilo+2, xvals_, x );
      const double tmp03 = BASIS_FUN( ilo+0, ilo+3, xvals_, x );
      const double tmp10 = BASIS_FUN( ilo+1, ilo+0, xvals_, x );
      const double tmp12 = BASIS_FUN( ilo+1, ilo+2, xvals_, x );
      const double tmp13 = BASIS_FUN( ilo+1, ilo+3, xvals_, x );
      const double tmp20 = BASIS_FUN( ilo+2, ilo+0, xvals_, x );
      const double tmp21 = BASIS_FUN( ilo+2, ilo+1, xvals_, x );
      const double tmp23 = BASIS_FUN( ilo+2, ilo+3, xvals_, x );
      const double tmp30 = BASIS_FUN( ilo+3, ilo+0, xvals_, x );
      const double tmp31 = BASIS_FUN( ilo+3, ilo+1, xvals_, x );
      const double tmp32 = BASIS_FUN( ilo+3, ilo+2, xvals_, x );
      val = fvals_[ilo  ] * ( tmp02*tmp03/(xvals_[ilo+0]-xvals_[ilo+1])
                             + tmp01*tmp03/(xvals_[ilo+0]-xvals_[ilo+2])
                             + tmp01*tmp02/(xvals_[ilo+0]-xvals_[ilo+3]) )
      + fvals_[ilo+1] * ( tmp12*tmp13/(xvals_[ilo+1]-xvals_[ilo+0])
                         + tmp10*tmp13/(xvals_[ilo+1]-xvals_[ilo+2])
                         + tmp10*tmp12/(xvals_[ilo+1]-xvals_[ilo+3]) )
      + fvals_[ilo+2] * ( tmp21*tmp23/(xvals_[ilo+2]-xvals_[ilo+0])
                         + tmp20*tmp23/(xvals_[ilo+2]-xvals_[ilo+1])
                         + tmp20*tmp21/(xvals_[ilo+2]-xvals_[ilo+3]) )
      + fvals_[ilo+3] * ( tmp31*tmp32/(xvals_[ilo+3]-xvals_[ilo+0])
                         + tmp30*tmp32/(xvals_[ilo+3]-xvals_[ilo+1])
                         + tmp30*tmp31/(xvals_[ilo+3]-xvals_[ilo+2]) );
      break;
    }
#endif
    default: // not unrolled.  This will be significantly slower due to repeated calculation of BASIS_FUN
      for( size_t k=0; k<=order_; ++k ){
        double tmp = 0.0;
        for( size_t i=0; i<=order_; ++i ){
          if( i==k ) continue;
          double tmpprod = 1.0;
          for( size_t j=0; j<=order_; ++j ){
            if( j==i || j==k ) continue;
            tmpprod *= BASIS_FUN( ilo+k, ilo+j, xvals_, x );
          }
          tmp += tmpprod / ( xvals_[ilo+k] - xvals_[ilo+i] );
        }
        val += fvals_[k+ilo] * tmp;
      }
      break;
  }
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
  gpu_val[idx] = val;
#else
  return val;
#endif
}
  
//-------------------------------------------------------------------
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__global__ void second_derivative1D(double* gpu_val,
                                    const double* const x1,
                                    const double* const xvals_,
                                    const double* const fvals_,
                                    const size_t* xvalsize,
                                    const unsigned int order_,
                                    const bool* isUniform,
                                    const bool allowClipping,
                                    const double* const bounds,
                                    const size_t indepsize,
                                    const int dim1,
                                    const int dim2)
{
  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) return;
  gpu_val[idx] = 0.0;
  if( order_ == 1 ) return;
  const double x = allowClipping ? max( min( x1[idx], bounds[1] ), bounds[0] ) : x1[idx];
  double coefs[5];
  const size_t ilo = gpu_second_der_basis_funs_1d( xvals_, x, order_, coefs, xvalsize[0], isUniform[0] );
  for( short k=0; k<=order_; ++k ) gpu_val[idx] += fvals_[ilo+k]*coefs[k];
#else
double
LagrangeInterpolant1D::second_derivative( const double* const indep, const int dim1, const int dim2 ) const
{
  assert( xvals_.size() > order_+1 );
  const double x = allowClipping_ ? std::max( std::min( *indep, bounds_.second ), bounds_.first ) : *indep;
  if( order_ == 1 ) return 0;  // For first order, the second derivative is zero.
  
  double coefs[order_+1];
  const size_t ilo = second_der_basis_funs_1d( xvals_, x, order_, coefs, isUniform_ );
  double val = 0.0;
  for( short k=0; k<=order_; ++k ) val += fvals_[ilo+k]*coefs[k];
  return val;
#endif
}
  
  
//===================================================================

#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__global__ void value2D(double* gpu_val,
                        const double* const x1,
                        const double* const x2,
                        const double* const xvals_,
                        const double* const yvals_,
                        const double* const fvals_,
                        const size_t* xvalsize,
                        const unsigned int order_,
                        const bool* isUniform,
                        const bool allowClipping,
                        const double* const bounds,
                        const size_t indepsize)
{

  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) return;
  const double x = allowClipping ? min( max( x1[idx], bounds[0] ), bounds[1] ) : x1[idx];
  const double y = allowClipping ? min( max( x2[idx], bounds[2] ), bounds[3] ) : x2[idx];
  
  double xcoef1d_[5], ycoef1d_[5]; // use order + 1 instead of 5
  const size_t ilo = gpu_basis_funs_1d( xvals_, x, order_, &xcoef1d_[0], xvalsize[0], isUniform[0] );
  const size_t jlo = gpu_basis_funs_1d( yvals_, y, order_, &ycoef1d_[0], xvalsize[1], isUniform[1] );
  
  const size_t nx = xvalsize[0];
#else
double
  LagrangeInterpolant2D::value( const double* const indep ) const
{
  const size_t nx = xvals_.size();
  const double x = allowClipping_ ? std::min( std::max( indep[0], xbounds_.first ), xbounds_.second ) : indep[0];
  const double y = allowClipping_ ? std::min( std::max( indep[1], ybounds_.first ), ybounds_.second ) : indep[1];
  
  const size_t ilo = basis_funs_1d( xvals_, x, order_, xcoef1d_, isUniformX_ );
  const size_t jlo = basis_funs_1d( yvals_, y, order_, ycoef1d_, isUniformY_ );
#endif
  double val = 0.0;
  
  switch ( order_ ){
    case 1:
      val = fvals_[ilo  +(jlo  )*nx] * xcoef1d_[0] * ycoef1d_[0]
      + fvals_[ilo+1+(jlo  )*nx] * xcoef1d_[1] * ycoef1d_[0]
      + fvals_[ilo  +(jlo+1)*nx] * xcoef1d_[0] * ycoef1d_[1]
      + fvals_[ilo+1+(jlo+1)*nx] * xcoef1d_[1] * ycoef1d_[1];
      break;
    default:
      for( size_t j=0; j<=order_; ++j ){
        const size_t jix = (jlo+j)*nx;
        for( size_t i=0; i<=order_; ++i ){
          val += fvals_[ ilo+i + jix ] * xcoef1d_[i] * ycoef1d_[j];
        }
      }
      break;
  }
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
  gpu_val[idx] = val;
#else
  return val;
#endif

}
//-------------------------------------------------------------------
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__global__ void derivative2D(double* gpu_val,
                             const double* const x1,
                             const double* const x2,
                             const double* const xvals_,
                             const double* const yvals_,
                             const double* const fvals_,
                             const size_t* xvalsize,
                             const unsigned int order_,
                             const bool* isUniform,
                             const bool allowClipping,
                             const double* const bounds,
                             const size_t indepsize,
                             const int dim)
{
  
  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) return;
  const double x = allowClipping ? min( max( x1[idx], bounds[0] ), bounds[1] ) : x1[idx];
  const double y = allowClipping ? min( max( x2[idx], bounds[2] ), bounds[3] ) : x2[idx];
  
  double xcoef1d_[5], ycoef1d_[5]; // use order + 1 instead of 5
  size_t ilo, jlo;
  if( dim==0 ){
    ilo = gpu_der_basis_funs_1d( xvals_, x, order_, &xcoef1d_[0], xvalsize[0], isUniform[0] );
    jlo =     gpu_basis_funs_1d( yvals_, y, order_, &ycoef1d_[0], xvalsize[1], isUniform[1] );
  }
  else{
    ilo =     gpu_basis_funs_1d( xvals_, x, order_, &xcoef1d_[0], xvalsize[0], isUniform[0] );
    jlo = gpu_der_basis_funs_1d( yvals_, y, order_, &ycoef1d_[0], xvalsize[1], isUniform[1] );
  }
  const size_t nx = xvalsize[0];
#else
double
LagrangeInterpolant2D::derivative( const double* const indep, const int dim ) const
{
  assert( dim <  2 );
  assert( dim >= 0);
  const size_t nx = xvals_.size();
  
  const double x = allowClipping_ ? std::min( std::max( indep[0], xbounds_.first ), xbounds_.second ) : indep[0];
  const double y = allowClipping_ ? std::min( std::max( indep[1], ybounds_.first ), ybounds_.second ) : indep[1];
  
  size_t ilo, jlo;
  if( dim==0 ){
    ilo = der_basis_funs_1d( xvals_, x, order_, xcoef1d_, isUniformX_ );
    jlo =     basis_funs_1d( yvals_, y, order_, ycoef1d_, isUniformY_ );
  }
  else{
    ilo =     basis_funs_1d( xvals_, x, order_, xcoef1d_, isUniformX_ );
    jlo = der_basis_funs_1d( yvals_, y, order_, ycoef1d_, isUniformY_ );
  }
# endif
  double val = 0.0;
  switch ( order_ ){
    case 1:
      val = fvals_[ilo  +(jlo  )*nx] * xcoef1d_[0] * ycoef1d_[0]
      + fvals_[ilo+1+(jlo  )*nx] * xcoef1d_[1] * ycoef1d_[0]
      + fvals_[ilo  +(jlo+1)*nx] * xcoef1d_[0] * ycoef1d_[1]
      + fvals_[ilo+1+(jlo+1)*nx] * xcoef1d_[1] * ycoef1d_[1];
      break;
    default:
      for( size_t j=0; j<=order_; ++j ){
        const size_t jix = (jlo+j)*nx;
        for( size_t i=0; i<=order_; ++i ){
          val += fvals_[ ilo+i + jix ] * xcoef1d_[i] * ycoef1d_[j];
        }
      }
      break;
  }
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
  gpu_val[idx] = val;
#else
  return val;
#endif
}
  
//-------------------------------------------------------------------

#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__global__ void second_derivative2D(double* gpu_val,
                                    const double* const x1,
                                    const double* const x2,
                                    const double* const xvals_,
                                    const double* const yvals_,
                                    const double* const fvals_,
                                    const size_t* xvalsize,
                                    const unsigned int order_,
                                    const bool* isUniform,
                                    const bool allowClipping,
                                    const double* const bounds,
                                    const size_t indepsize,
                                    const int dim1,
                                    const int dim2)
{
  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) return;
  
  gpu_val[idx] = 0.0;
  if( order_ == 1 ) return;

  const double x = allowClipping ? min( max( x1[idx], bounds[0] ), bounds[1] ) : x1[idx];
  const double y = allowClipping ? min( max( x2[idx], bounds[2] ), bounds[3] ) : x2[idx];
  double xcoefs[5], ycoefs[5]; // CUDA compiler needs comiple time size!
  size_t ilo=0, jlo=0;
  
  switch( dim1 ){
    case 0:{
      switch( dim2 ){
        case 0:{
          ilo = gpu_second_der_basis_funs_1d( xvals_, x, order_, &xcoefs[0], xvalsize[0], isUniform[0] );
          jlo =            gpu_basis_funs_1d( yvals_, y, order_, &ycoefs[0], xvalsize[1], isUniform[1] );
          break;
        }
        case 1:{
          ilo = gpu_der_basis_funs_1d( xvals_, x, order_, &xcoefs[0], xvalsize[0], isUniform[0] );
          jlo = gpu_der_basis_funs_1d( yvals_, y, order_, &ycoefs[0], xvalsize[1], isUniform[1] );
          break;
        }
      }
      break;
    }
    case 1:{
      switch( dim2 ){
        case 0:{
          ilo = gpu_der_basis_funs_1d( xvals_, x, order_, &xcoefs[0], xvalsize[0], isUniform[0] );
          jlo = gpu_der_basis_funs_1d( yvals_, y, order_, &ycoefs[0], xvalsize[1], isUniform[1] );
          break;
        }
        case 1:{
          ilo =            gpu_basis_funs_1d( xvals_, x, order_, &xcoefs[0], xvalsize[0], isUniform[0] );
          jlo = gpu_second_der_basis_funs_1d( yvals_, y, order_, &ycoefs[0], xvalsize[1], isUniform[1] );
          break;
        }
      }
      break;
    }
  }

  for( size_t ky=0; ky<=order_; ++ky ){
    const size_t i = ilo + (ky+jlo)*xvalsize[0];
    for( size_t kx=0; kx<=order_; ++kx ){
      gpu_val[idx] += fvals_[kx+i] * xcoefs[kx] * ycoefs[ky];
    }
  }
}
  
#else
double
LagrangeInterpolant2D::second_derivative( const double* const indep, const int dim1, const int dim2 ) const
{
  assert( dim1 < 2 );
  assert( dim2 < 2 );
  
  const double x = allowClipping_ ? std::min( std::max( indep[0], xbounds_.first ), xbounds_.second ) : indep[0];
  const double y = allowClipping_ ? std::min( std::max( indep[1], ybounds_.first ), ybounds_.second ) : indep[1];
  double xcoefs[order_+1], ycoefs[order_+1];
  
  size_t ilo=0, jlo=0;
  
  switch( dim1 ){
    case 0:{
      switch( dim2 ){
        case 0:{
          ilo = second_der_basis_funs_1d( xvals_, x, order_, xcoefs, isUniformX_ );
          jlo =            basis_funs_1d( yvals_, y, order_, ycoefs, isUniformY_ );
          break;
        }
        case 1:{
          ilo = der_basis_funs_1d( xvals_, x, order_, xcoefs, isUniformX_ );
          jlo = der_basis_funs_1d( yvals_, y, order_, ycoefs, isUniformY_ );
          break;
        }
      }
      break;
    }
    case 1:{
      switch( dim2 ){
        case 0:{
          ilo = der_basis_funs_1d( xvals_, x, order_, xcoefs, isUniformX_ );
          jlo = der_basis_funs_1d( yvals_, y, order_, ycoefs, isUniformY_ );
          break;
        }
        case 1:{
          ilo =            basis_funs_1d( xvals_, x, order_, xcoefs, isUniformX_ );
          jlo = second_der_basis_funs_1d( yvals_, y, order_, ycoefs, isUniformY_ );
          break;
        }
      }
      break;
    }
    default:
      throw std::invalid_argument("invalid dimension specification for second derivative");
  }
  const size_t nx = xvals_.size();
  double val = 0.0;
  for( size_t ky=0; ky<=order_; ++ky ){
    const size_t i = ilo + (ky+jlo)*nx;
    for( size_t kx=0; kx<=order_; ++kx ){
      val += fvals_[kx+i] * xcoefs[kx] * ycoefs[ky];
    }
  }
  return val;
}
#endif
//===================================================================
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__global__ void value3D(double* gpu_val,
                                const double* const x1,
                                const double* const x2,
                                const double* const x3,
                                const double* const xvals,
                                const double* const yvals,
                                const double* const zvals,
                                const double* const fvals_,
                                const size_t* xvalsize,
                                const unsigned int order_,
                                const bool* isUniform,
                                const bool allowClipping,
                                const double* const bounds,
                                const size_t indepsize)
{
  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) {
    return;
  }
  const double x = allowClipping ? min( max( x1[idx], bounds[0] ), bounds[1] ) : x1[idx];
  const double y = allowClipping ? min( max( x2[idx], bounds[2] ), bounds[3] ) : x2[idx];
  const double z = allowClipping ? min( max( x3[idx], bounds[4] ), bounds[5] ) : x3[idx];
  
  double xcoef1d_[5], ycoef1d_[5], zcoef1d_[5];
  const size_t ilo = gpu_basis_funs_1d( xvals, x, order_, &xcoef1d_[0], xvalsize[0], isUniform[0] );
  const size_t jlo = gpu_basis_funs_1d( yvals, y, order_, &ycoef1d_[0], xvalsize[1], isUniform[1] );
  const size_t klo = gpu_basis_funs_1d( zvals, z, order_, &zcoef1d_[0], xvalsize[2], isUniform[2] );
  
  const size_t nx = xvalsize[0];
  const size_t ny = xvalsize[1];
#else
double
LagrangeInterpolant3D::value( const double* const indep ) const
{
  const size_t nx = xvals_.size();
  const size_t ny = yvals_.size();
  
  const double x = allowClipping_ ? std::min( std::max( indep[0], xbounds_.first ), xbounds_.second ) : indep[0];
  const double y = allowClipping_ ? std::min( std::max( indep[1], ybounds_.first ), ybounds_.second ) : indep[1];
  const double z = allowClipping_ ? std::min( std::max( indep[2], zbounds_.first ), zbounds_.second ) : indep[2];
  
  const size_t ilo = basis_funs_1d( xvals_, x, order_, xcoef1d_, isUniformX_ );
  const size_t jlo = basis_funs_1d( yvals_, y, order_, ycoef1d_, isUniformY_ );
  const size_t klo = basis_funs_1d( zvals_, z, order_, zcoef1d_, isUniformZ_ );
#endif
  double val = 0.0;
  for( size_t k=0; k<=order_; ++k ){
    const size_t kix = (klo+k)*nx*ny;
    for( size_t j=0; j<=order_; ++j ){
      const size_t jix = (jlo+j)*nx;
      const double tmp = ycoef1d_[j] * zcoef1d_[k];
      for( size_t i=0; i<=order_; ++i ){
        const size_t ix = ilo+i + jix + kix;
        val += fvals_[ix] * xcoef1d_[i] * tmp;
      }
    }
  }
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
  gpu_val[idx] = val;
#else
  return val;
#endif

}
//-------------------------------------------------------------------
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__global__ void derivative3D(double* gpu_val,
                        const double* const x1,
                        const double* const x2,
                        const double* const x3,
                        const double* const xvals,
                        const double* const yvals,
                        const double* const zvals,
                        const double* const fvals_,
                        const size_t* xvalsize,
                        const unsigned int order_,
                        const bool* isUniform,
                        const bool allowClipping,
                        const double* const bounds,
                        const size_t indepsize,
                        const int dim)
{
  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) return;

  const double x = allowClipping ? min( max( x1[idx], bounds[0] ), bounds[1] ) : x1[idx];
  const double y = allowClipping ? min( max( x2[idx], bounds[2] ), bounds[3] ) : x2[idx];
  const double z = allowClipping ? min( max( x3[idx], bounds[4] ), bounds[5] ) : x3[idx];
  
  double xcoef1d_[5], ycoef1d_[5], zcoef1d_[5];
  size_t ilo, jlo, klo;
  if( dim==0 ){
    ilo = gpu_der_basis_funs_1d( xvals, x, order_, &xcoef1d_[0], xvalsize[0], isUniform[0] );
    jlo =     gpu_basis_funs_1d( yvals, y, order_, &ycoef1d_[0], xvalsize[1], isUniform[1] );
    klo =     gpu_basis_funs_1d( zvals, z, order_, &zcoef1d_[0], xvalsize[2], isUniform[2] );
  }
  else if( dim==1 ){
    ilo =     gpu_basis_funs_1d( xvals, x, order_, &xcoef1d_[0], xvalsize[0], isUniform[0] );
    jlo = gpu_der_basis_funs_1d( yvals, y, order_, &ycoef1d_[0], xvalsize[1], isUniform[1] );
    klo =     gpu_basis_funs_1d( zvals, z, order_, &zcoef1d_[0], xvalsize[2], isUniform[2] );
  }
  else{
    assert( dim==2 );
    ilo =     gpu_basis_funs_1d( xvals, x, order_, &xcoef1d_[0], xvalsize[0], isUniform[0] );
    jlo =     gpu_basis_funs_1d( yvals, y, order_, &ycoef1d_[0], xvalsize[1], isUniform[1] );
    klo = gpu_der_basis_funs_1d( zvals, z, order_, &zcoef1d_[0], xvalsize[2], isUniform[2] );
  }
  
  const size_t nx = xvalsize[0];
  const size_t ny = xvalsize[1];
#else
double
LagrangeInterpolant3D::derivative( const double* const indep, const int dim ) const
{
  const size_t nx = xvals_.size();
  const size_t ny = yvals_.size();
  
  const double x = allowClipping_ ? std::min( std::max( indep[0], xbounds_.first ), xbounds_.second ) : indep[0];
  const double y = allowClipping_ ? std::min( std::max( indep[1], ybounds_.first ), ybounds_.second ) : indep[1];
  const double z = allowClipping_ ? std::min( std::max( indep[2], zbounds_.first ), zbounds_.second ) : indep[2];
  
  size_t ilo, jlo, klo;
  if( dim==0 ){
    ilo = der_basis_funs_1d( xvals_, x, order_, xcoef1d_, isUniformX_ );
    jlo =     basis_funs_1d( yvals_, y, order_, ycoef1d_, isUniformY_ );
    klo =     basis_funs_1d( zvals_, z, order_, zcoef1d_, isUniformZ_ );
  }
  else if( dim==1 ){
    ilo =     basis_funs_1d( xvals_, x, order_, xcoef1d_, isUniformX_ );
    jlo = der_basis_funs_1d( yvals_, y, order_, ycoef1d_, isUniformY_ );
    klo =     basis_funs_1d( zvals_, z, order_, zcoef1d_, isUniformZ_ );
  }
  else{
    assert( dim==2 );
    ilo =     basis_funs_1d( xvals_, x, order_, xcoef1d_, isUniformX_ );
    jlo =     basis_funs_1d( yvals_, y, order_, ycoef1d_, isUniformY_ );
    klo = der_basis_funs_1d( zvals_, z, order_, zcoef1d_, isUniformZ_ );
  }
# endif
  double val = 0.0;
  
  for( size_t k=0; k<=order_; ++k ){
    const size_t kix = (klo+k)*nx*ny;
    for( size_t j=0; j<=order_; ++j ){
      const size_t jix = (jlo+j)*nx;
      const double tmp = ycoef1d_[j] * zcoef1d_[k];
      for( size_t i=0; i<=order_; ++i ){
        const size_t ix = ilo+i + jix + kix;
        val += fvals_[ix] * xcoef1d_[i] * tmp;
      }
    }
  }
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
  gpu_val[idx] = val;
#else
  return val;
#endif
}
  
  

//-------------------------------------------------------------------
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__global__ void second_derivative3D(double* gpu_val,
                                    const double* const x1,
                                    const double* const x2,
                                    const double* const x3,
                                    const double* const xvals,
                                    const double* const yvals,
                                    const double* const zvals,
                                    const double* const fvals_,
                                    const size_t* xvalsize,
                                    const unsigned int order_,
                                    const bool* isUniform,
                                    const bool allowClipping,
                                    const double* const bounds,
                                    const size_t indepsize,
                                    const int dim1,
                                    const int dim2)
{
  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) return;

  gpu_val[idx] = 0.0;
  if( order_ == 1 ) return;

  const double x = allowClipping ? min( max( x1[idx], bounds[0] ), bounds[1] ) : x1[idx];
  const double y = allowClipping ? min( max( x2[idx], bounds[2] ), bounds[3] ) : x2[idx];
  const double z = allowClipping ? min( max( x3[idx], bounds[4] ), bounds[5] ) : x3[idx];
  
  double xcoefs[5], ycoefs[5], zcoefs[5];
  
  size_t ilo=0, jlo=0, klo=0;
  
  switch( dim1 ){
    case 0:{
      switch( dim2 ){
        case 0:{
          ilo = gpu_second_der_basis_funs_1d( xvals, x, order_, &xcoefs[0], xvalsize[0], isUniform[0] );
          jlo =            gpu_basis_funs_1d( yvals, y, order_, &ycoefs[0], xvalsize[1], isUniform[1] );
          klo =            gpu_basis_funs_1d( zvals, z, order_, &zcoefs[0], xvalsize[2], isUniform[2] );
          break;
        }
        case 1:{
          ilo = gpu_der_basis_funs_1d( xvals, x, order_, &xcoefs[0], xvalsize[0], isUniform[0] );
          jlo = gpu_der_basis_funs_1d( yvals, y, order_, &ycoefs[0], xvalsize[1], isUniform[1] );
          klo =     gpu_basis_funs_1d( zvals, z, order_, &zcoefs[0], xvalsize[2], isUniform[2] );
          break;
        }
        case 2:{
          ilo = gpu_der_basis_funs_1d( xvals, x, order_, &xcoefs[0], xvalsize[0], isUniform[0] );
          jlo =     gpu_basis_funs_1d( yvals, y, order_, &ycoefs[0], xvalsize[1], isUniform[1] );
          klo = gpu_der_basis_funs_1d( zvals, x, order_, &zcoefs[0], xvalsize[2], isUniform[2] );
          break;
        }
      }
      break;
    }
    case 1:{
      switch( dim2 ){
        case 0:{
          ilo = gpu_der_basis_funs_1d( xvals, x, order_, &xcoefs[0], xvalsize[0], isUniform[0] );
          jlo = gpu_der_basis_funs_1d( yvals, y, order_, &ycoefs[0], xvalsize[1], isUniform[1] );
          klo =     gpu_basis_funs_1d( zvals, z, order_, &zcoefs[0], xvalsize[2], isUniform[2] );
          break;
        }
        case 1:{
          ilo =     gpu_basis_funs_1d( xvals, x, order_, &xcoefs[0], xvalsize[0], isUniform[0] );
          jlo = gpu_der_basis_funs_1d( yvals, y, order_, &ycoefs[0], xvalsize[1], isUniform[1] );
          klo =     gpu_basis_funs_1d( zvals, z, order_, &zcoefs[0], xvalsize[2], isUniform[2] );
          break;
        }
        case 2:{
          ilo =     gpu_basis_funs_1d( xvals, x, order_, &xcoefs[0], xvalsize[0], isUniform[0] );
          jlo = gpu_der_basis_funs_1d( yvals, y, order_, &ycoefs[0], xvalsize[1], isUniform[1] );
          klo = gpu_der_basis_funs_1d( zvals, x, order_, &zcoefs[0], xvalsize[2], isUniform[2] );
          break;
        }
      }
      break;
    }
    case 2:{
      switch( dim2 ){
        case 0:{
          ilo = gpu_der_basis_funs_1d( xvals, x, order_, &xcoefs[0], xvalsize[0], isUniform[0] );
          jlo =     gpu_basis_funs_1d( yvals, y, order_, &ycoefs[0], xvalsize[1], isUniform[1] );
          klo = gpu_der_basis_funs_1d( zvals, x, order_, &zcoefs[0], xvalsize[2], isUniform[2] );
          break;
        }
        case 1:{
          ilo =     gpu_basis_funs_1d( xvals, x, order_, &xcoefs[0], xvalsize[0], isUniform[0] );
          jlo = gpu_der_basis_funs_1d( yvals, y, order_, &ycoefs[0], xvalsize[1], isUniform[1] );
          klo = gpu_der_basis_funs_1d( zvals, x, order_, &zcoefs[0], xvalsize[2], isUniform[2] );
          break;
        }
        case 2:{
          ilo =     gpu_basis_funs_1d( xvals, x, order_, &xcoefs[0], xvalsize[0], isUniform[0] );
          jlo =     gpu_basis_funs_1d( yvals, y, order_, &ycoefs[0], xvalsize[1], isUniform[1] );
          klo = gpu_der_basis_funs_1d( zvals, x, order_, &zcoefs[0], xvalsize[2], isUniform[2] );
          break;
        }
      }
      break;
    }
  }

  for( size_t kz=0; kz<=order_; ++kz ){
    const size_t k = klo + kz;
    for( size_t ky=0; ky<=order_; ++ky ){
      const size_t j = jlo + ky;
      for( size_t kx=0; kx<=order_; ++kx ){
        gpu_val[idx] += fvals_[ilo+kx + j*xvalsize[0] + k*xvalsize[0]*xvalsize[1]] * xcoefs[kx]*ycoefs[ky]*zcoefs[kz];
      }
    }
  }
}
#else
double
LagrangeInterpolant3D::second_derivative( const double* const indep, const int dim1, const int dim2 ) const
{
  if( order_ == 1 ) return 0;
  
  assert( dim1 < 3 );
  assert( dim2 < 3 );
  
  const double x = allowClipping_ ? std::min( std::max( indep[0], xbounds_.first ), xbounds_.second ) : indep[0];
  const double y = allowClipping_ ? std::min( std::max( indep[1], ybounds_.first ), ybounds_.second ) : indep[1];
  const double z = allowClipping_ ? std::min( std::max( indep[2], zbounds_.first ), zbounds_.second ) : indep[2];
  
  double xcoefs[order_+1], ycoefs[order_+1], zcoefs[order_+1];
  
  size_t ilo=0, jlo=0, klo=0;
  
  switch( dim1 ){
    case 0:{
      switch( dim2 ){
        case 0:{
          ilo = second_der_basis_funs_1d( xvals_, x, order_, xcoefs, isUniformX_ );
          jlo =            basis_funs_1d( yvals_, y, order_, ycoefs, isUniformY_ );
          klo =            basis_funs_1d( zvals_, z, order_, zcoefs, isUniformZ_ );
          break;
        }
        case 1:{
          ilo = der_basis_funs_1d( xvals_, x, order_, xcoefs, isUniformX_ );
          jlo = der_basis_funs_1d( yvals_, y, order_, ycoefs, isUniformY_ );
          klo =     basis_funs_1d( zvals_, z, order_, zcoefs, isUniformZ_ );
          break;
        }
        case 2:{
          ilo = der_basis_funs_1d( xvals_, x, order_, xcoefs, isUniformX_ );
          jlo =     basis_funs_1d( yvals_, y, order_, ycoefs, isUniformY_ );
          klo = der_basis_funs_1d( zvals_, z, order_, zcoefs, isUniformZ_ );
          break;
        }
      }
      break;
    }
    case 1:{
      switch( dim2 ){
        case 0:{
          ilo = der_basis_funs_1d( xvals_, x, order_, xcoefs, isUniformX_ );
          jlo = der_basis_funs_1d( yvals_, y, order_, ycoefs, isUniformY_ );
          klo =     basis_funs_1d( zvals_, z, order_, zcoefs, isUniformZ_ );
          break;
        }
        case 1:{
          ilo =            basis_funs_1d( xvals_, x, order_, xcoefs, isUniformX_ );
          jlo = second_der_basis_funs_1d( yvals_, y, order_, ycoefs, isUniformY_ );
          klo =            basis_funs_1d( zvals_, z, order_, zcoefs, isUniformZ_ );
          break;
        }
        case 2:{
          ilo =     basis_funs_1d( xvals_, x, order_, xcoefs, isUniformX_ );
          jlo = der_basis_funs_1d( yvals_, y, order_, ycoefs, isUniformY_ );
          klo = der_basis_funs_1d( zvals_, z, order_, zcoefs, isUniformZ_ );
          break;
        }
      }
      break;
    }
    case 2:{
      switch( dim2 ){
        case 0:{
          ilo = der_basis_funs_1d( xvals_, x, order_, xcoefs, isUniformX_ );
          jlo =     basis_funs_1d( yvals_, y, order_, ycoefs, isUniformY_ );
          klo = der_basis_funs_1d( zvals_, z, order_, zcoefs, isUniformZ_ );
          break;
        }
        case 1:{
          ilo =     basis_funs_1d( xvals_, x, order_, xcoefs, isUniformX_ );
          jlo = der_basis_funs_1d( yvals_, y, order_, ycoefs, isUniformY_ );
          klo = der_basis_funs_1d( zvals_, z, order_, zcoefs, isUniformZ_ );
          break;
        }
        case 2:{
          ilo =            basis_funs_1d( xvals_, x, order_, xcoefs, isUniformX_ );
          jlo =            basis_funs_1d( yvals_, y, order_, ycoefs, isUniformY_ );
          klo = second_der_basis_funs_1d( zvals_, z, order_, zcoefs, isUniformZ_ );
          break;
        }
      }
      break;
    }
    default:
      throw std::invalid_argument("invalid dimension specification for second derivative");
  }
  
  const size_t nx = xvals_.size();
  const size_t ny = yvals_.size();
  double val = 0.0;
  for( size_t kz=0; kz<=order_; ++kz ){
    const size_t k = klo + kz;
    for( size_t ky=0; ky<=order_; ++ky ){
      const size_t j = jlo + ky;
      for( size_t kx=0; kx<=order_; ++kx ){
        val += fvals_[ilo+kx + j*nx + k*nx*ny] * xcoefs[kx]*ycoefs[ky]*zcoefs[kz];
      }
    }
  }
  return val;
}
#endif
//===================================================================
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__global__ void value4D(double* gpu_val,
                        const double* x1,
                        const double* x2,
                        const double* x3,
                        const double* x4,
                        const double* const x1vals,
                        const double* const x2vals,
                        const double* const x3vals,
                        const double* const x4vals,
                        const double* const fvals_,
                        const size_t* xvalsize,
                        const unsigned int order_,
                        const bool* isUniform,
                        const bool allowClipping,
                        const double* const bounds,
                        const size_t indepsize)

{
  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) {
    return;
  }
  const double xx1 = allowClipping ? min( max( x1[idx], bounds[0] ), bounds[1] ) : x1[idx];
  const double xx2 = allowClipping ? min( max( x2[idx], bounds[2] ), bounds[3] ) : x2[idx];
  const double xx3 = allowClipping ? min( max( x3[idx], bounds[4] ), bounds[5] ) : x3[idx];
  const double xx4 = allowClipping ? min( max( x4[idx], bounds[6] ), bounds[7] ) : x4[idx];

  double x1coef1d_[5], x2coef1d_[5], x3coef1d_[5], x4coef1d_[5];
  const size_t i1lo = gpu_basis_funs_1d( x1vals, xx1, order_, &x1coef1d_[0], xvalsize[0], isUniform[0] );
  const size_t i2lo = gpu_basis_funs_1d( x2vals, xx2, order_, &x2coef1d_[0], xvalsize[1], isUniform[1] );
  const size_t i3lo = gpu_basis_funs_1d( x3vals, xx3, order_, &x3coef1d_[0], xvalsize[2], isUniform[2] );
  const size_t i4lo = gpu_basis_funs_1d( x4vals, xx4, order_, &x4coef1d_[0], xvalsize[3], isUniform[3] );
  
  const size_t nx1 = xvalsize[0];
  const size_t nx2 = xvalsize[1];
  const size_t nx3 = xvalsize[2];
#else
double
LagrangeInterpolant4D::value( const double* const indep ) const
{
  const size_t nx1 = x1vals_.size();
  const size_t nx2 = x2vals_.size();
  const size_t nx3 = x3vals_.size();
    
  const double x1 = allowClipping_ ? std::min( std::max( indep[0], bounds_[0].first ), bounds_[0].second ) : indep[0];
  const double x2 = allowClipping_ ? std::min( std::max( indep[1], bounds_[1].first ), bounds_[1].second ) : indep[1];
  const double x3 = allowClipping_ ? std::min( std::max( indep[2], bounds_[2].first ), bounds_[2].second ) : indep[2];
  const double x4 = allowClipping_ ? std::min( std::max( indep[3], bounds_[3].first ), bounds_[3].second ) : indep[3];
    
  const size_t i1lo = basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
  const size_t i2lo = basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
  const size_t i3lo = basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
  const size_t i4lo = basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
#endif
 
  double val = 0.0;
  
  for( size_t k4=0; k4<=order_; ++k4 ){
    const size_t i4 = (i4lo+k4)*nx1*nx2*nx3;
    for( size_t k3=0; k3<=order_; ++k3 ){
      const double tmp = x3coef1d_[k3] * x4coef1d_[k4];
      const size_t i3 = (i3lo+k3)*nx1*nx2;
      for( size_t k2=0; k2<=order_; ++k2 ){
        const double tmp2 = x2coef1d_[k2] * tmp;
        const size_t i2 = (i2lo+k2)*nx1;
        for( size_t k1=0; k1<=order_; ++k1 ){
          const size_t i1 = i1lo+k1 + i2 + i3 + i4;
#         ifndef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
          assert( i1 < fvals_.size() );
#         endif
          val += fvals_[i1] * x1coef1d_[k1] * tmp2;
        }
      }
    }
  }
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
  gpu_val[idx] = val;
#else
  return val;
#endif

}
//-------------------------------------------------------------------
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__global__ void derivative4D(double* gpu_val,
                             const double* x1,
                             const double* x2,
                             const double* x3,
                             const double* x4,
                             const double* const x1vals,
                             const double* const x2vals,
                             const double* const x3vals,
                             const double* const x4vals,
                             const double* const fvals_,
                             const size_t* xvalsize,
                             const unsigned int order_,
                             const bool* isUniform,
                             const bool allowClipping,
                             const double* const bounds,
                             const size_t indepsize,
                             const int dim)
  
{
  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) return;

  const double xx1 = allowClipping ? min( max( x1[idx], bounds[0] ), bounds[1] ) : x1[idx];
  const double xx2 = allowClipping ? min( max( x2[idx], bounds[2] ), bounds[3] ) : x2[idx];
  const double xx3 = allowClipping ? min( max( x3[idx], bounds[4] ), bounds[5] ) : x3[idx];
  const double xx4 = allowClipping ? min( max( x4[idx], bounds[6] ), bounds[7] ) : x4[idx];
  
  double x1coef1d_[5], x2coef1d_[5], x3coef1d_[5], x4coef1d_[5];
  size_t i1lo, i2lo, i3lo, i4lo;
  switch( dim ){
    case 0:
      i1lo = gpu_der_basis_funs_1d( x1vals, xx1, order_, &x1coef1d_[0], xvalsize[0], isUniform[0] );
      i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coef1d_[0], xvalsize[1], isUniform[1] );
      i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coef1d_[0], xvalsize[2], isUniform[2] );
      i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coef1d_[0], xvalsize[3], isUniform[3] );
      break;
    case 1:
      i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coef1d_[0], xvalsize[0], isUniform[0] );
      i2lo = gpu_der_basis_funs_1d( x2vals, xx2, order_, &x2coef1d_[0], xvalsize[1], isUniform[1] );
      i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coef1d_[0], xvalsize[2], isUniform[2] );
      i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coef1d_[0], xvalsize[3], isUniform[3] );
      break;
    case 2:
      i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coef1d_[0], xvalsize[0], isUniform[0] );
      i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coef1d_[0], xvalsize[1], isUniform[1] );
      i3lo = gpu_der_basis_funs_1d( x3vals, xx3, order_, &x3coef1d_[0], xvalsize[2], isUniform[2] );
      i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coef1d_[0], xvalsize[3], isUniform[3] );
      break;
    default:
      assert( dim==3 );
      i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coef1d_[0], xvalsize[0], isUniform[0] );
      i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coef1d_[0], xvalsize[1], isUniform[1] );
      i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coef1d_[0], xvalsize[2], isUniform[2] );
      i4lo = gpu_der_basis_funs_1d( x4vals, xx4, order_, &x4coef1d_[0], xvalsize[3], isUniform[3] );
  } // switch(dim)

  
  const size_t nx1 = xvalsize[0];
  const size_t nx2 = xvalsize[1];
  const size_t nx3 = xvalsize[2];
#else
double
LagrangeInterpolant4D::derivative( const double* const indep,
                                   const int dim ) const
{
  const size_t nx1 = x1vals_.size();
  const size_t nx2 = x2vals_.size();
  const size_t nx3 = x3vals_.size();
  
  const double x1 = allowClipping_ ? std::min( std::max( indep[0], bounds_[0].first ), bounds_[0].second ) : indep[0];
  const double x2 = allowClipping_ ? std::min( std::max( indep[1], bounds_[1].first ), bounds_[1].second ) : indep[1];
  const double x3 = allowClipping_ ? std::min( std::max( indep[2], bounds_[2].first ), bounds_[2].second ) : indep[2];
  const double x4 = allowClipping_ ? std::min( std::max( indep[3], bounds_[3].first ), bounds_[3].second ) : indep[3];
  
  size_t i1lo, i2lo, i3lo, i4lo;
  switch( dim ){
    case 0:
      i1lo = der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
      i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
      i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
      i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
      break;
    case 1:
      i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0]);
      i2lo = der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1]);
      i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2]);
      i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3]);
      break;
    case 2:
      i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0]);
      i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1]);
      i3lo = der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2]);
      i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3]);
      break;
    default:
      assert( dim==3 );
      i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
      i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
      i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
      i4lo = der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
  } // switch(dim)
#endif
  double val = 0.0;
  
  for( size_t k4=0; k4<=order_; ++k4 ){
    const size_t i4 = (i4lo+k4)*nx1*nx2*nx3;
    for( size_t k3=0; k3<=order_; ++k3 ){
      const double tmp = x3coef1d_[k3] * x4coef1d_[k4];
      const size_t i3 = (i3lo+k3)*nx1*nx2;
      for( size_t k2=0; k2<=order_; ++k2 ){
        const double tmp2 = x2coef1d_[k2] * tmp;
        const size_t i2 = (i2lo+k2)*nx1;
        for( size_t k1=0; k1<=order_; ++k1 ){
          const size_t i1 = i1lo+k1 + i2 + i3 + i4;
#         ifndef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
          assert( i1 < fvals_.size() );
#         endif
          val += fvals_[i1] * x1coef1d_[k1] * tmp2;
        }
      }
    }
  }
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
  gpu_val[idx] = val;
#else
  return val;
#endif
}

//-------------------------------------------------------------------
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__global__ void second_derivative4D(double* gpu_val,
                                    const double* x1,
                                    const double* x2,
                                    const double* x3,
                                    const double* x4,
                                    const double* const x1vals,
                                    const double* const x2vals,
                                    const double* const x3vals,
                                    const double* const x4vals,
                                    const double* const fvals_,
                                    const size_t* xvalsize,
                                    const unsigned int order_,
                                    const bool* isUniform,
                                    const bool allowClipping,
                                    const double* const bounds,
                                    const size_t indepsize,
                                    const int dim1,
                                    const int dim2)
  
{
  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) return;

  gpu_val[idx] = 0.0;
  if( order_ == 1 ) return;

  const double xx1 = allowClipping ? min( max( x1[idx], bounds[0] ), bounds[1] ) : x1[idx];
  const double xx2 = allowClipping ? min( max( x2[idx], bounds[2] ), bounds[3] ) : x2[idx];
  const double xx3 = allowClipping ? min( max( x3[idx], bounds[4] ), bounds[5] ) : x3[idx];
  const double xx4 = allowClipping ? min( max( x4[idx], bounds[6] ), bounds[7] ) : x4[idx];
  
  double x1coefs[5], x2coefs[5], x3coefs[5], x4coefs[5];
  
  size_t i1lo, i2lo, i3lo, i4lo;
  
  switch( dim1 ){
    case 0:{
      switch( dim2 ){
        case 0:
          i1lo = gpu_second_der_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo =            gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo =            gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo =            gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          break;
        case 1:
          i1lo = gpu_der_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0],              isUniform[0] );
          i2lo = gpu_der_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0],              isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          break;
        case 2:
          i1lo = gpu_der_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0],              isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i2lo = gpu_der_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0],              isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          break;
        case 3:
          i1lo = gpu_der_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0],              isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo = gpu_der_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0],              isUniform[3] );
          break;
      }
      break;
    } // case 0 on switch(dim1)
    case 1:{
      switch( dim2 ){
        case 0:
          i1lo = gpu_der_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0],              isUniform[0] );
          i2lo = gpu_der_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0],              isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          break;
        case 1:
          i1lo =            gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo = gpu_second_der_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo =            gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo =            gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          break;
        case 2:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo = gpu_der_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0],              isUniform[1] );
          i3lo = gpu_der_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0],              isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          break;
        case 3:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo = gpu_der_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0],              isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo = gpu_der_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0],              isUniform[3] );
          break;
      }
      break;
    } // case 1 on switch(dim1)
    case 2:{
      switch( dim2 ){
        case 0:
          i1lo = gpu_der_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0],              isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo = gpu_der_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0],              isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          break;
        case 1:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo = gpu_der_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0],              isUniform[1] );
          i3lo = gpu_der_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0],              isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          break;
        case 2:
          i1lo =            gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo =            gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo = gpu_second_der_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo =            gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          break;
        case 3:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo = gpu_der_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0],              isUniform[2] );
          i4lo = gpu_der_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0],              isUniform[3] );
          break;
      }
      break;
    } // case 2 on switch(dim1)
    case 3:{
      switch( dim2 ){
        case 0:
          i1lo = gpu_der_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0],              isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo = gpu_der_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0],              isUniform[3] );
          break;
        case 1:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo = gpu_der_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0],              isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo = gpu_der_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0],              isUniform[3] );
          break;
        case 2:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo = gpu_der_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0],              isUniform[2] );
          i4lo = gpu_der_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0],              isUniform[3] );
          break;
        case 3:
          i1lo =            gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo =            gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo =            gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo = gpu_second_der_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          break;
      }
      break;
    } // case 3 on switch(dim1)
  }
  
  for( size_t k4=0; k4<=order_; ++k4 ){
    const size_t i4 = (i4lo+k4)*xvalsize[0]*xvalsize[1]*xvalsize[2];
    for( size_t k3=0; k3<=order_; ++k3 ){
      const double tmp = x3coefs[k3] * x4coefs[k4];
      const size_t i3 = (i3lo+k3)*xvalsize[0]*xvalsize[1];
      for( size_t k2=0; k2<=order_; ++k2 ){
        const double tmp2 = x2coefs[k2] * tmp;
        const size_t i2 = (i2lo+k2)*xvalsize[0];
        for( size_t k1=0; k1<=order_; ++k1 ){
          const size_t i1 = i1lo+k1 + i2 + i3 + i4;
          gpu_val[idx] += fvals_[i1] * x1coefs[k1] * tmp2;
        }
      }
    }
  }
}
#endif
  
//===================================================================
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__global__ void value5D(double* gpu_val,
                          const double* x1,
                          const double* x2,
                          const double* x3,
                          const double* x4,
                          const double* x5,
                          const double* const x1vals,
                          const double* const x2vals,
                          const double* const x3vals,
                          const double* const x4vals,
                          const double* const x5vals,
                          const double* const fvals_,
                          const size_t* xvalsize,
                          const unsigned int order_,
                          const bool* isUniform,
                          const bool allowClipping,
                          const double* const bounds,
                          const size_t indepsize)

{
  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) return;

  const size_t nx1 = xvalsize[0];
  const size_t nx2 = xvalsize[1];
  const size_t nx3 = xvalsize[2];
  const size_t nx4 = xvalsize[3];
  
  const double xx1 = allowClipping ? min( max( x1[idx], bounds[0] ), bounds[1] ) : x1[idx];
  const double xx2 = allowClipping ? min( max( x2[idx], bounds[2] ), bounds[3] ) : x2[idx];
  const double xx3 = allowClipping ? min( max( x3[idx], bounds[4] ), bounds[5] ) : x3[idx];
  const double xx4 = allowClipping ? min( max( x4[idx], bounds[6] ), bounds[7] ) : x4[idx];
  const double xx5 = allowClipping ? min( max( x5[idx], bounds[8] ), bounds[9] ) : x5[idx];
  
  double x1coef1d_[5], x2coef1d_[5], x3coef1d_[5], x4coef1d_[5], x5coef1d_[5];
  const size_t i1lo = gpu_basis_funs_1d( x1vals, xx1, order_, &x1coef1d_[0], xvalsize[0], isUniform[0] );
  const size_t i2lo = gpu_basis_funs_1d( x2vals, xx2, order_, &x2coef1d_[0], xvalsize[1], isUniform[1] );
  const size_t i3lo = gpu_basis_funs_1d( x3vals, xx3, order_, &x3coef1d_[0], xvalsize[2], isUniform[2] );
  const size_t i4lo = gpu_basis_funs_1d( x4vals, xx4, order_, &x4coef1d_[0], xvalsize[3], isUniform[3] );
  const size_t i5lo = gpu_basis_funs_1d( x5vals, xx5, order_, &x5coef1d_[0], xvalsize[4], isUniform[4] );
#else 
double
LagrangeInterpolant5D::value( const double* const indep ) const
{
  const size_t nx1 = x1vals_.size();
  const size_t nx2 = x2vals_.size();
  const size_t nx3 = x3vals_.size();
  const size_t nx4 = x4vals_.size();
    
  const double x1 = allowClipping_ ? std::min( std::max( indep[0], bounds_[0].first ), bounds_[0].second ) : indep[0];
  const double x2 = allowClipping_ ? std::min( std::max( indep[1], bounds_[1].first ), bounds_[1].second ) : indep[1];
  const double x3 = allowClipping_ ? std::min( std::max( indep[2], bounds_[2].first ), bounds_[2].second ) : indep[2];
  const double x4 = allowClipping_ ? std::min( std::max( indep[3], bounds_[3].first ), bounds_[3].second ) : indep[3];
  const double x5 = allowClipping_ ? std::min( std::max( indep[4], bounds_[4].first ), bounds_[4].second ) : indep[4];
    
  const size_t i1lo = basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
  const size_t i2lo = basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
  const size_t i3lo = basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
  const size_t i4lo = basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
  const size_t i5lo = basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
#endif
  double val = 0.0;
  
  for( size_t k5=0; k5<=order_; ++k5 ){
    const size_t i5 = (i5lo+k5)*nx1*nx2*nx3*nx4;
    for( size_t k4=0; k4<=order_; ++k4 ){
      const double tmp = x4coef1d_[k4] * x5coef1d_[k5];
      const size_t i4 = (i4lo+k4)*nx1*nx2*nx3;
      for( size_t k3=0; k3<=order_; ++k3 ){
        const double tmp2 = x3coef1d_[k3] * tmp;
        const size_t i3 = (i3lo+k3)*nx1*nx2;
        for( size_t k2=0; k2<=order_; ++k2 ){
          const double tmp3 = x2coef1d_[k2] * tmp2;
          const size_t i2 = (i2lo+k2)*nx1;
          for( size_t k1=0; k1<=order_; ++k1 ){
            const size_t i1 = i1lo+k1 + i2 + i3 + i4 + i5;
#           ifndef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
            assert( i1 < fvals_.size() );
#           endif
            val += fvals_[i1] * x1coef1d_[k1] * tmp3;
          }
        }
      }
    }
  }
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
  gpu_val[idx] = val;
#else
  return val;
#endif
}
//-------------------------------------------------------------------
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__global__ void derivative5D(double* gpu_val,
                          const double* x1,
                          const double* x2,
                          const double* x3,
                          const double* x4,
                          const double* x5,
                          const double* const x1vals,
                          const double* const x2vals,
                          const double* const x3vals,
                          const double* const x4vals,
                          const double* const x5vals,
                          const double* const fvals_,
                          const size_t* xvalsize,
                          const unsigned int order_,
                          const bool* isUniform,
                          const bool allowClipping,
                          const double* const bounds,
                          const size_t indepsize,
                          const int dim)
  
{
  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) return;
  
  const size_t nx1 = xvalsize[0];
  const size_t nx2 = xvalsize[1];
  const size_t nx3 = xvalsize[2];
  const size_t nx4 = xvalsize[3];
  
  const double xx1 = allowClipping ? min( max( x1[idx], bounds[0] ), bounds[1] ) : x1[idx];
  const double xx2 = allowClipping ? min( max( x2[idx], bounds[2] ), bounds[3] ) : x2[idx];
  const double xx3 = allowClipping ? min( max( x3[idx], bounds[4] ), bounds[5] ) : x3[idx];
  const double xx4 = allowClipping ? min( max( x4[idx], bounds[6] ), bounds[7] ) : x4[idx];
  const double xx5 = allowClipping ? min( max( x5[idx], bounds[8] ), bounds[9] ) : x5[idx];
  
  double x1coef1d_[5], x2coef1d_[5], x3coef1d_[5], x4coef1d_[5], x5coef1d_[5];
  size_t i1lo, i2lo, i3lo, i4lo, i5lo;
  switch( dim ){
    case 0:
      i1lo = gpu_der_basis_funs_1d( x1vals, xx1, order_, &x1coef1d_[0], xvalsize[0], isUniform[0] );
      i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coef1d_[0], xvalsize[1], isUniform[1] );
      i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coef1d_[0], xvalsize[2], isUniform[2] );
      i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coef1d_[0], xvalsize[3], isUniform[3] );
      i5lo =     gpu_basis_funs_1d( x5vals, xx5, order_, &x5coef1d_[0], xvalsize[4], isUniform[4] );
      break;
    case 1:
      i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coef1d_[0], xvalsize[0], isUniform[0] );
      i2lo = gpu_der_basis_funs_1d( x2vals, xx2, order_, &x2coef1d_[0], xvalsize[1], isUniform[1] );
      i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coef1d_[0], xvalsize[2], isUniform[2] );
      i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coef1d_[0], xvalsize[3], isUniform[3] );
      i5lo =     gpu_basis_funs_1d( x5vals, xx5, order_, &x5coef1d_[0], xvalsize[4], isUniform[4] );
      break;
    case 2:
      i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coef1d_[0], xvalsize[0], isUniform[0] );
      i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coef1d_[0], xvalsize[1], isUniform[1] );
      i3lo = gpu_der_basis_funs_1d( x3vals, xx3, order_, &x3coef1d_[0], xvalsize[2], isUniform[2] );
      i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coef1d_[0], xvalsize[3], isUniform[3] );
      i5lo =     gpu_basis_funs_1d( x5vals, xx5, order_, &x5coef1d_[0], xvalsize[4], isUniform[4] );
      break;
    case 3:
      i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coef1d_[0], xvalsize[0], isUniform[0] );
      i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coef1d_[0], xvalsize[1], isUniform[1] );
      i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coef1d_[0], xvalsize[2], isUniform[2] );
      i4lo = gpu_der_basis_funs_1d( x4vals, xx4, order_, &x4coef1d_[0], xvalsize[3], isUniform[3] );
      i5lo =     gpu_basis_funs_1d( x5vals, xx5, order_, &x5coef1d_[0], xvalsize[4], isUniform[4] );
      break;
    case 4:
      i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coef1d_[0], xvalsize[0], isUniform[0] );
      i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coef1d_[0], xvalsize[1], isUniform[1] );
      i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coef1d_[0], xvalsize[2], isUniform[2] );
      i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coef1d_[0], xvalsize[3], isUniform[3] );
      i5lo = gpu_der_basis_funs_1d( x5vals, xx5, order_, &x5coef1d_[0], xvalsize[4], isUniform[4] );
      break;
    default:
      assert(false);
  }
#else 
double
LagrangeInterpolant5D::derivative( const double* const indep,
                                   const int dim ) const
{
  const size_t nx1 = x1vals_.size();
  const size_t nx2 = x2vals_.size();
  const size_t nx3 = x3vals_.size();
  const size_t nx4 = x4vals_.size();
  
  const double x1 = allowClipping_ ? std::min( std::max( indep[0], bounds_[0].first ), bounds_[0].second ) : indep[0];
  const double x2 = allowClipping_ ? std::min( std::max( indep[1], bounds_[1].first ), bounds_[1].second ) : indep[1];
  const double x3 = allowClipping_ ? std::min( std::max( indep[2], bounds_[2].first ), bounds_[2].second ) : indep[2];
  const double x4 = allowClipping_ ? std::min( std::max( indep[3], bounds_[3].first ), bounds_[3].second ) : indep[3];
  const double x5 = allowClipping_ ? std::min( std::max( indep[4], bounds_[4].first ), bounds_[4].second ) : indep[4];
  
  size_t i1lo, i2lo, i3lo, i4lo, i5lo;
  
  switch( dim ){
    case 0:
      i1lo = der_basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
      i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
      i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
      i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
      i5lo =     basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
      break;
    case 1:
      i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
      i2lo = der_basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
      i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
      i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
      i5lo =     basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
      break;
    case 2:
      i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
      i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
      i3lo = der_basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
      i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
      i5lo =     basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
      break;
    case 3:
      i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
      i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
      i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
      i4lo = der_basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
      i5lo =     basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
      break;
    case 4:
      i1lo =     basis_funs_1d( x1vals_, x1, order_, x1coef1d_, isUniform_[0] );
      i2lo =     basis_funs_1d( x2vals_, x2, order_, x2coef1d_, isUniform_[1] );
      i3lo =     basis_funs_1d( x3vals_, x3, order_, x3coef1d_, isUniform_[2] );
      i4lo =     basis_funs_1d( x4vals_, x4, order_, x4coef1d_, isUniform_[3] );
      i5lo = der_basis_funs_1d( x5vals_, x5, order_, x5coef1d_, isUniform_[4] );
      break;
    default:
      assert(false);
  }
#endif
  double val = 0.0;
  
  for( size_t k5=0; k5<=order_; ++k5 ){
    const size_t i5 = (i5lo+k5)*nx1*nx2*nx3*nx4;
    for( size_t k4=0; k4<=order_; ++k4 ){
      const double tmp = x4coef1d_[k4] * x5coef1d_[k5];
      const size_t i4 = (i4lo+k4)*nx1*nx2*nx3;
      for( size_t k3=0; k3<=order_; ++k3 ){
        const double tmp2 = x3coef1d_[k3] * tmp;
        const size_t i3 = (i3lo+k3)*nx1*nx2;
        for( size_t k2=0; k2<=order_; ++k2 ){
          const double tmp3 = x2coef1d_[k2] * tmp2;
          const size_t i2 = (i2lo+k2)*nx1;
          for( size_t k1=0; k1<=order_; ++k1 ){
            const size_t i1 = i1lo+k1 + i2 + i3 + i4 + i5;
#           ifndef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
            assert( i1 < fvals_.size() );
#           endif
            val += fvals_[i1] * x1coef1d_[k1] * tmp3;
          }
        }
      }
    }
  }
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
  gpu_val[idx] = val;
#else
  return val;
#endif
}
//-------------------------------------------------------------------
#ifdef LAGRANGEINTERPOLANT_POLY_ENABLED_CUDA
__global__ void second_derivative5D(double* gpu_val,
                                    const double* x1,
                                    const double* x2,
                                    const double* x3,
                                    const double* x4,
                                    const double* x5,
                                    const double* const x1vals,
                                    const double* const x2vals,
                                    const double* const x3vals,
                                    const double* const x4vals,
                                    const double* const x5vals,
                                    const double* const fvals_,
                                    const size_t* xvalsize,
                                    const unsigned int order_,
                                    const bool* isUniform,
                                    const bool allowClipping,
                                    const double* const bounds,
                                    const size_t indepsize,
                                    const int dim1,
                                    const int dim2)
  
{
  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) return;

  gpu_val[idx] = 0.0;
  if( order_ == 1 ) return;
  
  const double xx1 = allowClipping ? min( max( x1[idx], bounds[0] ), bounds[1] ) : x1[idx];
  const double xx2 = allowClipping ? min( max( x2[idx], bounds[2] ), bounds[3] ) : x2[idx];
  const double xx3 = allowClipping ? min( max( x3[idx], bounds[4] ), bounds[5] ) : x3[idx];
  const double xx4 = allowClipping ? min( max( x4[idx], bounds[6] ), bounds[7] ) : x4[idx];
  const double xx5 = allowClipping ? min( max( x5[idx], bounds[8] ), bounds[9] ) : x5[idx];
  
  double x1coefs[5], x2coefs[5], x3coefs[5], x4coefs[5], x5coefs[5];
  
  size_t i1lo, i2lo, i3lo, i4lo, i5lo;
  
  switch( dim1 ){
    case 0:{
      switch( dim2 ){
        case 0:
          i1lo = gpu_second_der_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo =            gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo =            gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo =            gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo =            gpu_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
        case 1:
          i1lo = gpu_der_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0],              isUniform[0] );
          i2lo = gpu_der_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0],              isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo =     gpu_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
        case 2:
          i1lo = gpu_der_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0],              isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo = gpu_der_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0],              isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo =     gpu_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
        case 3:
          i1lo = gpu_der_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0],              isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo = gpu_der_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0],              isUniform[3] );
          i5lo =     gpu_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
        case 4:
          i1lo = gpu_der_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0],              isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo = gpu_der_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0],              isUniform[4] );
          break;
      }
      break;
    } // case 0 on switch(dim1)
    case 1:{
      switch( dim2 ){
        case 0:
          i1lo = gpu_der_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0],              isUniform[0] );
          i2lo = gpu_der_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0],              isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo =     gpu_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
        case 1:
          i1lo =            gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo = gpu_second_der_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo =            gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo =            gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo =            gpu_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
        case 2:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo = gpu_der_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0],              isUniform[1] );
          i3lo = gpu_der_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0],              isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo =     gpu_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
        case 3:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo = gpu_der_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0],              isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo = gpu_der_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0],              isUniform[3] );
          i5lo =     gpu_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
        case 4:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo = gpu_der_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0],              isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo = gpu_der_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0],              isUniform[4] );
          break;
      }
      break;
    } // case 1 on switch(dim1)
    case 2:{
      switch( dim2 ){
        case 0:
          i1lo = gpu_der_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0],              isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo = gpu_der_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0],              isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo =     gpu_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
        case 1:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo = gpu_der_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0],              isUniform[1] );
          i3lo = gpu_der_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0],              isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo =     gpu_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
        case 2:
          i1lo =            gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo =            gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo = gpu_second_der_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo =            gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo =            gpu_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
        case 3:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo = gpu_der_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0],              isUniform[2] );
          i4lo = gpu_der_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0],              isUniform[3] );
          i5lo =     gpu_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
        case 4:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo = gpu_der_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0],              isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo = gpu_der_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0],              isUniform[4] );
          break;
      }
      break;
    } // case 2 on switch(dim1)
    case 3:{
      switch( dim2 ){
        case 0:
          i1lo = gpu_der_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0],              isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo = gpu_der_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0],              isUniform[3] );
          i5lo =     gpu_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
        case 1:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo = gpu_der_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0],              isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo = gpu_der_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0],              isUniform[3] );
          i5lo =     gpu_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
        case 2:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo = gpu_der_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0],              isUniform[2] );
          i4lo = gpu_der_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0],              isUniform[3] );
          i5lo =     gpu_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
        case 3:
          i1lo =            gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo =            gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo =            gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo = gpu_second_der_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo =            gpu_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
        case 4:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo = gpu_der_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0],              isUniform[3] );
          i5lo = gpu_der_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0],              isUniform[4] );
          break;
      }
      break;
    } // case 3 on switch(dim1)
    case 4:{
      switch( dim2 ){
        case 0:
          i1lo = gpu_der_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0],              isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo = gpu_der_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0],              isUniform[4] );
          break;
        case 1:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo = gpu_der_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0],              isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo = gpu_der_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0],              isUniform[4] );
          break;
        case 2:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo = gpu_der_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0],              isUniform[2] );
          i4lo =     gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo = gpu_der_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0],              isUniform[4] );
          break;
        case 3:
          i1lo =     gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo =     gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo =     gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo = gpu_der_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0],              isUniform[3] );
          i5lo = gpu_der_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0],              isUniform[4] );
          break;
        case 4:
          i1lo =            gpu_basis_funs_1d( x1vals, xx1, order_, &x1coefs[0], xvalsize[0], isUniform[0] );
          i2lo =            gpu_basis_funs_1d( x2vals, xx2, order_, &x2coefs[0], xvalsize[1], isUniform[1] );
          i3lo =            gpu_basis_funs_1d( x3vals, xx3, order_, &x3coefs[0], xvalsize[2], isUniform[2] );
          i4lo =            gpu_basis_funs_1d( x4vals, xx4, order_, &x4coefs[0], xvalsize[3], isUniform[3] );
          i5lo = gpu_second_der_basis_funs_1d( x5vals, xx5, order_, &x5coefs[0], xvalsize[4], isUniform[4] );
          break;
      }
      break;
    } // case 4 on switch(dim1)
  }
  
  for( size_t k5=0; k5<=order_; ++k5 ){
    const size_t i5 = (i5lo+k5)*xvalsize[0]*xvalsize[1]*xvalsize[2]*xvalsize[3];
    for( size_t k4=0; k4<=order_; ++k4 ){
      const double tmp = x4coefs[k4] * x5coefs[k5];
      const size_t i4 = (i4lo+k4)*xvalsize[0]*xvalsize[1]*xvalsize[2];
      for( size_t k3=0; k3<=order_; ++k3 ){
        const double tmp2 = x3coefs[k3] * tmp;
        const size_t i3 = (i3lo+k3)*xvalsize[0]*xvalsize[1];
        for( size_t k2=0; k2<=order_; ++k2 ){
          const double tmp3 = x2coefs[k2] * tmp2;
          const size_t i2 = (i2lo+k2)*xvalsize[0];
          for( size_t k1=0; k1<=order_; ++k1 ){
            const size_t i1 = i1lo+k1 + i2 + i3 + i4 + i5;
            gpu_val[idx] += fvals_[i1] * x1coefs[k1] * tmp3;
          }
        }
      }
    }
  }
}
#endif
  
//===================================================================
#endif /* LAGRANGEINTERPOLANT_POLY_H_LOCK */

