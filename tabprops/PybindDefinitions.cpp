
#ifdef ENABLE_PYTHON
#include<tabprops/StateTable.h>
#include<tabprops/LagrangeInterpolant.h>
#ifdef ENABLE_MIXMDL // || ENABLE_PREPROCESSOR - be careful here
#include<tabprops/prepro/mixmdl/BetaMixMdl.h>
#include<tabprops/prepro/mixmdl/ClippedGaussMixMdl.h>
#include<tabprops/prepro/mixmdl/GaussKronrod.h>
#endif
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>


namespace py = pybind11;

void err_checking(int nvar, py::array_t<double> x1, py::array_t<double> x2, py::array_t<double> x3, py::array_t<double> x4, py::array_t<double> x5){
    const double * const ptr_check_x1 = (const double * const) x1.request().ptr;
    const double * const ptr_check_x2 = (const double * const) x2.request().ptr;
    const double * const ptr_check_x3 = (const double * const) x3.request().ptr;
    const double * const ptr_check_x4 = (const double * const) x4.request().ptr;
    const double * const ptr_check_x5 = (const double * const) x5.request().ptr;
    switch(nvar){
        case 1:
            if (!isnan(ptr_check_x2[0]) || !isnan(ptr_check_x3[0]) || !isnan(ptr_check_x4[0]) || !isnan(ptr_check_x5[0])){throw std::runtime_error("Expected 1 input.");}
            break;
        case 2:
            if (isnan(ptr_check_x2[0]) || !isnan(ptr_check_x3[0]) || !isnan(ptr_check_x4[0]) || !isnan(ptr_check_x5[0])){throw std::runtime_error("Expected 2 inputs.");}
            if (x1.request().shape != x2.request().shape){throw std::runtime_error("array shapes must match.");}
            break;
        case 3:
            if (isnan(ptr_check_x2[0]) || isnan(ptr_check_x3[0]) || !isnan(ptr_check_x4[0]) || !isnan(ptr_check_x5[0])){throw std::runtime_error("Expected 3 inputs.");}
            if (x1.request().shape != x2.request().shape ||
                x1.request().shape != x3.request().shape){throw std::runtime_error("array shapes must match.");}
            break;
        case 4:
            if (isnan(ptr_check_x2[0]) || isnan(ptr_check_x3[0]) || isnan(ptr_check_x4[0]) || !isnan(ptr_check_x5[0])){throw std::runtime_error("Expected 4 inputs.");}
            if (x1.request().shape != x2.request().shape ||
                x1.request().shape != x3.request().shape ||
                x1.request().shape != x4.request().shape){throw std::runtime_error("array shapes must match.");}
            break;
        case 5:
            if (isnan(ptr_check_x2[0]) || isnan(ptr_check_x3[0]) || isnan(ptr_check_x4[0]) || isnan(ptr_check_x5[0])){throw std::runtime_error("Expected 5 inputs.");}
            if (x1.request().shape != x2.request().shape ||
                x1.request().shape != x3.request().shape ||
                x1.request().shape != x4.request().shape ||
                x1.request().shape != x5.request().shape){throw std::runtime_error("array shapes must match.");}
            break;
    }
}

PYBIND11_MODULE(pytabprops, m) {
    py::class_<StateTable>(m, "StateTable")
        .def(py::init<const int>())
        .def(py::init<>())
        .def("get_ndim", &StateTable::get_ndim)
        .def("has_indepvar", &StateTable::has_indepvar)
        .def("has_depvar", &StateTable::has_depvar)
        .def("get_indepvar_names", &StateTable::get_indepvar_names)
        .def("get_depvar_names", &StateTable::get_depvar_names)
        .def("read_table", &StateTable::read_table)
        .def("write_table", &StateTable::write_table)
        .def("query", [](StateTable &self,
                         const std::string & depvarname,
                         py::array_t<double> x1,
                         py::array_t<double> x2,
                         py::array_t<double> x3,
                         py::array_t<double> x4,
                         py::array_t<double> x5)
        {
            const InterpT *const interp = self.find_entry(depvarname);
            if (interp==NULL){throw std::runtime_error(depvarname+" not in table.");}
            err_checking(self.get_ndim(), x1, x2, x3, x4, x5);
            auto query_single = [interp](double x1, double x2, double x3, double x4, double x5) {
                double query_ptr [5];
                query_ptr[0] = x1;
                query_ptr[1] = x2;
                query_ptr[2] = x3;
                query_ptr[3] = x4;
                query_ptr[4] = x5;
                return interp->value(query_ptr);
            };
            return py::vectorize(query_single)(x1, x2, x3, x4, x5);
        },
            py::arg("depvarname"),
            py::arg("x1"),
            py::arg("x2")=py::none(),
            py::arg("x3")=py::none(),
            py::arg("x4")=py::none(),
            py::arg("x5")=py::none()
        )

        .def("derivative", [](StateTable &self,
                              const std::string & depvarname,
                              py::array_t<double> x1)
        {
            if (self.get_ndim() != 1){throw std::runtime_error("Expected "+std::to_string(self.get_ndim())+" inputs");}
            const InterpT *const interp = self.find_entry(depvarname);
            if (interp==NULL){throw std::runtime_error(depvarname+" not in table.");}

            auto derivative_single = [interp](double x1) {
                double query_ptr [1];
                query_ptr[0] = x1;
                return interp->derivative(query_ptr, 0);
            };
            return py::vectorize(derivative_single)(x1);
        })

        .def("second_derivative", [](StateTable &self,
                                     const std::string & depvarname,
                                     py::array_t<double> x1)
        {
            if (self.get_ndim() != 1){throw std::runtime_error("Expected "+std::to_string(self.get_ndim())+" inputs");}
            const InterpT *const interp = self.find_entry(depvarname);
            if (interp==NULL){throw std::runtime_error(depvarname+" not in table.");}

            auto second_derivative_single = [interp](double x1) {
                double query_ptr [1];
                query_ptr[0] = x1;
                return interp->second_derivative(query_ptr, 0, 0);
            };
            return py::vectorize(second_derivative_single)(x1);
        })

        .def("derivative", [](StateTable &self,
                              const std::string & depvarname,
                              const std::string & indepvarname,
                              py::array_t<double> x1,
                              py::array_t<double> x2,
                              py::array_t<double> x3,
                              py::array_t<double> x4,
                              py::array_t<double> x5)
        {
            const InterpT *const interp = self.find_entry(depvarname);
            if (interp==NULL){throw std::runtime_error(depvarname+" not in table.");}

            int idx;
            auto it = std::find(self.get_indepvar_names().begin(), self.get_indepvar_names().end(), indepvarname);
            if (it != self.get_indepvar_names().end()){idx = it - self.get_indepvar_names().begin();}
            else{throw std::runtime_error(indepvarname+" not an independent variable.");}

            err_checking(self.get_ndim(), x1, x2, x3, x4, x5);

            auto derivative_single = [interp, idx](double x1, double x2, double x3, double x4, double x5) {
               double query_ptr [5];
               query_ptr[0] = x1;
               query_ptr[1] = x2;
               query_ptr[2] = x3;
               query_ptr[3] = x4;
               query_ptr[4] = x5;
               return interp->derivative(query_ptr, idx);
            };
            return py::vectorize(derivative_single)(x1, x2, x3, x4, x5);
        },
            py::arg("depvarname"),
            py::arg("indepvarname"),
            py::arg("x1"),
            py::arg("x2")=py::none(),
            py::arg("x3")=py::none(),
            py::arg("x4")=py::none(),
            py::arg("x5")=py::none()
        )

        .def("derivatives", [m](StateTable &self,
                                const std::string & depvarname,
                                py::array_t<double> x1,
                                py::array_t<double> x2,
                                py::array_t<double> x3,
                                py::array_t<double> x4,
                                py::array_t<double> x5)
        {
            const int nvar = self.get_ndim();
            const std::vector<std::string> indepvarnames = self.get_indepvar_names();
            py::tuple result(nvar);
            for (size_t i = 0; i < nvar; i++){
                result[i] = m.attr("StateTable").attr("derivative")(self, depvarname, indepvarnames[i], x1, x2, x3, x4, x5);
            }
            return result;
        },
            py::arg("depvarname"),
            py::arg("x1"),
            py::arg("x2")=py::none(),
            py::arg("x3")=py::none(),
            py::arg("x4")=py::none(),
            py::arg("x5")=py::none()
        )

        .def("second_derivative", [](StateTable &self,
                                     const std::string & depvarname,
                                     const std::string & indepvarname1,
                                     const std::string & indepvarname2,
                                     py::array_t<double> x1,
                                     py::array_t<double> x2,
                                     py::array_t<double> x3,
                                     py::array_t<double> x4,
                                     py::array_t<double> x5)
        {
            const InterpT *const interp = self.find_entry(depvarname);
            if (interp==NULL){throw std::runtime_error(depvarname+" not in table.");}

            int idx1;
            {
                auto it = std::find(self.get_indepvar_names().begin(), self.get_indepvar_names().end(), indepvarname1);
                if (it != self.get_indepvar_names().end()){idx1 = it - self.get_indepvar_names().begin();}
                else{throw std::runtime_error(indepvarname1+" not an independent variable.");}
            }

            int idx2;
            {
                auto it = std::find(self.get_indepvar_names().begin(), self.get_indepvar_names().end(), indepvarname2);
                if (it != self.get_indepvar_names().end()){idx2 = it - self.get_indepvar_names().begin();}
                else{throw std::runtime_error(indepvarname2+" not an independent variable.");}
            }

            err_checking(self.get_ndim(), x1, x2, x3, x4, x5);

            auto second_derivative_single = [interp, idx1, idx2](double x1, double x2, double x3, double x4, double x5) {
               double query_ptr [5];
               query_ptr[0] = x1;
               query_ptr[1] = x2;
               query_ptr[2] = x3;
               query_ptr[3] = x4;
               query_ptr[4] = x5;
               return interp->second_derivative(query_ptr, idx1, idx2);
            };
            return py::vectorize(second_derivative_single)(x1, x2, x3, x4, x5);
        },
            py::arg("depvarname"),
            py::arg("indepvarname1"),
            py::arg("indepvarname2"),
            py::arg("x1"),
            py::arg("x2")=py::none(),
            py::arg("x3")=py::none(),
            py::arg("x4")=py::none(),
            py::arg("x5")=py::none()
        )

        .def("add_entry", [](StateTable &self, const std::string & name,
                             const LagrangeInterpolant1D & interp,
                             const std::vector<std::string> & indepVarNames){
                             return self.add_entry(name, interp.clone(), indepVarNames, true);
                             })
        .def("add_entry", [](StateTable &self, const std::string & name,
                             const LagrangeInterpolant2D & interp,
                             const std::vector<std::string> & indepVarNames){
                             return self.add_entry(name, interp.clone(), indepVarNames, true);
                             })
        .def("add_entry", [](StateTable &self, const std::string & name,
                             const LagrangeInterpolant3D & interp,
                             const std::vector<std::string> & indepVarNames){
                             return self.add_entry(name, interp.clone(), indepVarNames, true);
                             })
        .def("add_entry", [](StateTable &self, const std::string & name,
                             const LagrangeInterpolant4D & interp,
                             const std::vector<std::string> & indepVarNames){
                             return self.add_entry(name, interp.clone(), indepVarNames, true);
                             })
        .def("add_entry", [](StateTable &self, const std::string & name,
                             const LagrangeInterpolant5D & interp,
                             const std::vector<std::string> & indepVarNames){
                             return self.add_entry(name, interp.clone(), indepVarNames, true);
                             });

    py::class_<LagrangeInterpolant1D>(m, "LagrangeInterpolant1D")
        .def(py::init<const unsigned,
                      const std::vector<double>&,
                      const std::vector<double>&,
                      const bool>());

    py::class_<LagrangeInterpolant2D>(m, "LagrangeInterpolant2D")
        .def(py::init<const unsigned,
                      const std::vector<double>&,
                      const std::vector<double>&,
                      const std::vector<double>&,
                      const bool>());

    py::class_<LagrangeInterpolant3D>(m, "LagrangeInterpolant3D")
        .def(py::init<const unsigned,
                      const std::vector<double>&,
                      const std::vector<double>&,
                      const std::vector<double>&,
                      const std::vector<double>&,
                      const bool>());

    py::class_<LagrangeInterpolant4D>(m, "LagrangeInterpolant4D")
        .def(py::init<const unsigned,
                      const std::vector<double>&,
                      const std::vector<double>&,
                      const std::vector<double>&,
                      const std::vector<double>&,
                      const std::vector<double>&,
                      const bool>());

    py::class_<LagrangeInterpolant5D>(m, "LagrangeInterpolant5D")
        .def(py::init<const unsigned,
                      const std::vector<double>&,
                      const std::vector<double>&,
                      const std::vector<double>&,
                      const std::vector<double>&,
                      const std::vector<double>&,
                      const std::vector<double>&,
                      const bool>());

#ifdef ENABLE_MIXMDL // || ENABLE_PREPROCESSOR - be careful here

    py::class_<BetaMixMdl>(m, "BetaMixMdl")
        .def(py::init<>())
        .def("get_pdf", static_cast<double (BetaMixMdl::*)(const double)>(&BetaMixMdl::get_pdf), "get_pdf")
        .def("get_pdf", py::vectorize(static_cast<double (BetaMixMdl::*)(const double)>(&BetaMixMdl::get_pdf)))
        .def("set_mean", static_cast<void (PresumedPDFMixMdl::*)(const double)>(&PresumedPDFMixMdl::set_mean), "set_mean")
        .def("set_variance", static_cast<void (PresumedPDFMixMdl::*)(const double)>(&PresumedPDFMixMdl::set_variance), "set_variance")
        .def("set_scaled_variance", static_cast<void (PresumedPDFMixMdl::*)(const double)>(&PresumedPDFMixMdl::set_scaled_variance), "set_scaled_variance")
        .def("get_mean", static_cast<double (PresumedPDFMixMdl::*)()const>(&PresumedPDFMixMdl::get_mean), "get_mean")
        .def("get_variance", static_cast<double (PresumedPDFMixMdl::*)()const>(&PresumedPDFMixMdl::get_variance), "get_variance")
        .def("get_scaled_variance", static_cast<double (PresumedPDFMixMdl::*)()const>(&PresumedPDFMixMdl::get_scaled_variance), "get_scaled_variance")
        .def("integrate", [](BetaMixMdl &self,
                             const LagrangeInterpolant1D & interp,
                             const int n_intervals){
                                 auto* integrator = new GaussKronrod( 0.0, 1.0, n_intervals );
                                 std::vector<double> vals(1, 123.4);
                                 auto * func = new FunctorDoubleVec<LagrangeInterpolant1D>(
                                        static_cast<LagrangeInterpolant1D*>(interp.clone()),
                                        &LagrangeInterpolant1D::value,
                                        0,
                                        vals);
                                 integrator->set_integrand_func(func);
                                 self.set_integrator(integrator);
                                 self.set_convolution_func(func);
                                 return self.integrate();
                             });

    py::class_<ClipGauss>(m, "ClippedGaussMixMdl")
        .def(py::init<>())
        .def(py::init<const int, const int, const bool>())
        .def("get_pdf", static_cast<double (ClipGauss::*)(const double)>(&ClipGauss::get_pdf), "get_pdf")
        .def("get_pdf", py::vectorize(static_cast<double (ClipGauss::*)(const double)>(&ClipGauss::get_pdf)))
        .def("set_mean", static_cast<void (PresumedPDFMixMdl::*)(const double)>(&PresumedPDFMixMdl::set_mean), "set_mean")
        .def("set_variance", static_cast<void (PresumedPDFMixMdl::*)(const double)>(&PresumedPDFMixMdl::set_variance), "set_variance")
        .def("set_scaled_variance", static_cast<void (PresumedPDFMixMdl::*)(const double)>(&PresumedPDFMixMdl::set_scaled_variance), "set_scaled_variance")
        .def("get_mean", static_cast<double (PresumedPDFMixMdl::*)()const>(&PresumedPDFMixMdl::get_mean), "get_mean")
        .def("get_variance", static_cast<double (PresumedPDFMixMdl::*)()const>(&PresumedPDFMixMdl::get_variance), "get_variance")
        .def("get_scaled_variance", static_cast<double (PresumedPDFMixMdl::*)()const>(&PresumedPDFMixMdl::get_scaled_variance), "get_scaled_variance")
        .def("integrate", [](ClipGauss &self,
                             const LagrangeInterpolant1D & interp,
                             const int n_intervals){
                                 auto* integrator = new GaussKronrod( 0.0, 1.0, n_intervals );
                                 std::vector<double> vals(1, 123.4);
                                 auto * func = new FunctorDoubleVec<LagrangeInterpolant1D>(
                                        static_cast<LagrangeInterpolant1D*>(interp.clone()),
                                        &LagrangeInterpolant1D::value,
                                        0,
                                        vals);
                                 integrator->set_integrand_func(func);
                                 self.set_integrator(integrator);
                                 self.set_convolution_func(func);
                                 return self.integrate();
                             });
#endif
}

#endif
