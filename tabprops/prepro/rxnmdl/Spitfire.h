/*
 * Copyright (c) 2014-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef TABPROPS_SPITFIRE_H
#define TABPROPS_SPITFIRE_H

#include <tabprops/prepro/rxnmdl/ReactionModel.h>

class Spitfire : public ReactionModel {
public:

    Spitfire(Cantera::IdealGasMix &gas, const int order, const std::string &txtFileDir, const std::string &tableName);

    ~Spitfire() {}

    void implement();

    const std::vector<std::string> &indep_var_names() const { return ivarNames_; }

private:

    std::vector<std::string> ivarNames_;

    std::vector<std::string> dvarNames_;

    std::map<std::string, std::vector<double> > ivarMap_;

    std::map<std::string, std::vector<double> > dvarMap_;

    int nDim_;

    std::string tableName_;

    std::map<std::string, std::vector<double> >
    read_bulkdata(const std::vector<std::string> &var_names, const bool isIndepVar, const std::string &outdir) const;

    template<typename T>
    std::vector<T> read_file(const std::string &file_name) const;

    void check_for_depvar(std::string depVarName) const;

    void verify_table(const std::string) const;
};


#endif //TABPROPS_SPITFIRE_H
