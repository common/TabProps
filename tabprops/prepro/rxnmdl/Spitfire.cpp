/*
 * Copyright (c) 2014-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <fstream>
#include <iostream>
#include <tabprops/StateTable.h>    // for table verification
#include <tabprops/prepro/rxnmdl/Spitfire.h>

//--------------------------------------------------------------------

Spitfire::Spitfire(Cantera::IdealGasMix &gas,
                   const int order,
                   const std::string &txtFileDir,
                   const std::string &tableName)
        : ReactionModel(gas,
                        read_file<std::string>(txtFileDir + "/metadata_independent_variables.txt"),
                        order,
                        "Spitfire"),
          tableName_(tableName) {
    // read independent variables
    ivarNames_ = read_file<std::string>(txtFileDir + "/metadata_independent_variables.txt");
    nDim_ = ivarNames_.size();
    if (nDim_ > 5) { throw std::runtime_error("Unsupported table dimensionality."); }
    // read values for each independent variable into a map
    ivarMap_ = read_bulkdata(ivarNames_, true, txtFileDir);

    size_t gridpts = 1;
    for (const auto &ivar : ivarNames_) {
        gridpts *= ivarMap_.at(ivar).size();
    }

    // read independent variables
    dvarNames_ = read_file<std::string>(txtFileDir + "/metadata_dependent_variables.txt");
    // check T,p,Yi are present in dependent variables
    check_for_depvar("temperature");
    check_for_depvar("pressure");
    for (int isp = 0; isp < gasProps_.nSpecies(); ++isp) {
        check_for_depvar("mass_fraction_" + gasProps_.speciesName(isp));
    }

    // read values for each dependent variable into a map
    dvarMap_ = read_bulkdata(dvarNames_, false, txtFileDir);
    // check for consistent dimensionality
    if (dvarMap_.at("temperature").size() != gridpts) {
        throw std::runtime_error("Dimension mismatch between independent variables and temperature.");
    }
    if (dvarMap_.at("pressure").size() != gridpts) {
        throw std::runtime_error("Dimension mismatch between independent variables and pressure.");
    }
    for (int isp = 0; isp < gasProps_.nSpecies(); ++isp) {
        if (dvarMap_.at("mass_fraction_" + gasProps_.speciesName(isp)).size() != gridpts) {
            throw std::runtime_error(
                    "Dimension mismatch between independent variables and mass_fraction_" + gasProps_.speciesName(isp) +
                    ".");
        }
    }

    tableBuilder_.set_filename(tableName);
}

//--------------------------------------------------------------------

void
Spitfire::implement() {
    for (int i = 0; i < nDim_; ++i) {
        tableBuilder_.set_mesh(i, ivarMap_.at(ivarNames_[i]));
    }

    std::vector<double> pt(nDim_, 0.0);
    std::vector<double> ys(gasProps_.nSpecies(), 0.0);

    switch (nDim_) {
        case 1:
            for (int i = 0; i < ivarMap_.at(ivarNames_[0]).size(); ++i) {
                pt[0] = ivarMap_.at(ivarNames_[0])[i];
                const double T = dvarMap_.at("temperature")[i];
                const double P = dvarMap_.at("pressure")[i];
                for (int isp = 0; isp < gasProps_.nSpecies(); ++isp) {
                    std::string specName = gasProps_.speciesName(isp);
                    ys[isp] = dvarMap_.at("mass_fraction_" + specName)[i];
                }
                tableBuilder_.insert_entry(pt, T, P, ys);
            }
            break;
        case 2:
            for (int j = 0; j < ivarMap_.at(ivarNames_[1]).size(); ++j) {
                pt[1] = ivarMap_.at(ivarNames_[1])[j];
                for (int i = 0; i < ivarMap_.at(ivarNames_[0]).size(); ++i) {
                    pt[0] = ivarMap_.at(ivarNames_[0])[i];
                    const size_t idx = i + j * ivarMap_.at(ivarNames_[0]).size();
                    const double T = dvarMap_.at("temperature")[idx];
                    const double P = dvarMap_.at("pressure")[idx];
                    for (int isp = 0; isp < gasProps_.nSpecies(); ++isp) {
                        std::string specName = gasProps_.speciesName(isp);
                        ys[isp] = dvarMap_.at("mass_fraction_" + specName)[idx];
                    }
                    tableBuilder_.insert_entry(pt, T, P, ys);
                }
            }
            break;
        case 3:
            for (int k = 0; k < ivarMap_.at(ivarNames_[2]).size(); ++k) {
                pt[2] = ivarMap_.at(ivarNames_[2])[k];
                for (int j = 0; j < ivarMap_.at(ivarNames_[1]).size(); ++j) {
                    pt[1] = ivarMap_.at(ivarNames_[1])[j];
                    for (int i = 0; i < ivarMap_.at(ivarNames_[0]).size(); ++i) {
                        pt[0] = ivarMap_.at(ivarNames_[0])[i];
                        const size_t idx = i + j * ivarMap_.at(ivarNames_[0]).size() +
                                           k * ivarMap_.at(ivarNames_[0]).size() *
                                           ivarMap_.at(ivarNames_[1]).size();
                        const double T = dvarMap_.at("temperature")[idx];
                        const double P = dvarMap_.at("pressure")[idx];
                        for (int isp = 0; isp < gasProps_.nSpecies(); ++isp) {
                            std::string specName = gasProps_.speciesName(isp);
                            ys[isp] = dvarMap_.at("mass_fraction_" + specName)[idx];
                        }
                        tableBuilder_.insert_entry(pt, T, P, ys);
                    }
                }
            }
            break;
        case 4:
            for (int l = 0; l < ivarMap_.at(ivarNames_[3]).size(); ++l) {
                pt[3] = ivarMap_.at(ivarNames_[3])[l];
                for (int k = 0; k < ivarMap_.at(ivarNames_[2]).size(); ++k) {
                    pt[2] = ivarMap_.at(ivarNames_[2])[k];
                    for (int j = 0; j < ivarMap_.at(ivarNames_[1]).size(); ++j) {
                        pt[1] = ivarMap_.at(ivarNames_[1])[j];
                        for (int i = 0; i < ivarMap_.at(ivarNames_[0]).size(); ++i) {
                            pt[0] = ivarMap_.at(ivarNames_[0])[i];
                            const size_t idx = i + j * ivarMap_.at(ivarNames_[0]).size() +
                                               k * ivarMap_.at(ivarNames_[0]).size() *
                                               ivarMap_.at(ivarNames_[1]).size() +
                                               l * ivarMap_.at(ivarNames_[0]).size() *
                                               ivarMap_.at(ivarNames_[1]).size() *
                                               ivarMap_.at(ivarNames_[2]).size();
                            const double T = dvarMap_.at("temperature")[idx];
                            const double P = dvarMap_.at("pressure")[idx];
                            for (int isp = 0; isp < gasProps_.nSpecies(); ++isp) {
                                std::string specName = gasProps_.speciesName(isp);
                                ys[isp] = dvarMap_.at("mass_fraction_" + specName)[idx];
                            }
                            tableBuilder_.insert_entry(pt, T, P, ys);
                        }
                    }
                }
            }
            break;
        case 5:
            for (int m = 0; m < ivarMap_.at(ivarNames_[4]).size(); ++m) {
                pt[4] = ivarMap_.at(ivarNames_[4])[m];
                for (int l = 0; l < ivarMap_.at(ivarNames_[3]).size(); ++l) {
                    pt[3] = ivarMap_.at(ivarNames_[3])[l];
                    for (int k = 0; k < ivarMap_.at(ivarNames_[2]).size(); ++k) {
                        pt[2] = ivarMap_.at(ivarNames_[2])[k];
                        for (int j = 0; j < ivarMap_.at(ivarNames_[1]).size(); ++j) {
                            pt[1] = ivarMap_.at(ivarNames_[1])[j];
                            for (int i = 0; i < ivarMap_.at(ivarNames_[0]).size(); ++i) {
                                pt[0] = ivarMap_.at(ivarNames_[0])[i];
                                const size_t idx = i + j * ivarMap_.at(ivarNames_[0]).size() +
                                                   k * ivarMap_.at(ivarNames_[0]).size() *
                                                   ivarMap_.at(ivarNames_[1]).size() +
                                                   l * ivarMap_.at(ivarNames_[0]).size() *
                                                   ivarMap_.at(ivarNames_[1]).size() *
                                                   ivarMap_.at(ivarNames_[2]).size() +
                                                   m * ivarMap_.at(ivarNames_[0]).size() *
                                                   ivarMap_.at(ivarNames_[1]).size() *
                                                   ivarMap_.at(ivarNames_[2]).size() *
                                                   ivarMap_.at(ivarNames_[3]).size();
                                const double T = dvarMap_.at("temperature")[idx];
                                const double P = dvarMap_.at("pressure")[idx];
                                for (int isp = 0; isp < gasProps_.nSpecies(); ++isp) {
                                    std::string specName = gasProps_.speciesName(isp);
                                    ys[isp] = dvarMap_.at("mass_fraction_" + specName)[idx];
                                }
                                tableBuilder_.insert_entry(pt, T, P, ys);
                            }
                        }
                    }
                }
            }
            break;
        default:
            std::ostringstream errmsg;
            errmsg << "ERROR: unsupported dimension for table creation!"
                   << __FILE__ << " : " << __LINE__ << std::endl;
            throw std::runtime_error(errmsg.str());

    }

    tableBuilder_.generate_table();

    verify_table(tableName_ + ".tbl");
}

//--------------------------------------------------------------------

template<typename T>
std::vector<T> Spitfire::read_file(const std::string &file_name) const {
    std::vector<T> result;
    {
        std::ifstream infile(file_name);
        std::string line;
        if (infile.is_open()) {
            while (std::getline(infile, line)) {
                std::istringstream iss(line);
                T this_value;
                iss >> this_value;
                result.push_back(this_value);
            }
        } else throw std::runtime_error("Unable to open file " + file_name);
    }
    return result;
}

//--------------------------------------------------------------------

std::map<std::string, std::vector<double> > Spitfire::read_bulkdata(const std::vector<std::string> &var_names,
                                                                    const bool isIndepVar,
                                                                    const std::string &outdir) const {
    std::map<std::string, std::vector<double> > value_map;
    std::string file_prefix;
    isIndepVar ? file_prefix = "ivar_" : file_prefix = "dvar_";
    for (const auto &var : var_names) {
        value_map.insert(std::make_pair(var, read_file<double>(outdir + "/bulkdata_" + file_prefix + var + ".txt")));
    }
    return value_map;
}

//--------------------------------------------------------------------

void Spitfire::check_for_depvar(std::string depVarName) const {
    int depVarIndex = -1;
    for (int i = 0; i < dvarNames_.size(); ++i) {
        if (depVarName == dvarNames_[i]) {
            depVarIndex = i;
        }
    }
    if (depVarIndex < 0) {
        throw std::runtime_error("Could not find " + depVarName + " in dependent variables.");
    }
}

//--------------------------------------------------------------------

void Spitfire::verify_table(const std::string filename) const {

    // load the table from disk
    std::cout << "Loading " << filename << std::endl;
    StateTable table;
    table.read_table(filename);

    const InterpT *const tblTsp = table.find_entry("Temperature");
    bool pointsInterpolatedCorrectly = true;
    if (tblTsp == NULL) {
        std::cout << "Could not verify table since temperature was not found." << std::endl;
        pointsInterpolatedCorrectly = false;
    } else {
        std::string depvarname = "temperature";
        std::vector<double> pt(nDim_, 0.0);
        // get percent difference between the original and interpolated value for the temperature.
        switch (nDim_) {
            case 1:
                for (int i = 0; i < ivarMap_.at(ivarNames_[0]).size(); ++i) {
                    pt[0] = ivarMap_.at(ivarNames_[0])[i];
                    try {
                        const double dval = dvarMap_.at(depvarname)[i];
                        double Tperr =
                                100.0 * (dval - tblTsp->value(pt)) / dval;
                        if (std::abs(Tperr) > 1.0e-10) {
                            std::cout << "PROBLEMS at ivar = [" << pt[0] << "]!  depvar: " << dval
                                      << ", " << tblTsp->value(pt) << ", %error=" << Tperr << std::endl;
                            pointsInterpolatedCorrectly = false;
                        }
                    }
                    catch (std::runtime_error &err) {
                        std::cout << err.what() << "ERROR interpolating table for ivar1=" << pt[0] << std::endl;
                    }
                }
                break;
            case 2:
                for (int j = 0; j < ivarMap_.at(ivarNames_[1]).size(); ++j) {
                    pt[1] = ivarMap_.at(ivarNames_[1])[j];
                    for (int i = 0; i < ivarMap_.at(ivarNames_[0]).size(); ++i) {
                        pt[0] = ivarMap_.at(ivarNames_[0])[i];
                        const size_t idx = i + j * ivarMap_.at(ivarNames_[0]).size();
                        try {
                            const double dval = dvarMap_.at(depvarname)[idx];
                            double Tperr =
                                    100.0 * (dval - tblTsp->value(pt)) / dval;
                            if (std::abs(Tperr) > 1.0e-10) {
                                std::cout << "PROBLEMS at ivar = [" << pt[0] << ", " << pt[1] << "]!  depvar: " << dval
                                          << ", " << tblTsp->value(pt) << ", %error=" << Tperr << std::endl;
                                pointsInterpolatedCorrectly = false;
                            }
                        }
                        catch (std::runtime_error &err) {
                            std::cout << err.what() << "ERROR interpolating table for ivar = [" << pt[0] << ", "
                                      << pt[1] << "]" << std::endl;
                        }
                    }
                }
                break;
            case 3:
                for (int k = 0; k < ivarMap_.at(ivarNames_[2]).size(); ++k) {
                    pt[2] = ivarMap_.at(ivarNames_[2])[k];
                    for (int j = 0; j < ivarMap_.at(ivarNames_[1]).size(); ++j) {
                        pt[1] = ivarMap_.at(ivarNames_[1])[j];
                        for (int i = 0; i < ivarMap_.at(ivarNames_[0]).size(); ++i) {
                            pt[0] = ivarMap_.at(ivarNames_[0])[i];
                            const size_t idx = i + j * ivarMap_.at(ivarNames_[0]).size() +
                                               k * ivarMap_.at(ivarNames_[0]).size() *
                                               ivarMap_.at(ivarNames_[1]).size();
                            try {
                                const double dval = dvarMap_.at(depvarname)[idx];
                                double Tperr =
                                        100.0 * (dval - tblTsp->value(pt)) / dval;
                                if (std::abs(Tperr) > 1.0e-10) {
                                    std::cout << "PROBLEMS at ivar = [" << pt[0] << ", " << pt[1] << ", " << pt[2]
                                              << "]!  depvar: " << dval << ", " << tblTsp->value(pt) << ", %error="
                                              << Tperr << std::endl;
                                    pointsInterpolatedCorrectly = false;
                                }
                            }
                            catch (std::runtime_error &err) {
                                std::cout << err.what() << "ERROR interpolating table for ivar = [" << pt[0] << ", "
                                          << pt[1] << ", " << pt[2] << "]" << std::endl;
                            }
                        }
                    }
                }
                break;
            case 4:
                for (int l = 0; l < ivarMap_.at(ivarNames_[3]).size(); ++l) {
                    pt[3] = ivarMap_.at(ivarNames_[3])[l];
                    for (int k = 0; k < ivarMap_.at(ivarNames_[2]).size(); ++k) {
                        pt[2] = ivarMap_.at(ivarNames_[2])[k];
                        for (int j = 0; j < ivarMap_.at(ivarNames_[1]).size(); ++j) {
                            pt[1] = ivarMap_.at(ivarNames_[1])[j];
                            for (int i = 0; i < ivarMap_.at(ivarNames_[0]).size(); ++i) {
                                pt[0] = ivarMap_.at(ivarNames_[0])[i];
                                const size_t idx = i + j * ivarMap_.at(ivarNames_[0]).size() +
                                                   k * ivarMap_.at(ivarNames_[0]).size() *
                                                   ivarMap_.at(ivarNames_[1]).size() +
                                                   l * ivarMap_.at(ivarNames_[0]).size() *
                                                   ivarMap_.at(ivarNames_[1]).size() *
                                                   ivarMap_.at(ivarNames_[2]).size();
                                try {
                                    const double dval = dvarMap_.at(depvarname)[idx];
                                    double Tperr =
                                            100.0 * (dval - tblTsp->value(pt)) / dval;
                                    if (std::abs(Tperr) > 1.0e-10) {
                                        std::cout << "PROBLEMS at ivar = [" << pt[0] << ", " << pt[1] << ", " << pt[2]
                                                  << ", " << pt[3] << "]!  depvar: " << dval << ", "
                                                  << tblTsp->value(pt) << ", %error=" << Tperr << std::endl;
                                        pointsInterpolatedCorrectly = false;
                                    }
                                }
                                catch (std::runtime_error &err) {
                                    std::cout << err.what() << "ERROR interpolating table for ivar = [" << pt[0] << ", "
                                              << pt[1] << ", " << pt[2] << ", " << pt[3] << "]" << std::endl;
                                }
                            }
                        }
                    }
                }
                break;
            case 5:
                for (int m = 0; m < ivarMap_.at(ivarNames_[4]).size(); ++m) {
                    pt[4] = ivarMap_.at(ivarNames_[4])[m];
                    for (int l = 0; l < ivarMap_.at(ivarNames_[3]).size(); ++l) {
                        pt[3] = ivarMap_.at(ivarNames_[3])[l];
                        for (int k = 0; k < ivarMap_.at(ivarNames_[2]).size(); ++k) {
                            pt[2] = ivarMap_.at(ivarNames_[2])[k];
                            for (int j = 0; j < ivarMap_.at(ivarNames_[1]).size(); ++j) {
                                pt[1] = ivarMap_.at(ivarNames_[1])[j];
                                for (int i = 0; i < ivarMap_.at(ivarNames_[0]).size(); ++i) {
                                    pt[0] = ivarMap_.at(ivarNames_[0])[i];
                                    const size_t idx = i + j * ivarMap_.at(ivarNames_[0]).size() +
                                                       k * ivarMap_.at(ivarNames_[0]).size() *
                                                       ivarMap_.at(ivarNames_[1]).size() +
                                                       l * ivarMap_.at(ivarNames_[0]).size() *
                                                       ivarMap_.at(ivarNames_[1]).size() *
                                                       ivarMap_.at(ivarNames_[2]).size() +
                                                       m * ivarMap_.at(ivarNames_[0]).size() *
                                                       ivarMap_.at(ivarNames_[1]).size() *
                                                       ivarMap_.at(ivarNames_[2]).size() *
                                                       ivarMap_.at(ivarNames_[3]).size();
                                    try {
                                        const double dval = dvarMap_.at(depvarname)[idx];
                                        double Tperr =
                                                100.0 * (dval - tblTsp->value(pt)) / dval;
                                        if (std::abs(Tperr) > 1.0e-10) {
                                            std::cout << "PROBLEMS at ivar = [" << pt[0] << ", " << pt[1] << ", " << pt[2]
                                                      << ", " << pt[3] << ", " << pt[4] << "]!  depvar: " << dval << ", "
                                                      << tblTsp->value(pt) << ", %error=" << Tperr << std::endl;
                                            pointsInterpolatedCorrectly = false;
                                        }
                                    }
                                    catch (std::runtime_error &err) {
                                        std::cout << err.what() << "ERROR interpolating table for ivar = [" << pt[0] << ", "
                                                  << pt[1] << ", " << pt[2] << ", " << pt[3] << ", " << pt[4] << "]" << std::endl;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            default:
                std::ostringstream errmsg;
                errmsg << "ERROR: unsupported table dimension!"
                       << __FILE__ << " : " << __LINE__ << std::endl;
                throw std::runtime_error(errmsg.str());
        }

        if (pointsInterpolatedCorrectly) {
            std::cout << "The table correctly interpolates the spitfire data." << std::endl;
        } else {
            std::cout << "********" << std::endl
                      << "WARNING: the table does not interpolate the spitfire data!  This likely indicates a problem with the table!"
                      << "********" << std::endl;
        }
    }
}
