# ——————  CUDA macros  ————————
macro( tp_cuda_prep_file file )
    add_custom_command( OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${file}.cu
            COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/${file} ${CMAKE_CURRENT_BINARY_DIR}
            COMMAND ${CMAKE_COMMAND} -E rename ${CMAKE_CURRENT_BINARY_DIR}/${file} ${CMAKE_CURRENT_BINARY_DIR}/${file}.cu
            DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${file}
        )
    set_source_files_properties( ${CMAKE_CURRENT_BINARY_DIR}/${file}.cu
            PROPERTIES GENERATED TRUE
        )
endmacro( tp_cuda_prep_file )

macro( tp_add_library lib_name )
    set( files ${ARGN} )
    foreach( file ${files} )
        if( ENABLE_CUDA )
            tp_cuda_prep_file( ${file} )
            set( cufiles ${cufiles} ${CMAKE_CURRENT_BINARY_DIR}/${file}.cu )
        endif( ENABLE_CUDA )
    endforeach()
    if( ENABLE_CUDA )
        cuda_add_library( ${lib_name} ${cufiles} )
        unset( cufiles )
    else( ENABLE_CUDA )
        add_library( ${lib_name} ${files} )
    endif( ENABLE_CUDA )
endmacro( tp_add_library )

macro( tp_add_executable exe_name )
    set( files ${ARGN} )
    foreach( file ${files} )
        if( ENABLE_CUDA )
            tp_cuda_prep_file( ${file} )
            set( cufiles ${cufiles} ${CMAKE_CURRENT_BINARY_DIR}/${file}.cu )
        endif( ENABLE_CUDA )
    endforeach()
    if( ENABLE_CUDA )
        cuda_add_executable( ${exe_name} ${cufiles} )
        unset( cufiles )
    else( ENABLE_CUDA )
        add_executable( ${exe_name} ${files} )
    endif( ENABLE_CUDA )
endmacro( tp_add_executable )

macro( tp_cuda_prep_dir )
    if( ENABLE_CUDA )
        include_directories( ${CMAKE_CURRENT_SOURCE_DIR} )
    endif( ENABLE_CUDA )
endmacro( tp_cuda_prep_dir )
# ————————————————————————————