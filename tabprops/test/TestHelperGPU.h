#ifndef HLA_TestHelperGPU_h
#define HLA_TestHelperGPU_h

#include <iostream>

#ifdef ENABLE_CUDA
#include "cuda.h"


#include <boost/chrono.hpp>

#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>


//==============================================================================

bool gpu_error_check(const double* gpuValues, const double* cpuValues, const size_t n, const std::string modelname, const boost::chrono::duration<double>& tcpu, const boost::chrono::duration<double>& tgpu)
{
  double *hostValues;
  hostValues = (double*)malloc(sizeof(double) * n);
  cudaError_t err= cudaMemcpy( hostValues, gpuValues, sizeof(double) * n, cudaMemcpyDeviceToHost );
  if(err !=0){
    std::cout << " CUDA error in time_it : " << cudaGetErrorString(err) << "\n";
    return false;
  }
  for (int i=0; i<n; ++i) {
    if (std::abs(hostValues[i] - cpuValues[i]) > 1e-8) {
      std::cout << " GPU ERROR : in the time_it()" << hostValues[i] << " -  " << cpuValues[i] << "\n";
      return false;
    }
  }
  std::cout << "\t" << n/tgpu.count() << " GPU "  << modelname << " per second" << std::endl;
  std::cout << "\t" << std::scientific << std::setprecision(2) << "GPU/CPU is " << tcpu/tgpu << std::endl;
  return true;
}
#endif

#endif // HLA_TestHelperGPU_h
